//
//  TAKUUIDStorage.m
//  TAKUUID
//
//  Created by Takahiro Ooishi
//  Copyright (c) 2014 Takahiro Ooishi. All rights reserved.
//  Released under the MIT license.
//

#import "TAKUUIDStorage.h"

static NSString * const Account = @"TAKUUIDStorage/Account";

@implementation TAKUUIDStorage

+ (TAKUUIDStorage *)sharedInstance {
  static TAKUUIDStorage *_instance = nil;
  static dispatch_once_t oncePredicate;
  dispatch_once(&oncePredicate, ^{
    _instance = [[TAKUUIDStorage alloc] init];
  });
  return _instance;
}

- (NSString *)UUIDString {
  CFDataRef result;
  OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)[self queryForFind], (CFTypeRef *)&result);
  if (status != errSecSuccess) {
    return nil;
  } else {
    NSData *data = (__bridge_transfer NSData *)result;
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
  }
}

- (NSString *)createUUIDString {
  NSString *UUIDString = [NSUUID UUID].UUIDString;
  return [self saveUUIDString:UUIDString] ? UUIDString : nil;
}

- (NSString *)createUUIDStringIfNeeded {
  return [self UUIDString] ?: [self createUUIDString];
}

- (BOOL)saveUUIDString:(NSString *)UUIDString {
  return SecItemAdd((__bridge CFDictionaryRef)[self queryForAdd:UUIDString], NULL) == errSecSuccess;
}

- (BOOL)clear {
  return SecItemDelete((__bridge CFDictionaryRef)[self queryForDelete]) == errSecSuccess;
}

- (BOOL)migrate {
  NSString *UUIDString = [self UUIDString];
  if (!UUIDString) {
    return NO;
  }
  if (![self clear]) {
    return NO;
  }
  return SecItemAdd((__bridge CFDictionaryRef)[self queryForAdd:UUIDString], NULL) == errSecSuccess;
}

#pragma mark - Private

- (NSDictionary *)queryForFind {
  return @{(__bridge id)kSecClass:(__bridge id)kSecClassGenericPassword,
           (__bridge id)kSecAttrAccount:Account,
           (__bridge id)kSecAttrService:[NSBundle mainBundle].bundleIdentifier,
           (__bridge id)kSecReturnData:(__bridge id)kCFBooleanTrue};
}

- (NSDictionary *)queryForAdd:(NSString *)UUIDString {
  NSMutableDictionary *items = @{
    (__bridge id)kSecClass:(__bridge id)kSecClassGenericPassword,
    (__bridge id)kSecAttrAccount:Account,
    (__bridge id)kSecAttrAccessible:(__bridge id)kSecAttrAccessibleAfterFirstUnlock,
    (__bridge id)kSecValueData:[UUIDString dataUsingEncoding:NSUTF8StringEncoding],
    (__bridge id)kSecAttrDescription:@"",
    (__bridge id)kSecAttrComment:@"",
    (__bridge id)kSecAttrService:[NSBundle mainBundle].bundleIdentifier
  }.mutableCopy;

  if (self.accessGroup.length > 0) {
    items[(__bridge id)kSecAttrAccessGroup] = self.accessGroup;
  }

  return [NSDictionary dictionaryWithDictionary:items];
}

- (NSDictionary *)queryForDelete {
  return @{(__bridge id)kSecClass:(__bridge id)kSecClassGenericPassword,
           (__bridge id)kSecAttrAccount:Account,
           (__bridge id)kSecAttrService:[NSBundle mainBundle].bundleIdentifier};
}

@end
