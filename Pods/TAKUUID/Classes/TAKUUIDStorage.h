//
//  TAKUUIDStorage.h
//  TAKUUID
//
//  Created by Takahiro Ooishi
//  Copyright (c) 2014 Takahiro Ooishi. All rights reserved.
//  Released under the MIT license.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>

NS_ASSUME_NONNULL_BEGIN

@interface TAKUUIDStorage : NSObject

@property (nonatomic, assign, readonly) OSStatus lastErrorStatus;
@property (nonatomic, copy, nullable) NSString *accessGroup;

@property (nonatomic, strong, class, readonly) TAKUUIDStorage *sharedInstance;

- (nullable NSString *)UUIDString; // Nil if not found in Keychain

- (nullable NSString *)createUUIDString; // Create and save UUIDString to Keychain. Might fail if an UUIDString existed in Keychain.
- (nullable NSString *)createUUIDStringIfNeeded; // Only create if there is no UUIDString found in Keychain.

- (BOOL)saveUUIDString:(NSString *)UUIDString; // Save UUIDString to Keychain
- (BOOL)clear; // Clear current UUIDString in Keychain. Return `YES` if found and successfully delete.

- (BOOL)migrate;

@end

NS_ASSUME_NONNULL_END
