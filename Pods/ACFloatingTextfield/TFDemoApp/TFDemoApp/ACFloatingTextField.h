//
//  AppTextField.h
//  TFDemoApp
//
//  Created by Abhishek Chandani on 19/05/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACFloatingTextField : UITextField {
  UIView *bottomLineView;
}

@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, strong) UIColor *selectedLineColor;

@property (nonatomic, strong) UIColor *placeholderColor;
@property (nonatomic, strong) UIColor *selectedPlaceholderColor;

@property (nonatomic, strong, readonly) UILabel *placeholderLabel;

@property (assign) BOOL disableFloatingLabel;

- (instancetype)init;
- (instancetype)initWithFrame:(CGRect)frame;

- (void)setTextFieldPlaceholderText:(NSString *)placeholderText;
- (void)upadteTextField:(CGRect)frame;

@end
