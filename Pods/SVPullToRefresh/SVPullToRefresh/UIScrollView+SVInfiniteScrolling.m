//
// UIScrollView+SVInfiniteScrolling.m
//
// Created by Sam Vermette on 23.04.12.
// Copyright (c) 2012 samvermette.com. All rights reserved.
//
// https://github.com/samvermette/SVPullToRefresh
//

#import <QuartzCore/QuartzCore.h>
#import "UIScrollView+SVInfiniteScrolling.h"

static const CGFloat SVInfiniteScrollingViewHeight = 60;

#pragma mark - SVInfiniteScrollingView

@interface SVInfiniteScrollingView ()

@property (nonatomic, copy) void (^infiniteScrollingHandler)(void);

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, assign) SVInfiniteScrollingState state;
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, UIView *> *viewForState;
@property (nonatomic, weak) UIScrollView *scrollView;
@property (nonatomic, assign) CGFloat originalBottomInset;
@property (nonatomic, assign) BOOL wasTriggeredByUser;
@property (nonatomic, assign, getter=isObserving) BOOL observing;

- (void)resetScrollViewContentInset;
- (void)setScrollViewContentInsetForInfiniteScrolling;

@end

@implementation SVInfiniteScrollingView

@synthesize state = _state;

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.state = SVInfiniteScrollingStateStopped;
    self.enabled = YES;
    self.viewForState = [NSMutableDictionary dictionary];
  }
  return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
  [super willMoveToSuperview:newSuperview];
  if (self.superview && !newSuperview) {
    UIScrollView *scrollView = (UIScrollView *)self.superview;
    if (scrollView.showsInfiniteScrolling) {
      if (self.isObserving) {
        [scrollView removeObserver:self forKeyPath:@"contentOffset"];
        [scrollView removeObserver:self forKeyPath:@"contentSize"];
        self.observing = NO;
      }
    }
  }
}

- (void)layoutSubviews {
  [super layoutSubviews];
  self.activityIndicatorView.center = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.height / 2.0);
}

#pragma mark - Scroll View

- (void)resetScrollViewContentInset {
  UIEdgeInsets currentInsets = self.scrollView.contentInset;
  currentInsets.bottom = self.originalBottomInset;
  CGPoint offsetBefore = self.scrollView.contentOffset;
  self.scrollView.contentInset = currentInsets;
  self.scrollView.contentOffset = offsetBefore;
}

- (void)setScrollViewContentInsetForInfiniteScrolling {
  UIEdgeInsets currentInsets = self.scrollView.contentInset;
  currentInsets.bottom = self.originalBottomInset + SVInfiniteScrollingViewHeight;
  self.scrollView.contentInset = currentInsets;
}

#pragma mark - Observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey, id> *)change context:(void *)context {
  if ([keyPath isEqualToString:@"contentOffset"]) {
    CGPoint newOffset = [change[NSKeyValueChangeNewKey] CGPointValue];
    // only excute when pull up
    if (newOffset.y >= 0)
      [self scrollViewDidScroll:newOffset];
  }
  else if ([keyPath isEqualToString:@"contentSize"]) {
    [self layoutSubviews];
    self.frame = CGRectMake(0, self.scrollView.contentSize.height, self.bounds.size.width, SVInfiniteScrollingViewHeight);
  }
}

- (void)scrollViewDidScroll:(CGPoint)contentOffset {
  if (self.state != SVInfiniteScrollingStateLoading && self.isEnabled) {
    CGFloat scrollOffsetThreshold = self.scrollView.contentSize.height - self.scrollView.bounds.size.height;
    
    if (!self.scrollView.isDragging && self.state == SVInfiniteScrollingStateTriggered)
      self.state = SVInfiniteScrollingStateLoading;
    else if (contentOffset.y > scrollOffsetThreshold && self.state == SVInfiniteScrollingStateStopped && self.scrollView.isDragging)
      self.state = SVInfiniteScrollingStateTriggered;
    else if (contentOffset.y < scrollOffsetThreshold && self.state != SVInfiniteScrollingStateStopped)
      self.state = SVInfiniteScrollingStateStopped;
  }
}

#pragma mark -

- (UIActivityIndicatorView *)activityIndicatorView {
  if (!_activityIndicatorView) {
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityIndicatorView.hidesWhenStopped = YES;
    [self addSubview:_activityIndicatorView];
  }
  return _activityIndicatorView;
}

- (UIActivityIndicatorViewStyle)activityIndicatorViewStyle {
  return self.activityIndicatorView.activityIndicatorViewStyle;
}

- (void)setActivityIndicatorViewStyle:(UIActivityIndicatorViewStyle)viewStyle {
  self.activityIndicatorView.activityIndicatorViewStyle = viewStyle;
}

#pragma mark -

- (void)startAnimating {
  self.state = SVInfiniteScrollingStateLoading;
}

- (void)stopAnimating {
  self.state = SVInfiniteScrollingStateStopped;
}

- (void)setState:(SVInfiniteScrollingState)newState {
  
  if (_state == newState)
    return;
  
  SVInfiniteScrollingState previousState = _state;
  _state = newState;
  
  for (UIView *otherView in self.viewForState.allValues) {
    [otherView removeFromSuperview];
  }
  
  UIView *customView = self.viewForState[@(newState)];
  if (customView) {
    CGRect rect = customView.bounds;
    rect.origin = CGPointMake(roundf((self.bounds.size.width - rect.size.width)/2), roundf((self.bounds.size.height - rect.size.height)/2));
    customView.frame = rect;
    [self addSubview:customView];
  } else {
    CGRect viewBounds = self.activityIndicatorView.bounds;
    CGPoint origin = CGPointMake(roundf((self.bounds.size.width - viewBounds.size.width)/2), roundf((self.bounds.size.height - viewBounds.size.height)/2));
    self.activityIndicatorView.frame = CGRectMake(origin.x, origin.y, viewBounds.size.width, viewBounds.size.height);
    
    switch (newState) {
      case SVInfiniteScrollingStateStopped:
        [self.activityIndicatorView stopAnimating];
        break;
        
      case SVInfiniteScrollingStateTriggered:
        [self.activityIndicatorView startAnimating];
        break;
        
      case SVInfiniteScrollingStateLoading:
        [self.activityIndicatorView startAnimating];
        break;
    }
  }
  
  if (previousState == SVInfiniteScrollingStateTriggered && newState == SVInfiniteScrollingStateLoading && self.infiniteScrollingHandler && self.isEnabled)
    self.infiniteScrollingHandler();
}

- (void)setCustomView:(UIView *)view forState:(SVInfiniteScrollingState)state {
  self.viewForState[@(state)] = view;
  self.state = self.state;
}

@end

#import <objc/runtime.h>

#pragma mark - UIScrollView (SVInfiniteScrollingView)

@implementation UIScrollView (SVInfiniteScrolling)

@dynamic infiniteScrollingView;

- (void)addInfiniteScrollingWithHandler:(nullable void (^)(void))handler {
  if (!self.infiniteScrollingView) {
    SVInfiniteScrollingView *view = [[SVInfiniteScrollingView alloc] initWithFrame:CGRectMake(0.0, self.contentSize.height, self.bounds.size.width, SVInfiniteScrollingViewHeight)];
    view.infiniteScrollingHandler = handler;
    view.scrollView = self;
    [self addSubview:view];
    
    view.originalBottomInset = self.contentInset.bottom;
    self.infiniteScrollingView = view;
    self.showsInfiniteScrolling = YES;
  } else {
    self.infiniteScrollingView.infiniteScrollingHandler = handler;
  }
}

- (void)triggerInfiniteScrolling {
  self.infiniteScrollingView.state = SVInfiniteScrollingStateTriggered;
  [self.infiniteScrollingView startAnimating];
}

- (void)setInfiniteScrollingView:(SVInfiniteScrollingView *)infiniteScrollingView {
  [self willChangeValueForKey:@"infiniteScrollingView"];
  objc_setAssociatedObject(self, &@selector(infiniteScrollingView), infiniteScrollingView, OBJC_ASSOCIATION_ASSIGN);
  [self didChangeValueForKey:@"infiniteScrollingView"];
}

- (SVInfiniteScrollingView *)infiniteScrollingView {
  return objc_getAssociatedObject(self, &@selector(infiniteScrollingView));
}

- (void)setShowsInfiniteScrolling:(BOOL)showsInfiniteScrolling {
  if (!self.infiniteScrollingView)
    return;
  
  self.infiniteScrollingView.hidden = !showsInfiniteScrolling;
  
  if (!showsInfiniteScrolling) {
    if (self.infiniteScrollingView.isObserving) {
      [self removeObserver:self.infiniteScrollingView forKeyPath:@"contentOffset"];
      [self removeObserver:self.infiniteScrollingView forKeyPath:@"contentSize"];
      [self.infiniteScrollingView resetScrollViewContentInset];
      self.infiniteScrollingView.observing = NO;
    }
  } else {
    if (!self.infiniteScrollingView.isObserving) {
      [self addObserver:self.infiniteScrollingView forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:NULL];
      [self addObserver:self.infiniteScrollingView forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
      [self.infiniteScrollingView setScrollViewContentInsetForInfiniteScrolling];
      self.infiniteScrollingView.observing = YES;
      
      [self.infiniteScrollingView setNeedsLayout];
      self.infiniteScrollingView.frame = CGRectMake(0.0, self.contentSize.height, self.infiniteScrollingView.bounds.size.width, SVInfiniteScrollingViewHeight);
    }
  }
}

- (BOOL)showsInfiniteScrolling {
  return !self.infiniteScrollingView.isHidden;
}

@end
