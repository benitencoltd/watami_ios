//
// UIScrollView+SVInfiniteScrolling.h
//
// Created by Sam Vermette on 23.04.12.
// Copyright (c) 2012 samvermette.com. All rights reserved.
//
// https://github.com/samvermette/SVPullToRefresh
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SVInfiniteScrollingView;

@interface UIScrollView (SVInfiniteScrolling)

- (void)addInfiniteScrollingWithHandler:(nullable void (^)(void))handler;
- (void)triggerInfiniteScrolling;

@property (nonatomic, strong, readonly, nullable) SVInfiniteScrollingView *infiniteScrollingView;
@property (nonatomic, assign) BOOL showsInfiniteScrolling;

@end


typedef NS_ENUM(NSUInteger, SVInfiniteScrollingState) {
	  SVInfiniteScrollingStateStopped = 0,
    SVInfiniteScrollingStateTriggered,
    SVInfiniteScrollingStateLoading
};

@interface SVInfiniteScrollingView : UIView

@property (nonatomic, assign) UIActivityIndicatorViewStyle activityIndicatorViewStyle;
@property (nonatomic, assign, readonly) SVInfiniteScrollingState state;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;

- (void)setCustomView:(nullable UIView *)view forState:(SVInfiniteScrollingState)state;

- (void)startAnimating;
- (void)stopAnimating;

@end

NS_ASSUME_NONNULL_END
