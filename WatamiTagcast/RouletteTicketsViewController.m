//
//  RouletteLotteriesViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/15/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "RouletteTicketsViewController.h"
#import "TicketsClaimAlertController.h"

@interface RouletteTicketsViewController ()

@end

@implementation RouletteTicketsViewController {
  BOOL _hasMatchedTagcast;
}

- (instancetype)initWithType:(WTRouletteTicketType)type tickets:(NSArray<WTRouletteTicket *> *)tickets tagcasts:(NSArray<NSDictionary<NSString *, id> *> *)tagcasts {
  self = [super initWithObjectClass:[WTRouletteTicket class] tagcasts:tagcasts];
  if (self) {
    self.title = NSLocalizedString(@"tickets", nil);
    [self.objects addObjectsFromArray:tickets];
    self.loadsObjestsOnViewDidLoad = NO;
    _type = type;
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [Common trackScreen:@"lottery_list_screen"];
}

- (void)claim {
  NSArray<NSIndexPath *> *indexPathsForSelectedItems = [self sortedIndexPathsForSelectedItems];
  NSArray<WTRouletteTicket *> *selectedTickets = [indexPathsForSelectedItems objectsByTransform:^WTRouletteTicket *(NSIndexPath *indexPath) {
    return self.objects[indexPath.item];
  }];
  
  TicketsClaimAlertController *claimAlertController = [[TicketsClaimAlertController alloc] init];
  [claimAlertController view];
  [claimAlertController.imageView sd_setImageWithURL:selectedTickets.firstObject.imageURL];
  if (selectedTickets.count > 1) {
    claimAlertController.expiryDateLabel.hidden = YES;
  } else {
    claimAlertController.expiryDateLabel.text = [NSString stringWithFormat:@"%@ : %@", NSLocalizedString(@"exp", nil), WTStringFromDate(selectedTickets.firstObject.expiryDate)];
  }
  WTWeakify(self);
  claimAlertController.claimHandler = ^(TicketsClaimAlertController *controller) {
    WTStrongify(RouletteTicketsViewController);
    NSAssert(indexPathsForSelectedItems.count > 0, @"Number of selected items must be greater than 0");
    // get joined ids as string descending. `7,3,1`
    NSString *stringIds = [[indexPathsForSelectedItems objectsByTransform:^NSString *(NSIndexPath *indexPath) {
      return [NSString stringWithFormat:@"%u", (unsigned int)self.objects[indexPath.item].id];
    }] componentsJoinedByString:@","];
    
    NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                               @"action":@"claimlottery",
                                               @"lang":[Common getLang],
                                               @"parameter":@{
                                                   @"lotterys_id":stringIds}};
    WTWeakify(self);
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
      WTStrongify(RouletteTicketsViewController);
      dispatch_async(dispatch_get_main_queue(), ^{
        [controller dismissViewControllerAnimated:YES completion:nil];
      });
      JSON(data)
      if (json) {
        void (^okActionHandler)(UIAlertAction *action);
        if ([json[@"data"][@"status"] boolValue]) {
          [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidClaimRouletteTicketsNotification object:selectedTickets];
          okActionHandler = ^(UIAlertAction *action) {
            for (NSIndexPath *indexPath in indexPathsForSelectedItems) {
              [strongSelf.objects removeObjectAtIndex:indexPath.item];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
              [strongSelf->_collectionView deleteItemsAtIndexPaths:indexPathsForSelectedItems];
              [strongSelf updateSelections];
            });
          };
        }
        dispatch_async(dispatch_get_main_queue(), ^{
          UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"WATAMI" message:json[@"data"][@"message"] preferredStyle:UIAlertControllerStyleAlert];
          [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:okActionHandler]];
          [strongSelf presentViewController:alertController animated:YES completion:nil];
        });
        [Common trackEvent:@"claim_roulette_ticket" action:@"claim_roulette_ticket" label:@"claim_roulette_ticket"];
      } else {
        
      }
    }];
    [task resume];
  };
  [self.tabBarController presentViewController:claimAlertController animated:YES completion:nil];
}

- (BOOL)isTagcastEnabledForCurrentFeature:(NSDictionary<NSString *, id> *)tagcast {
  return [tagcast[@"enable_lottery"] boolValue];
}

- (void)requestAppLinkWithURLString:(NSString *)URLString completion:(void (^)(NSString *appLinkId))completion {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getapplink",
                                             @"lang":[Common getLang],
                                             @"url":URLString};
  WTWeakify(self);
  [[[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(RouletteTicketsViewController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
      NSString *appLinkId = json[@"data"][@"app_link_id"];
      if (appLinkId.length > 0) {
        completion(appLinkId);
      }
    });
  }] resume];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  TicketCollectionViewCell *cell = [super collectionView:collectionView cellForItemAtIndexPath:indexPath];
  WTRouletteTicket *ticket = self.objects[indexPath.item];
  [cell.imageView sd_setImageWithURL:ticket.imageURL];
  cell.imageTextLabel.text = ticket.text;
  cell.expiryDateLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"exp", nil), WTStringFromDate(ticket.expiryDate)];
  WTWeakify(self);
  cell.shareHandler = ^(UIButton *sender) {
    WTStrongify(RouletteTicketsViewController);
    NSString *appLinkURLString = [NSString stringWithFormat:@"com.beniten.tagcast.watami://detail?type=1&desc=%@&title=%@&id=%u", ticket.text, ticket.title, (unsigned)ticket.id];
    [strongSelf requestAppLinkWithURLString:appLinkURLString completion:^(NSString *appLinkId) {
      WTStrongify(RouletteTicketsViewController);
      FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
      content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://fb.me/%@", appLinkId]];
      content.contentTitle = kWatamiTitle;
      content.contentDescription = ticket.text;
      content.imageURL = ticket.imageURL;
      
      FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
      dialog.fromViewController = strongSelf;
      dialog.mode = FBSDKShareDialogModeNative;
      dialog.shareContent = content;
      [dialog show];
    }];
  };
  return cell;
}

@end
