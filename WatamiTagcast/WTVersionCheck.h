//
//  WTVersionCheck.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/23/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTVersionCheck : JSONModel

@property (nonatomic, copy) NSString *latestVersionString;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, assign) BOOL forced;
@property (nonatomic, assign, getter=isIncentiveClaimed) BOOL incentiveClaimed;

@end
