//
//  DAKeyboardControl.m
//  DAKeyboardControlExample
//
//  Created by Daniel Amitay on 7/14/12.
//  Copyright (c) 2012 Daniel Amitay. All rights reserved.
//

#import "SVKeyboardControl.h"
#import <objc/runtime.h>

#define UIViewAnimationOptions(curve) curve << 16

@interface UIView (DAKeyboardControl_Internal) <UIGestureRecognizerDelegate>

@property (nonatomic, weak) UIResponder *currentResponder;
@property (nonatomic, strong) UIView *keyboardActiveView;
@property (nonatomic, strong) UIPanGestureRecognizer *keyboardPanRecognizer;
@property (nonatomic, assign, getter=isPanning) BOOL panning;

+ (UIView *)firstResponderInView:(UIView *)view;
+ (UIView *)inputSetHostViewInResponder:(UIResponder *)responder;

@end

@implementation UIView (DAKeyboardControl)

+ (void)load {
  // Swizzle the 'addSubview:' method to ensure that all input fields
  // have a valid inputAccessoryView upon addition to the view heirarchy
  SEL originalSelector = @selector(addSubview:);
  SEL swizzledSelector = @selector(swizzled_addSubview:);
  Method originalMethod = class_getInstanceMethod(self, originalSelector);
  Method swizzledMethod = class_getInstanceMethod(self, swizzledSelector);
  class_addMethod(self,
                  originalSelector,
                  class_getMethodImplementation(self, originalSelector),
                  method_getTypeEncoding(originalMethod));
  class_addMethod(self,
                  swizzledSelector,
                  class_getMethodImplementation(self, swizzledSelector),
                  method_getTypeEncoding(swizzledMethod));
  method_exchangeImplementations(originalMethod, swizzledMethod);
}

#pragma mark - Public Methods

- (void)addKeyboardPanningWithFrameHandler:(SVKeyboardFrameChangeHandler)frameHandler {
  [self addKeyboardControl:YES frameHandler:frameHandler constraintHandler:nil];
}

- (void)addKeyboardPanningWithFrameHandler:(SVKeyboardFrameChangeHandler)frameHandler
                         constraintHandler:(SVKeyboardFrameChangeHandler)constraintHandler {
  [self addKeyboardControl:YES frameHandler:frameHandler constraintHandler:constraintHandler];
}

- (void)addKeyboardNonpanningWithFrameHandler:(SVKeyboardFrameChangeHandler)frameHandler {
  [self addKeyboardControl:NO frameHandler:frameHandler constraintHandler:nil];
}

- (void)addKeyboardNonpanningWithFrameHandler:(SVKeyboardFrameChangeHandler)frameHandler
                            constraintHandler:(SVKeyboardFrameChangeHandler)constraintHandler {
  [self addKeyboardControl:NO frameHandler:frameHandler constraintHandler:constraintHandler];
}

- (void)addKeyboardControl:(BOOL)panning
              frameHandler:(SVKeyboardFrameChangeHandler)frameHandler
         constraintHandler:(SVKeyboardFrameChangeHandler)constraintHandler {
  if (panning && [self respondsToSelector:@selector(setKeyboardDismissMode:)]) {
    [(UIScrollView *)self setKeyboardDismissMode:UIScrollViewKeyboardDismissModeInteractive];
  } else {
    self.panning = panning;
  }
  self.keyboardFrameHandler = frameHandler;
  self.keyboardConstraintHandler = constraintHandler;
  
  NSNotificationCenter *notificationCenter = NSNotificationCenter.defaultCenter;
  // Register for text input notifications
  [notificationCenter addObserver:self selector:@selector(responderDidBecomeActive:) name:UITextFieldTextDidBeginEditingNotification object:nil];
  [notificationCenter addObserver:self selector:@selector(responderDidBecomeActive:) name:UITextViewTextDidBeginEditingNotification object:nil];
  // Register for keyboard notifications
  [notificationCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
  [notificationCenter addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
  [notificationCenter addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
  [notificationCenter addObserver:self selector:@selector(keyboardDidChangeFrame:) name:UIKeyboardDidChangeFrameNotification object:nil];
  [notificationCenter addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
  [notificationCenter addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)removeKeyboardControl {
  NSNotificationCenter *notificationCenter = NSNotificationCenter.defaultCenter;
  // Unregister for text input notifications
  [notificationCenter removeObserver:self name:UITextFieldTextDidBeginEditingNotification object:nil];
  [notificationCenter removeObserver:self name:UITextViewTextDidBeginEditingNotification object:nil];
  // Unregister for keyboard notifications
  [notificationCenter removeObserver:self name:UIKeyboardWillShowNotification object:nil];
  [notificationCenter removeObserver:self name:UIKeyboardDidShowNotification object:nil];
  [notificationCenter removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
  [notificationCenter removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
  [notificationCenter removeObserver:self name:UIKeyboardWillHideNotification object:nil];
  [notificationCenter removeObserver:self name:UIKeyboardDidHideNotification object:nil];
  
  // Unregister any gesture recognizer
  [self removeGestureRecognizer:self.keyboardPanRecognizer];
  
  // Release a few properties
  self.keyboardFrameHandler = nil;
  self.keyboardConstraintHandler = nil;
  self.currentResponder = nil;
  self.keyboardActiveView = nil;
  self.keyboardPanRecognizer = nil;
}

- (void)hideKeyboard {
  if (self.keyboardActiveView) {
    self.keyboardActiveView.hidden = YES;
    self.keyboardActiveView.userInteractionEnabled = NO;
    [self.currentResponder resignFirstResponder];
//    [self endEditing:YES];
  }
}

#pragma mark - Input Notifications

- (void)responderDidBecomeActive:(NSNotification *)notification {
  UIResponder *responder = notification.object;
  self.currentResponder = responder;
  // Grab the active input, it will be used to find the keyboard view later on
  if (!responder.inputAccessoryView) {
    if ([responder respondsToSelector:@selector(setInputAccessoryView:)]) {
      UIView *emptyView = [[UIView alloc] initWithFrame:CGRectZero];
      emptyView.backgroundColor = [UIColor clearColor];
      [(id)responder setInputAccessoryView:emptyView];
    }
  }
  // Force the keyboard active view reset
  [self keyboardDidShow:nil];
}

#pragma mark - Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification {
  NSTimeInterval keyboardAnimationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
  if (keyboardAnimationDuration == 0.0) {
    return;
  }
  
  CGRect keyboardEndFrameView = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
  UIViewAnimationCurve keyboardAnimationCurve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
  
  self.keyboardActiveView.hidden = NO;
  self.keyboardPresenting = YES;
  
  BOOL constraintBasedKeyboardDidMoveBlockCalled = self.keyboardConstraintHandler != nil;
  if (constraintBasedKeyboardDidMoveBlockCalled) {
    self.keyboardConstraintHandler(keyboardEndFrameView, SVKeyboardStatePresenting);
  }
  
  [UIView animateWithDuration:keyboardAnimationDuration
                        delay:0.0
                      options:UIViewAnimationOptions(keyboardAnimationCurve) | UIViewAnimationOptionBeginFromCurrentState
                   animations:^{
//                     if (constraintBasedKeyboardDidMoveBlockCalled)
//                       [self layoutIfNeeded];
                     if (self.keyboardFrameHandler)
                       self.keyboardFrameHandler(keyboardEndFrameView, SVKeyboardStatePresenting);
                   }
                   completion:^(__unused BOOL finished){
                     if (self.isPanning && !self.keyboardPanRecognizer) {
                       // Register for gesture recognizer calls
                       UIPanGestureRecognizer *keyboardPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureDidChange:)];
                       keyboardPanRecognizer.minimumNumberOfTouches = 1;
                       keyboardPanRecognizer.delegate = self;
                       keyboardPanRecognizer.cancelsTouchesInView = NO;
                       [self addGestureRecognizer:keyboardPanRecognizer];
                       self.keyboardPanRecognizer = keyboardPanRecognizer;
                     }
                   }];
}

- (void)keyboardDidShow:(NSNotification *)notification {
  if (notification) {
    NSTimeInterval keyboardAnimationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    if (keyboardAnimationDuration == 0.0) {
      return;
    }
  }
  
  // Grab the keyboard view
  self.keyboardActiveView = [[self class] inputSetHostViewInResponder:self.currentResponder];
  self.keyboardActiveView.hidden = NO;
  
  // If the active keyboard view could not be found (UITextViews...), try again
  if (!self.keyboardActiveView) {
    // Find the first responder on subviews and look re-assign first responder to it
    UIResponder *responder = [[self class] firstResponderInView:self];
    self.currentResponder = responder;
    self.keyboardActiveView = [[self class] inputSetHostViewInResponder:responder];
    self.keyboardActiveView.hidden = NO;
  }
}

- (void)keyboardWillChangeFrame:(NSNotification *)notification {
  CGRect keyboardFrameBegin = [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
  CGRect keyboardFrameEnd = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
  if (CGSizeEqualToSize(keyboardFrameBegin.size, keyboardFrameEnd.size)) {
    return;
  }
  
  NSTimeInterval keyboardAnimationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
  UIViewAnimationCurve keyboardAnimationCurve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
  
  BOOL constraintBasedKeyboardDidMoveBlockCalled = self.keyboardConstraintHandler != nil;
  if (constraintBasedKeyboardDidMoveBlockCalled) {
    self.keyboardConstraintHandler(keyboardFrameEnd, SVKeyboardStateRotating);
  }
  
  [UIView animateWithDuration:keyboardAnimationDuration
                        delay:0.0
                      options:UIViewAnimationOptions(keyboardAnimationCurve) | UIViewAnimationOptionBeginFromCurrentState
                   animations:^{
//                     if (constraintBasedKeyboardDidMoveBlockCalled) {
//                       [self layoutIfNeeded];
//                     }
                     
                     if (self.keyboardFrameHandler) {
                       self.keyboardFrameHandler(keyboardFrameEnd, SVKeyboardStateRotating);
                     }
                   }
                   completion:nil];
}

- (void)keyboardDidChangeFrame:(NSNotification *)notification {
  // Nothing to see here
}

- (void)keyboardWillHide:(NSNotification *)notification {
  NSTimeInterval keyboardAnimationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
  if (keyboardAnimationDuration == 0.0) {
    return;
  }
  
  CGRect keyboardFrameEnd = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
  UIViewAnimationCurve keyboardAnimationCurve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
  
  SVKeyboardFrameChangeHandler keyboardConstraintHandler = self.keyboardConstraintHandler;
  if (keyboardConstraintHandler) {
    keyboardConstraintHandler(keyboardFrameEnd, SVKeyboardStateDismissing);
  }
  
  [UIView animateWithDuration:keyboardAnimationDuration
                        delay:0.0
                      options:UIViewAnimationOptions(keyboardAnimationCurve) | UIViewAnimationOptionBeginFromCurrentState
                   animations:^{
//                     if (keyboardConstraintHandler) {
//                       [self layoutIfNeeded];
//                     }
                     
                     if (self.keyboardFrameHandler) {
                       self.keyboardFrameHandler(keyboardFrameEnd, SVKeyboardStateDismissing);
                     }
                   }
                   completion:^(__unused BOOL finished){
                     // Remove gesture recognizer when keyboard is not showing
                     [self removeGestureRecognizer:self.keyboardPanRecognizer];
                     self.keyboardPanRecognizer = nil;
                   }];
}

- (void)keyboardDidHide:(NSNotification *)notification {
  NSTimeInterval keyboardAnimationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
  if (keyboardAnimationDuration == 0.0) {
    return;
  }
  
  self.keyboardActiveView.hidden = NO;
  self.keyboardActiveView.userInteractionEnabled = YES;
  self.keyboardActiveView = nil;
  self.currentResponder = nil;
  self.keyboardPresenting = NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey, id> *)change context:(void *)context {
  if ([keyPath isEqualToString:@"frame"] && object == self.keyboardActiveView) {
    CGRect keyboardEndFrameView = self.keyboardActiveView.frame;
    if (!self.keyboardActiveView.hidden) {
      if (self.keyboardFrameHandler) {
        self.keyboardFrameHandler(keyboardEndFrameView, SVKeyboardStateDragging);
      }
      if (self.keyboardConstraintHandler) {
        self.keyboardConstraintHandler(keyboardEndFrameView, SVKeyboardStateDragging);
//        [self layoutIfNeeded];
      }
    }
  }
}

#pragma mark - Touches Management

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
  return (gestureRecognizer == self.keyboardPanRecognizer || otherGestureRecognizer == self.keyboardPanRecognizer);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
  if ([touch.view isKindOfClass:[UITextView class]]) {
    UITextView *tv = (UITextView *)touch.view;
    NSLog(@"%@", tv.text);
  }
//  if (gestureRecognizer == self.keyboardPanRecognizer) {
//    // Don't allow panning if inside the active input (unless SELF is a UITextView and the receiving view)
//    return (touch.view.isFirstResponder || ([self isKindOfClass:[UITextView class]] && [self isEqual:touch.view]));
//  } else {
//    return YES;
//  }
  return YES;
}

- (void)panGestureDidChange:(UIPanGestureRecognizer *)panGestureRecognizer {
  if (!self.keyboardActiveView || !self.currentResponder || self.keyboardActiveView.isHidden) {
    UIResponder *responder = [[self class] firstResponderInView:self];
    self.currentResponder = responder;
    self.keyboardActiveView = [[self class] inputSetHostViewInResponder:responder];
    self.keyboardActiveView.hidden = NO;
  } else {
    self.keyboardActiveView.hidden = NO;
  }
  
  CGFloat keyboardViewHeight = self.keyboardActiveView.bounds.size.height;
  CGFloat keyboardWindowHeight = self.keyboardActiveView.superview.bounds.size.height;
  CGPoint touchLocationInKeyboardWindow = [panGestureRecognizer locationInView:self.keyboardActiveView.superview];
  
  // If touch is inside trigger offset, then disable keyboard input
  if (touchLocationInKeyboardWindow.y > (keyboardWindowHeight - keyboardViewHeight - self.keyboardTriggerOffset)) {
    self.keyboardActiveView.userInteractionEnabled = NO;
  } else {
    self.keyboardActiveView.userInteractionEnabled = YES;
  }
  switch (panGestureRecognizer.state) {
    case UIGestureRecognizerStateBegan: {
      // For the duration of this gesture, do not recognize more touches than
      // it started with
      panGestureRecognizer.maximumNumberOfTouches = panGestureRecognizer.numberOfTouches;
    }
      break;
    case UIGestureRecognizerStateChanged: {
      CGRect newKeyboardViewFrame = self.keyboardActiveView.frame;
      newKeyboardViewFrame.origin.y = touchLocationInKeyboardWindow.y + self.keyboardTriggerOffset;
      // Bound the keyboard to the bottom of the screen
      newKeyboardViewFrame.origin.y = MIN(newKeyboardViewFrame.origin.y, keyboardWindowHeight);
      newKeyboardViewFrame.origin.y = MAX(newKeyboardViewFrame.origin.y, keyboardWindowHeight - keyboardViewHeight);
      
      // Only update if the frame has actually changed
      if (newKeyboardViewFrame.origin.y != self.keyboardActiveView.frame.origin.y) {
        [UIView animateWithDuration:0.0
                              delay:0.0
                            options:UIViewAnimationOptionTransitionNone | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                           self.keyboardActiveView.frame = newKeyboardViewFrame;
                         }
                         completion:nil];
      }
    }
      break;
    case UIGestureRecognizerStateEnded:
    case UIGestureRecognizerStateCancelled: {
      CGFloat thresholdHeight = keyboardWindowHeight - keyboardViewHeight - self.keyboardTriggerOffset + 44.0;
      CGPoint velocity = [panGestureRecognizer velocityInView:self.keyboardActiveView];
      BOOL shouldRecede;
      
      if (touchLocationInKeyboardWindow.y < thresholdHeight || velocity.y < 0) {
        shouldRecede = NO;
      } else {
        shouldRecede = YES;
      }
      
      // If the keyboard has only been pushed down 44 pixels or has been
      // panned upwards let it pop back up; otherwise, let it drop down
      CGRect newKeyboardViewFrame = self.keyboardActiveView.frame;
      newKeyboardViewFrame.origin.y = (!shouldRecede ? keyboardWindowHeight - keyboardViewHeight : keyboardWindowHeight);
      if (CGRectEqualToRect(newKeyboardViewFrame, self.keyboardActiveView.frame)) {
        return;
      }
      
      [UIView animateWithDuration:0.25
                            delay:0.0
                          options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                       animations:^{
                         self.keyboardActiveView.frame = newKeyboardViewFrame;
                       }
                       completion:^(__unused BOOL finished){
                         self.keyboardActiveView.userInteractionEnabled = !shouldRecede;
                         if (shouldRecede) {
                           [self hideKeyboard];
                         }
                       }];
      
      // Set the max number of touches back to the default
      panGestureRecognizer.maximumNumberOfTouches = NSUIntegerMax;
    }
      break;
    default:
      break;
  }
}

#pragma mark - Internal Methods

- (void)swizzled_addSubview:(UIView *)subview {
  if (!subview.inputAccessoryView) {
    if ([subview isKindOfClass:[UITextField class]]) {
      UITextField *textField = (UITextField *)subview;
      UIView *nullView = [[UIView alloc] initWithFrame:CGRectZero];
      nullView.backgroundColor = [UIColor clearColor];
      textField.inputAccessoryView = nullView;
    }
    else if ([subview isKindOfClass:[UITextView class]]) {
      UITextView *textView = (UITextView *)subview;
      if (textView.isEditable) {
        UIView *nullView = [[UIView alloc] initWithFrame:CGRectZero];
        nullView.backgroundColor = [UIColor clearColor];
        textView.inputAccessoryView = nullView;
      }
    }
  }
  [self swizzled_addSubview:subview];
}

#pragma mark - Property Methods

- (CGRect)keyboardFrameInView {
  if (self.keyboardActiveView) {
    return self.keyboardActiveView.frame;
  } else {
    return CGRectMake(0.0, UIScreen.mainScreen.bounds.size.height, 0.0, 0.0);
  }
}

- (SVKeyboardFrameChangeHandler)keyboardFrameHandler {
  return objc_getAssociatedObject(self, @selector(keyboardFrameHandler));
}

- (void)setKeyboardFrameHandler:(SVKeyboardFrameChangeHandler)keyboardFrameHandler {
  [self willChangeValueForKey:@"keyboardConstraintHandler"];
  objc_setAssociatedObject(self, @selector(keyboardFrameHandler), keyboardFrameHandler, OBJC_ASSOCIATION_COPY);
  [self didChangeValueForKey:@"keyboardConstraintHandler"];
}

- (SVKeyboardFrameChangeHandler)keyboardConstraintHandler {
  return objc_getAssociatedObject(self, @selector(keyboardConstraintHandler));
}

- (void)setKeyboardConstraintHandler:(SVKeyboardFrameChangeHandler)keyboardConstraintHandler {
  [self willChangeValueForKey:@"keyboardConstraintHandler"];
  objc_setAssociatedObject(self, @selector(keyboardConstraintHandler), keyboardConstraintHandler, OBJC_ASSOCIATION_COPY);
  [self didChangeValueForKey:@"keyboardConstraintHandler"];
}

- (CGFloat)keyboardTriggerOffset {
  return [objc_getAssociatedObject(self, @selector(keyboardTriggerOffset)) doubleValue];
}

- (void)setKeyboardTriggerOffset:(CGFloat)keyboardTriggerOffset {
  [self willChangeValueForKey:@"keyboardTriggerOffset"];
  objc_setAssociatedObject(self, @selector(keyboardTriggerOffset), @((double)keyboardTriggerOffset), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  [self didChangeValueForKey:@"keyboardTriggerOffset"];
}

- (BOOL)isPanning {
  return [objc_getAssociatedObject(self, @selector(isPanning)) boolValue];
}

- (void)setPanning:(BOOL)panning {
  [self willChangeValueForKey:@"panning"];
  objc_setAssociatedObject(self, @selector(isPanning), @(panning), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  [self didChangeValueForKey:@"panning"];
}

- (UIResponder *)currentResponder {
  return objc_getAssociatedObject(self, @selector(currentResponder));
}

- (void)setCurrentResponder:(UIResponder *)currentResponder {
  [self willChangeValueForKey:@"currentResponder"];
  objc_setAssociatedObject(self, @selector(currentResponder), currentResponder, OBJC_ASSOCIATION_ASSIGN);
  [self didChangeValueForKey:@"currentResponder"];
}

- (UIView *)keyboardActiveView {
  return objc_getAssociatedObject(self, @selector(keyboardActiveView));
}

- (void)setKeyboardActiveView:(UIView *)keyboardActiveView {
  [self willChangeValueForKey:@"keyboardActiveView"];
  [self.keyboardActiveView removeObserver:self forKeyPath:@"frame"];
  if (keyboardActiveView) {
    [keyboardActiveView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:NULL];
  }
  objc_setAssociatedObject(self, @selector(keyboardActiveView), keyboardActiveView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  [self didChangeValueForKey:@"keyboardActiveView"];
}

- (UIPanGestureRecognizer *)keyboardPanRecognizer {
  return objc_getAssociatedObject(self, @selector(keyboardPanRecognizer));
}

- (void)setKeyboardPanRecognizer:(UIPanGestureRecognizer *)keyboardPanRecognizer {
  [self willChangeValueForKey:@"keyboardPanRecognizer"];
  objc_setAssociatedObject(self, @selector(keyboardPanRecognizer), keyboardPanRecognizer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  [self didChangeValueForKey:@"keyboardPanRecognizer"];
}

- (BOOL)isKeyboardPresenting {
  return [objc_getAssociatedObject(self, @selector(isKeyboardPresenting)) boolValue];
}

- (void)setKeyboardPresenting:(BOOL)keyboardPresenting {
  [self willChangeValueForKey:@"keyboardPresenting"];
  objc_setAssociatedObject(self, @selector(isKeyboardPresenting), @(keyboardPresenting), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  [self didChangeValueForKey:@"keyboardPresenting"];
}

- (BOOL)keyboardWillRecede {
  CGFloat keyboardViewHeight = self.keyboardActiveView.bounds.size.height;
  CGFloat keyboardWindowHeight = self.keyboardActiveView.superview.bounds.size.height;
  CGPoint touchLocationInKeyboardWindow = [self.keyboardPanRecognizer locationInView:self.keyboardActiveView.superview];
  
  CGFloat thresholdHeight = keyboardWindowHeight - keyboardViewHeight - self.keyboardTriggerOffset + 44.0f;
  CGPoint velocity = [self.keyboardPanRecognizer velocityInView:self.keyboardActiveView];
  
  return touchLocationInKeyboardWindow.y >= thresholdHeight && velocity.y >= 0;
}

#pragma mark - Internal Class Methods

+ (UIView *)firstResponderInView:(UIView *)view {
  if (view.isFirstResponder) {
    return view;
  }
  UIView *found = nil;
  for (UIView *subview in view.subviews) {
    found = [self firstResponderInView:subview];
    if (found) {
      break;
    }
  }
  return found;
}

+ (UIView *)inputSetHostViewInResponder:(UIResponder *)responder {
  if (UIDevice.currentDevice.systemVersion.floatValue >= 9.0f) {
    for (UIWindow *window in UIApplication.sharedApplication.windows) {
      if ([window isKindOfClass:NSClassFromString(@"UIRemoteKeyboardWindow")]) {
        for (UIView *subView in window.subviews) {
          if ([subView isKindOfClass:NSClassFromString(@"UIInputSetContainerView")]) {
            for (UIView *subsubView in subView.subviews) {
              if ([subsubView isKindOfClass:NSClassFromString(@"UIInputSetHostView")])
                return subsubView;
            }
          }
        }
      }
    }
  } else {
    return responder.inputAccessoryView.superview;
  }
  return nil;
}

@end
