//
//  StampCollectionViewCell.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StampCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UILabel *numberLabel;
@property (nonatomic, strong, readonly) UIImageView *bucketImageView;
@property (nonatomic, strong, readonly) UIImageView *thankYouImageView;

@end
