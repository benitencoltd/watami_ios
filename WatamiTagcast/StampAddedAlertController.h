//
//  StampAddedAlertController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/21/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StampAddedAlertController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

- (IBAction)didTapOk:(UIButton *)sender;

@end
