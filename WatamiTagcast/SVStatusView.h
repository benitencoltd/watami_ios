//
//  SVStatusView.h
//  SVKit
//
//  Created by Sereivoan Yong on 3/31/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SVStatusViewState) {
  SVStatusViewStateNone      = 0,
  SVStatusViewStateAnimating = 1,
  SVStatusViewStateEmpty     = 2,
  SVStatusViewStateError     = 3,
} NS_SWIFT_NAME(SVStatusView.State);

typedef NS_ENUM(NSUInteger, SVStatusViewStyle) {
  SVStatusViewStyleBlack = 0, // Good against white background
  SVStatusViewStyleWhite = 1, // Good against black background
} NS_SWIFT_NAME(SVStatusView.Style);

@interface SVStatusView : UIView

// These 4 properties are created lazily.
@property (nonatomic, strong, readonly) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong, readonly) UILabel *titleLabel; // numberOfLines is 1. width = 1:3 super
@property (nonatomic, strong, readonly) UILabel *subtitleLabel; // numberOfLines is 2. width = 1:2 super

@property (nonatomic, assign) SVStatusViewState state;
@property (nonatomic, assign) SVStatusViewStyle style; // Default to black.
@property (nonatomic, assign) UIEdgeInsets insets;
@property (nonatomic, copy) void (^reloadHandler)(void); // Only called when user taps on imageView and state is error.

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setImage:(UIImage *)image forState:(SVStatusViewState)state;
- (void)setTitle:(NSString *)title forState:(SVStatusViewState)state;
- (void)setSubtitle:(NSString *)subtitle forState:(SVStatusViewState)state;

- (nullable UIImage *)imageForState:(SVStatusViewState)state;
- (nullable NSString *)titleForState:(SVStatusViewState)state;
- (nullable NSString *)subtitleForState:(SVStatusViewState)state;

- (void)setStateFromObject:(id)object; // error if nil, empty if object is array and is empty, none if non-nil.

@end

NS_ASSUME_NONNULL_END
