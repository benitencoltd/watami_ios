//
//  MenuItemCategoryCollectionViewCell.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/10/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuItemCategoryCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong, readonly) UILabel *nameLabel;

+ (CGFloat)itemHeight;

@end
