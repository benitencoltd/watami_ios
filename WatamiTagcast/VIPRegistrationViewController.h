//
//  VIPRegistrationViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/27/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ACFloatingTextfield/ACFloatingTextField.h>
#import "WTProfile.h"
#import "WTVIPInfo.h"

@interface VIPRegistrationViewController : UIViewController

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topScrollViewConstraint, *bottomScrollViewConstraint;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIView *contentView;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *photoImageView;
@property (nonatomic, weak) IBOutlet UIButton *uploadButton;
@property (nonatomic, weak) IBOutlet UILabel *genderTitleLabel, *dobTitleLabel;
@property (nonatomic, weak) IBOutlet ACFloatingTextField *usernameTextField;
@property (nonatomic, weak) IBOutlet ACFloatingTextField *emailTextField;
@property (nonatomic, weak) IBOutlet ACFloatingTextField *dobYearTextField, *dobMonthTextField, *dobDayTextField;
@property (nonatomic, weak) IBOutlet UIButton *mCheckBoxButton, *fCheckBoxButton;
@property (nonatomic, weak) IBOutlet UILabel *mLabel, *fLabel;

@property (nonatomic, weak) IBOutlet UIButton *saveButton;

@property (nonatomic, strong, readonly) WTProfile *profile; // Non-nil if registration is succeeded or is in `edits` mode.

- (instancetype)initWithProfile:(WTProfile *)profile vipInfo:(WTVIPInfo *)vipInfo; // Pass nil to create new, otherwise update (all textFields are filled with given pofile info respectively).

- (IBAction)selectGender:(UITapGestureRecognizer *)gestureRecognizer;
- (IBAction)uploadPhoto:(UIButton *)sender;
- (IBAction)editProfile:(UIButton *)sender;
- (IBAction)didTapView:(UITapGestureRecognizer *)gestureRecognizer; // to end editing when user taps.

@end
