//
//  HomeViewController.m
//  WatamiTagcast
//
//  Created by Mr. Haruhiro Sato on 1/4/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <TAKUUID/TAKUUIDStorage.h>
#import "HomeViewController.h"
#import "VIPRegistrationViewController.h"
#import "VIPProfileViewController.h"
#import "AllVIPUsersViewController.h"
#import "RankUpgradeAlertController.h"

#import "HomeRankingItemCollectionViewCell.h"

#import "WTProfile.h"
#import "WTVIPInfo.h"
#import "Constants.h"

#define kSpacing 9.0
#define kRankingItemSpacing 8.0
#define kRankingItemTitleFontSize 13.0

@interface HomeViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UIView *topImageContainerView;
@property (nonatomic, strong) UIImageView *topImageView;

@property (nonatomic, strong) UIView *rankingContainerView;
@property (nonatomic, strong) UILabel *rankTitleLabel;
@property (nonatomic, strong) UIButton *rankingViewAllButton;
@property (nonatomic, strong) UICollectionView *rankingCollectionView;

@property (nonatomic, strong) WTProfile *profile;
@property (nonatomic, strong) WTVIPInfo *vipInfo;

@end

@implementation HomeViewController {
  NSURLSessionDataTask *_profileTask, *_vipInfoTask;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [_vipInfoTask cancel];
  WTDeallocLog(self);
}

#pragma mark -

- (void)viewDidLoad {
  [super viewDidLoad];
  
  CGFloat contentWidth = [UIScreen mainScreen].bounds.size.width - 18.0;
  
  _rankingContainerView = [[UIView alloc] init];
  _rankingContainerView.backgroundColor = [UIColor whiteColor];
  [self.view addSubview:_rankingContainerView];
  
  UIFont *rankTitleFont = [UIFont appFontOfSize:16.0];
  CGFloat rankTitleHeight = ceil(rankTitleFont.lineHeight);
  
  _rankingViewAllButton = [UIButton buttonWithType:UIButtonTypeSystem];
  _rankingViewAllButton.tintColor = [UIColor blackColor];
  _rankingViewAllButton.titleLabel.font = [UIFont appFontOfSize:14.0];
  [_rankingViewAllButton setTitle:NSLocalizedString(@"view_rank", nil) forState:UIControlStateNormal];
  [_rankingViewAllButton sizeToFit];
  CGRect viewAllRect = _rankingViewAllButton.frame;
  viewAllRect.origin = CGPointMake(contentWidth - viewAllRect.size.width - 8.0, kRankingItemSpacing);
  viewAllRect.size.height = rankTitleHeight;
  _rankingViewAllButton.frame = viewAllRect;
  [_rankingViewAllButton addTarget:self action:@selector(showVIPUsers:) forControlEvents:UIControlEventTouchUpInside];
  [_rankingContainerView addSubview:_rankingViewAllButton];
  
  _rankTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kRankingItemSpacing, kRankingItemSpacing, CGRectGetMinX(_rankingViewAllButton.frame) - kRankingItemSpacing * 2, rankTitleHeight)];
  _rankTitleLabel.font = rankTitleFont;
  [_rankingContainerView addSubview:_rankTitleLabel];
  
  //
  CGFloat rankingItemWidth = (contentWidth - kRankingItemSpacing * 5) / 4;
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.minimumLineSpacing = kRankingItemSpacing;
  flowLayout.itemSize = CGSizeMake(rankingItemWidth, rankingItemWidth + 4.0 + ceil([UIFont appFontOfSize:kRankingItemTitleFontSize].lineHeight));
  flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
  flowLayout.sectionInset = UIEdgeInsetsMake(0.0, kRankingItemSpacing, 0.0, kRankingItemSpacing);
  
  CGFloat rankingHeight = kRankingItemSpacing + _rankTitleLabel.frame.size.height + 4.0 + flowLayout.itemSize.height + kRankingItemSpacing;
  _rankingCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0.0, CGRectGetMaxY(_rankTitleLabel.frame) + 4.0, contentWidth, flowLayout.itemSize.height) collectionViewLayout:flowLayout];
  _rankingCollectionView.alwaysBounceHorizontal = YES;
  _rankingCollectionView.backgroundColor = [UIColor whiteColor];
  _rankingCollectionView.showsHorizontalScrollIndicator = NO;
  _rankingCollectionView.showsVerticalScrollIndicator = NO;
  [_rankingCollectionView registerClass:[HomeVIPItemCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([HomeVIPItemCollectionViewCell class])];
  [_rankingCollectionView registerClass:[HomeRankingItemCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([HomeRankingItemCollectionViewCell class])];
  _rankingCollectionView.dataSource = self;
  _rankingCollectionView.delegate = self;
  [_rankingContainerView addSubview:_rankingCollectionView];
  
  // Section Height
  CGFloat remainedHeight = self.view.frame.size.height - CGRectGetMaxY(self.navigationController.navigationBar.frame) - self.tabBarController.tabBar.frame.size.height - rankingHeight - 50.0;
  CGFloat fractionalHeight = remainedHeight / 4.6;
  CGFloat topImageHeight = fractionalHeight * 2.2;
  CGFloat menuItemHeight = fractionalHeight * 1.2;
  
  // Top Image
  _topImageContainerView = [[UIView alloc] initWithFrame:CGRectMake(9.0, 10.0, contentWidth, topImageHeight)];
  _topImageContainerView.backgroundColor = [UIColor whiteColor];
  _topImageContainerView.layer.cornerRadius = 4.0;
  [self.view addSubview:_topImageContainerView];
  AddShadowToView(_topImageContainerView);
  
  _topImageView = [[UIImageView alloc] initWithFrame:_topImageContainerView.bounds];
  _topImageView.clipsToBounds = YES;
  _topImageView.contentMode = UIViewContentModeScaleAspectFill;
  _topImageView.layer.cornerRadius = 4.0;
  _topImageView.image = [UIImage imageNamed:@"header_image.png"];
  [_topImageContainerView addSubview:_topImageView];
  
  _rankingContainerView.frame = CGRectMake(9.0, CGRectGetMaxY(_topImageContainerView.frame) + 10.0, contentWidth, rankingHeight);
  _rankingContainerView.layer.cornerRadius = 4.0;
  AddShadowToView(_rankingContainerView);
  
  // Menu
  NSArray<NSString *> *menuItemImageNames = @[@"ic_menu.png", @"ic_coupon.png", @"ic_message.png", @"ic_location.png"];
  NSArray<NSString *> *menuItemTitles = @[NSLocalizedString(@"menu", nil), NSLocalizedString(@"coupon", nil), NSLocalizedString(@"message", nil), NSLocalizedString(@"location", nil)];
  CGFloat menuItemWidth = (self.view.frame.size.width - 27.0) / 2;
  int k = 0;
  for (int i = 0; i < 2; i++) { // row
    for (int j = 0; j < 2; j++) { // col
      k++;
      UIControl *control = [[UIControl alloc] initWithFrame:CGRectMake(9.0 + (menuItemWidth + 9.0) * (CGFloat)j, CGRectGetMaxY(_rankingContainerView.frame) + 10.0 + (menuItemHeight + 10.0) * (CGFloat)i, menuItemWidth, menuItemHeight)];
      control.backgroundColor = DARK_RED_COLOR;
      control.tag = k;
      control.layer.cornerRadius = 4.0;
      [control addTarget:self action:@selector(didTapMenuItem:) forControlEvents:UIControlEventTouchUpInside];
      [self.view addSubview:control];
      AddShadowToView(control);
      
      UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectInset(control.bounds, 10.0, 10.0)];
      imageView.alpha = 0.2;
      imageView.contentMode = UIViewContentModeScaleAspectFit;
      imageView.image = [UIImage imageNamed:menuItemImageNames[k - 1]];
      [control addSubview:imageView];
      
      UILabel *label = [[UILabel alloc] initWithFrame:control.bounds];
      label.font = [UIFont appFontOfSize:20.0];
      label.textAlignment = NSTextAlignmentCenter;
      label.textColor = [UIColor whiteColor];
      label.text = menuItemTitles[k - 1];
      [control addSubview:label];
    }
  }
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidCreateOrUpdateProfile:) name:WTUserDidCreateOrUpdateProfileNotificationName object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [Common trackScreen:@"top_screen"];
  if ([[NSUserDefaults standardUserDefaults] stringForKey:kUserAccessTokenKey]) {
    [self reloadVIPInfo];
  }
}

#pragma mark -

- (void)showVIPUsers:(UIButton *)sender {
  AllVIPUsersViewController *usersViewController = [[AllVIPUsersViewController alloc] init];
  [self.navigationController pushViewController:usersViewController animated:YES];
}

- (void)didTapMenuItem:(UIControl *)sender {
  self.tabBarController.selectedIndex = sender.tag;
}

- (void)userDidCreateOrUpdateProfile:(NSNotification *)notification {
  WTProfile *profile = notification.object;
  if (profile) {
    _profile = profile;
    [_rankingCollectionView reloadData];
  }
}

- (void)reloadProfile {
  [_profileTask cancel];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getmemberprofile",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _profileTask = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(HomeViewController);
    JSON(data)
    if (json[@"data"]) {
      strongSelf.profile = [[WTProfile alloc] initWithDictionary:json[@"data"] error:NULL];
    } else {
      WTLog(@"Unable to retrieve user's profile.");
    }
  }];
  [_profileTask resume];
}

- (void)reloadVIPInfo {
  [_vipInfoTask cancel];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getStatusUser",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _vipInfoTask = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(HomeViewController);
    JSON(data)
    if (json[@"data"]) {
      WTVIPInfo *vipInfo = [[WTVIPInfo alloc] initWithDictionary:json[@"data"] error:NULL];
      strongSelf.vipInfo = vipInfo;
      NSInteger currentRankType = [[NSUserDefaults standardUserDefaults] integerForKey:kUserRankTypeKey];
      if (currentRankType != 0 && currentRankType != vipInfo.currentRank.type) {
        dispatch_async(dispatch_get_main_queue(), ^{
          RankUpgradeAlertController *alertController = [[RankUpgradeAlertController alloc] initWithCurrentRank:vipInfo.currentRank previousRank:vipInfo.previousRank];
          [strongSelf.tabBarController presentViewController:alertController animated:YES completion:nil];
        });
      }
      [[NSUserDefaults standardUserDefaults] setInteger:vipInfo.currentRank.type forKey:kUserRankTypeKey];
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf.rankingCollectionView reloadData];
        strongSelf.rankTitleLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"rank", nil), vipInfo.currentRank.title.uppercaseString];
      });
      if (strongSelf.vipInfo.isRegistered) {
        [strongSelf reloadProfile];
      }
    } else {
      
    }
  }];
  [_vipInfoTask resume];
}

- (UIColor *)colorFromTicketType:(NSString *)type {
  type = type.lowercaseString;
  if ([type isEqualToString:@"gold"]) {
    return [UIColor colorWithRed:1.0 green:171.0/255.0 blue:0.0 alpha:1.0];
  } else if ([type isEqualToString:@"silver"]) {
    return [UIColor colorWithRed:120.0/255.0 green:144.0/255.0 blue:156.0/255.0 alpha:1.0];
  } else if ([type isEqualToString:@"bronze"]) {
    return [UIColor colorWithRed:78.0/255.0 green:52.0/255.0 blue:46.0/255.0 alpha:1.0];
  } else if ([type isEqualToString:@"lose"]) {
    return [UIColor colorWithRed:103.0/255.0 green:58.0/255.0 blue:183.0/255.0 alpha:1.0];
  } else {
    return [UIColor whiteColor];
  }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return 7;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.item == 0) {
    HomeVIPItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([HomeVIPItemCollectionViewCell class]) forIndexPath:indexPath];
    cell.circleImageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if (_vipInfo && _vipInfo.registered) {
      [cell.circleButton setImage:[[UIImage imageNamed:@"user_edit.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    } else {
      [cell.circleButton setImage:[[UIImage imageNamed:@"user_add.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
    UIImage *placeholderImage = [[[UIImage imageNamed:@"user_placeholder.png"] imageWithInsetPercent:0.75] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    if (_vipInfo.photoURL) {
      [cell.circleImageView sd_setImageWithURL:_vipInfo.photoURL placeholderImage:placeholderImage];
    } else {
      cell.circleImageView.image = placeholderImage;
    }
    cell.titleLabel.text = _vipInfo.isVIP ? @"VIP" : NSLocalizedString(@"profile", nil);
    WTWeakify(self);
    cell.actionHandler = ^{
      WTStrongify(HomeViewController);
      VIPRegistrationViewController *vipRegistrationViewController = [[VIPRegistrationViewController alloc] initWithProfile:strongSelf.profile vipInfo:_vipInfo];
      [strongSelf.navigationController pushViewController:vipRegistrationViewController animated:YES];
    };
    return cell;
  } else {
    HomeRankingItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([HomeRankingItemCollectionViewCell class]) forIndexPath:indexPath];
    if (indexPath.item == [collectionView numberOfItemsInSection:0] - 1) {
      cell.circleView.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:121.0/255.0 blue:1.0 alpha:1.0];
      cell.countLabel.text = [NSString stringWithFormat:@"%u", (unsigned)_vipInfo.numberOfIncentiveTickets];
      cell.titleLabel.text = NSLocalizedString(@"incentive", nil);
    } else if (indexPath.item == [collectionView numberOfItemsInSection:0] - 2) {
      cell.circleView.backgroundColor = [UIColor colorWithRed:197.0/255.0 green:17.0/255.0 blue:98.0/255.0 alpha:1.0];
      cell.countLabel.text = [NSString stringWithFormat:@"%u", (unsigned)_vipInfo.numberOfStamps];
      cell.titleLabel.text = NSLocalizedString(@"stamp", nil);
    } else {
      NSInteger index = indexPath.item - 1;
      if (_vipInfo) {
        WTVIPInfoTicketInfo *ticketInfo = _vipInfo.ticketInfos[index];
        cell.circleView.backgroundColor = [self colorFromTicketType:ticketInfo.type];
        cell.countLabel.text = [NSString stringWithFormat:@"%u", (unsigned)ticketInfo.count];
        cell.titleLabel.text = NSLocalizedString(ticketInfo.type.lowercaseString, nil);
      } else {
        NSArray<NSString *> *types = @[@"gold", @"silver", @"bronze", @"lose"];
        cell.circleView.backgroundColor = [self colorFromTicketType:types[index]];
        cell.countLabel.text = @"0";
        cell.titleLabel.text = NSLocalizedString(types[index], nil);
      }
    }
    return cell;
  }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.item == 0 && _vipInfo.isRegistered) {
    VIPProfileViewController *vipProfileViewController = [[VIPProfileViewController alloc] initWithProfile:_profile vipInfo:_vipInfo];
    [self.tabBarController presentViewController:vipProfileViewController animated:YES completion:nil];
  }
}

@end
