//
//  RankingTableViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/21/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "RankingTableViewCell.h"

#define NUMBER_LABEL_WIDTH 26.0

@implementation RankingTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    _numberLabel = [[UILabel alloc] init];
    _numberLabel.backgroundColor = DARK_RED_COLOR;
    _numberLabel.layer.cornerRadius = NUMBER_LABEL_WIDTH / 2;
    _numberLabel.layer.masksToBounds = YES;
    _numberLabel.textAlignment = NSTextAlignmentCenter;
    _numberLabel.textColor = [UIColor whiteColor];
    _numberLabel.font = [UIFont appFontOfSize:14.0];
    [self.contentView addSubview:_numberLabel];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont appFontOfSize:15.0];
    [self.contentView addSubview:_titleLabel];
    
    _countLabel = [[UILabel alloc] init];
    _countLabel.font = [UIFont appFontOfSize:15.0];
    _countLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_countLabel];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  _numberLabel.frame = CGRectMake(12.0, (self.contentView.frame.size.height - NUMBER_LABEL_WIDTH) / 2, NUMBER_LABEL_WIDTH, NUMBER_LABEL_WIDTH);
  [_countLabel sizeToFit];
  CGSize countSize = _countLabel.frame.size;
  _countLabel.frame = CGRectMake(self.contentView.frame.size.width - countSize.width - 12.0, (self.contentView.frame.size.height - countSize.height) / 2, countSize.width, countSize.height);
  CGFloat titleHeight = ceil(_titleLabel.font.lineHeight);
  _titleLabel.frame = CGRectMake(CGRectGetMaxX(_numberLabel.frame) + 12.0, (self.contentView.frame.size.height - titleHeight) / 2, CGRectGetMinX(_countLabel.frame) - 24.0 - CGRectGetMaxX(_numberLabel.frame), titleHeight);
}

@end
