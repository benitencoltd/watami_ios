//
//  Constants.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/7/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const kWatamiAppId = @"1146431324";
static NSString * const kWatamiAppIdPrefix = @"H43882HRT3";
static NSString * const kWatamiTitle = @"Watami Japanese Dining Cambodia";

static NSString * const kGMSServicesAPIKey = @"AIzaSyBhkVdqzb1wPJp6CvMnMEfN58vzImcCjnM";

//static NSString * const kWatamiAPIURLString = @"http://dev-tagcast-watami.beniten.com/api";
//static NSString * const kTagcastAPIKey = @"f8ea7419c7a6108fc2af26724cdaa01e";
//static NSString * const kOneSignalAppId = @"719bf36c-0ee4-4a35-8f33-2392a1b9fd05";

static NSString * const kWatamiAPIURLString = @"http://tagcast-watami.beniten.com/api";
static NSString * const kTagcastAPIKey = @"79fb3ee85caf254d2d7948e5ae3d95ad";
static NSString * const kOneSignalAppId = @"c9a3bfeb-8f7e-4323-bc57-195ebcd0aa85";

static NSString * const kUserAccessTokenKey = @"access_token";
static NSString * const kUserLanguageKey = @"language";
static NSString * const kUserRankTypeKey = @"rank_type";

static NSString * const kOneSignalLanguageValueToSendKey = @"onesignal_language_value_to_send"; // In case user changes language while not connected to the internet or other than that.

static NSNotificationName const WTUserDidCreateOrUpdateProfileNotificationName = @"WTUserDidCreateOrUpdateProfileNotificationName";
