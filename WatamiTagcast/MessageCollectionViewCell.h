//
//  MessageCollectionViewCell.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBShareButton.h"
#import "WTMessage.h"

@interface MessageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong, readonly) UILabel *textLabel;
@property (nonatomic, strong, readonly) UILabel *dateLabel;
@property (nonatomic, strong, readonly) FBShareButton *shareButton;

@property (nonatomic, copy) void (^imageTapHandler)(void);
@property (nonatomic, copy) void (^shareHandler)(void);

+ (CGFloat)itemHeightForMessage:(WTMessage *)message width:(CGFloat)width;

@end
