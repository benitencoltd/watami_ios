//
//  Utility.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/3/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "Utility.h"

@implementation UIViewController (Util)

- (BOOL)allowsUserInteraction {
  return self.tabBarController.tabBar.userInteractionEnabled || self.navigationController.navigationBar.userInteractionEnabled;
}

- (void)setAllowsUserInteraction:(BOOL)allowsUserInteraction {
  self.tabBarController.tabBar.userInteractionEnabled = allowsUserInteraction;
  self.navigationController.navigationBar.userInteractionEnabled = allowsUserInteraction;
}

- (BOOL)isVisible {
  return self.isViewLoaded && self.view.window != nil;
}

- (UINavigationController *)embedInNavigationController {
  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self];
  navigationController.modalPresentationStyle = self.modalPresentationStyle;
  navigationController.modalTransitionStyle = self.modalTransitionStyle;
  return navigationController;
}

@end

@implementation UINavigationBar (Utils)

- (void)removeBackBarButtonItemTitle {
  self.topItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

@end

@implementation UIButton (Utils)

- (void)addBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius {
  self.layer.borderColor = borderColor.CGColor;
  self.layer.borderWidth = borderWidth;
  self.layer.cornerRadius = cornerRadius;
  self.layer.masksToBounds = YES;
  self.layer.rasterizationScale = [UIScreen mainScreen].scale;
  self.layer.shouldRasterize = YES;
  
  self.imageView.backgroundColor = self.backgroundColor;
  self.imageView.layer.cornerRadius = cornerRadius - borderWidth;
  self.imageView.layer.masksToBounds = YES;
  
  self.imageEdgeInsets = UIEdgeInsetsMake(borderWidth, borderWidth, borderWidth, borderWidth);
  self.backgroundColor = borderColor;
}

@end

@implementation NSArray (Map)

- (NSArray *)objectsByTransform:(id (^)(id))transform {
  NSMutableArray *result = [NSMutableArray arrayWithCapacity:self.count];
  for (id obj in self) {
    [result addObject:transform(obj)];
  }
  return result;
}

- (id)objectByReduceTo:(id)initial reduce:(id (^)(id, id))reduce {
  for (id obj in self) {
    initial = reduce(initial, obj);
  }
  return initial;
}

@end

@implementation UIImage (Utils)

- (UIImage *)imageWithInsetPercent:(CGFloat)percent {
  UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0);
  CGSize newSize = CGSizeMake(self.size.width * percent, self.size.height * percent);
  [self drawInRect:CGRectMake((self.size.width - newSize.width) / 2, (self.size.height - newSize.height) / 2, newSize.width, newSize.height)];
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return newImage;
}

- (UIImage *)imageByResizingToSize:(CGSize)size {
  UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
  [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return newImage;
}

- (UIImage *)imageByScalingAspectFitSize:(CGSize)size {
  CGFloat oldWidth = self.size.width;
  CGFloat oldHeight = self.size.height;
  
  CGFloat scaleFactor = (oldWidth > oldHeight) ? size.width / oldWidth : size.height / oldHeight;
  
  CGFloat newHeight = oldHeight * scaleFactor;
  CGFloat newWidth = oldWidth * scaleFactor;
  
  return [self imageByResizingToSize:CGSizeMake(newWidth, newHeight)];
}

- (UIImage *)imageWithSize:(CGSize)size cornerRadius:(CGFloat)cornerRadius {
  CGRect rect = CGRectMake(0.0, 0.0, size.width, size.height);
  UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextAddPath(context, [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:cornerRadius].CGPath);
  CGContextDrawImage(context, rect, self.CGImage);
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

- (UIImage *)imageWithTintColor:(UIColor *)tintColor {
  // lets tint the icon - assumes your icons are black
  UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0);
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  CGContextTranslateCTM(context, 0, self.size.height);
  CGContextScaleCTM(context, 1.0, -1.0);
  
  CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
  
  // draw alpha-mask
  CGContextSetBlendMode(context, kCGBlendModeNormal);
  CGContextDrawImage(context, rect, self.CGImage);
  
  // draw tint color, preserving alpha values of original image
  CGContextSetBlendMode(context, kCGBlendModeSourceIn);
  [tintColor setFill];
  CGContextFillRect(context, rect);
  
  UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return coloredImage;
}

+ (UIImage *)imageWithColor:(UIColor *)color {
  return [self imageWithColor:color size:CGSizeMake(1.0, 1.0)];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
  CGRect rect = CGRectMake(0.0, 0.0, size.width, size.height);
  UIGraphicsBeginImageContext(rect.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(context, [color CGColor]);
  CGContextFillRect(context, rect);
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

@end
