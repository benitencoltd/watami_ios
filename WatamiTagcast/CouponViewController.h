//
//  CouponViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 1/11/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultViewController.h"

@interface CouponViewController : DefaultViewController

- (IBAction)showStamp:(UITapGestureRecognizer *)gestureRecognizer;

@end
