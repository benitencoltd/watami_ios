//
//  QueryCollectionViewController.h
//  Khemara
//
//  Created by Beniten on 11/18/16.
//  Copyright © 2016 Beniten. All rights reserved.
//

#import "QueryViewController.h"

NS_ASSUME_NONNULL_BEGIN

// If cell does not implement `height`/`heightForObject:`, itemWidth will be used instead.

@interface QueryCollectionViewController<ObjectType : JSONModel *> : QueryViewController<ObjectType> <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
  @protected
  UICollectionView *_collectionView;
  UICollectionViewLayout *_layout;
  Class _cellClass;
}

@property (nonatomic, strong, readonly) UICollectionView *collectionView;
@property (nonatomic, strong, readonly) UICollectionViewLayout *layout;
@property (nonatomic, strong, readonly, nullable) UICollectionViewFlowLayout *flowLayout; // nonnull if init via (initWithObjectClass:)
@property (nonatomic, assign, readonly, nullable) Class cellClass;

@property (nonatomic, assign) CGFloat itemSpacing;
@property (nonatomic, assign) NSInteger itemsPerRow;

- (instancetype)initWithObjectClass:(Class)objectClass; // Defaults to UICollectionViewFlowLayout
- (instancetype)initWithObjectClass:(Class)objectClass cellClass:(nullable Class)cellClass;
- (instancetype)initWithObjectClass:(Class)objectClass cellClass:(nullable Class)cellClass layout:(nullable UICollectionViewLayout *)layout;

@end

NS_ASSUME_NONNULL_END
