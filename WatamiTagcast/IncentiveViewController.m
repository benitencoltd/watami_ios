//
//  IncentiveViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/23/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "IncentiveViewController.h"
#import "StampTicketsViewController.h"
#import "RouletteGroupTicketsViewController.h"

@interface IncentiveViewController ()

@end

@implementation IncentiveViewController

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)showStampTickets:(UITapGestureRecognizer *)gestureRecognizer {
  StampTicketsViewController *stampTicketsViewController = [[StampTicketsViewController alloc] init];
  stampTicketsViewController.includesIncentiveTickets = YES;
  [self.navigationController pushViewController:stampTicketsViewController animated:YES];
}

- (void)showRouletteTickets:(UITapGestureRecognizer *)gestureRecognizer {
  RouletteGroupTicketsViewController *rouletteGroupTicketsViewConroller = [[RouletteGroupTicketsViewController alloc] initWithTagcasts:nil];
  [self.navigationController pushViewController:rouletteGroupTicketsViewConroller animated:YES];
}

@end
