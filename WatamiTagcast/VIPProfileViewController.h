//
//  VIPProfileViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/28/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WTProfile.h"
#import "WTVIPInfo.h"

@interface VIPProfileViewController : UIViewController

@property (nonatomic, strong, readonly) WTProfile *profile;
@property (nonatomic, strong, readonly) WTVIPInfo *vipInfo;

- (instancetype)initWithProfile:(WTProfile *)profile vipInfo:(WTVIPInfo *)vipInfo;

@end
