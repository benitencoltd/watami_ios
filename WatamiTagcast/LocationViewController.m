//
//  LocationViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/3/167.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import "LocationViewController.h"
#import "SVStatusView.h"

@interface LocationViewController ()

@property (nonatomic, strong) SVStatusView *statusView;

@end

@implementation LocationViewController {
  NSURLSessionDataTask *_task;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)loadView {
  [super loadView];
  
  _statusView = [[SVStatusView alloc] initWithFrame:self.view.bounds];
  _statusView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
  WTWeakify(self);
  _statusView.reloadHandler = ^{
    WTStrongify(LocationViewController);
    [strongSelf reloadInfo];
  };
  _statusView.style = SVStatusViewStyleWhite;
  [self.view addSubview:_statusView];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self reloadInfo];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [Common trackScreen:@"location_screen"];
}

- (void)reloadInfo {
  _scrollView.hidden = YES;
  _statusView.state = SVStatusViewStateAnimating;
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"shop",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _task = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(LocationViewController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      [strongSelf.statusView setStateFromObject:json[@"data"]];
    });
    if ([json[@"result"] boolValue]) {
      dispatch_async(dispatch_get_main_queue(), ^{
        strongSelf.scrollView.hidden = NO;
        NSDictionary<NSString *, id> *shop = json[@"data"];
        strongSelf.textLabel.text = [NSString stringWithFormat:@"%@\n\nTel: %@\n\nFax: %@\n\nWebsite: %@\n\nAddress: %@", shop[@"shop_text"], shop[@"shop_tel"], shop[@"shop_fax"], shop[@"shop_website"], shop[@"shop_address"]];
        
        NSArray<NSString *> *images = shop[@"shop_images"];
        
        [strongSelf.headerImageView sd_setImageWithURL:[NSURL URLWithString:images.firstObject]];
          
          
          strongSelf.mapView.camera = [GMSCameraPosition cameraWithLatitude:[shop[@"shop_map"][@"latitude"] floatValue]
                                                                  longitude:[shop[@"shop_map"][@"longitude"] floatValue]
                                                                       zoom:17.0];
        
//          GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectMake(0.0, imageMap.frame.origin.y + imageMap.frame.size.height + 10.0, screenWidth, 200.0) camera:camera];
//          [self.scrollView addSubview:mapView];
//          
          GMSMarker *marker = [[GMSMarker alloc] init];
          marker.position = strongSelf.mapView.camera.target;
//          marker.snippet = @"Hello World";
          marker.appearAnimation = kGMSMarkerAnimationPop;
          marker.map = strongSelf.mapView;
        });
    }
  }];
  [_task resume];
}

@end
