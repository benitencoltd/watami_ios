//
//  LanguagePickerController.m
//  WatamiTagcast
//
//  Created by Beniten on 3/4/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "LanguagePickerController.h"
#import "Constants.h"
#import "LanguageManager.h"
#import "AppDelegate.h"

@interface LanguagePickerController ()

@property (nonatomic, weak) IBOutlet UIButton *kmButton, *enButton, *jpButton;

@end

@implementation LanguagePickerController

- (instancetype)init {
  self = [super initWithNibName:NSStringFromClass([LanguagePickerController class]) bundle:nil];
  if (self) {
    self.title = NSLocalizedString(@"choose_language", nil);
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    if ([[NSUserDefaults standardUserDefaults] stringForKey:kUserLanguageKey]) {
      self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"cross"] style:UIBarButtonItemStylePlain target:self action:@selector(dismiss:)];
    }
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [_kmButton setTitle:NSLocalizedString(@"lang_km", nil) forState:UIControlStateNormal];
  [_enButton setTitle:NSLocalizedString(@"lang_en", nil) forState:UIControlStateNormal];
  [_jpButton setTitle:NSLocalizedString(@"lang_jp", nil) forState:UIControlStateNormal];
  _kmButton.titleLabel.font = [UIFont appFontOfSize:16.0];
  _enButton.titleLabel.font = [UIFont appFontOfSize:16.0];
  _jpButton.titleLabel.font = [UIFont appFontOfSize:16.0];
}

- (void)didSelectLanguage:(UIButton *)sender {
  self.navigationItem.rightBarButtonItem = nil;
  UIView *overlayView = [[UIView alloc] initWithFrame:self.view.bounds];
  overlayView.backgroundColor = [UIColor blackColor];
  overlayView.alpha = 0.0;
  [self.view addSubview:overlayView];
  NSString *selectedLanguage = @[kLanguageKM, kLanguageEN, kLanguageJA][sender.tag];
  if (![selectedLanguage isEqualToString:[LanguageController selectedLanguage]]) {
    [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
      overlayView.alpha = 1.0;
    } completion:^(BOOL finished) {
      [LanguageController setSelectedLanguage:selectedLanguage];
      [LanguageController sendOneSignalLanguageTag];
      [(AppDelegate *)[UIApplication sharedApplication].delegate reloadAppearances];
      [UIApplication sharedApplication].keyWindow.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController];
      [self dismissViewControllerAnimated:NO completion:nil];
    }];
  } else {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
  }
}

- (void)dismiss:(UIBarButtonItem *)sender {
  [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
