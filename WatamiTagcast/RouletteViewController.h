//
//  RouletteViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/15/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultViewController.h"

@interface RouletteViewController : DefaultViewController

@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIImageView *wheelImageView;
@property (nonatomic, weak) IBOutlet UIButton *startButton;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *playLoadingIndicatorView;

- (IBAction)start:(UIButton *)sender;
- (IBAction)showTickets:(UIButton *)sender;

@end
