//
//  WTMenuItemCategory.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/10/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTMenuItemCategory.h"

@implementation WTMenuItemCategory

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"id":@"category_id",
                                                                @"name":@"category_name",
                                                                @"text":@"menu_text",
                                                                @"imageURL":@"category_image",
                                                                @"thumbnailURL":@"category_thumbnail"}];
}

- (void)setImageURLWithNSArray:(NSArray<NSString *> *)array {
  _imageURL = [NSURL URLWithString:array.firstObject];
}

- (void)setThumbnailURLWithNSArray:(NSArray<NSString *> *)array {
  _thumbnailURL = [NSURL URLWithString:array.firstObject];
}

@end
