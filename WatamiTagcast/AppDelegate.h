//
//  AppDelegate.h
//  WatamiTagcast
//
//  Created by Choung Chamnab on 12/31/15.
//  Copyright © 2015 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)reloadAppearances;
- (void)sendOneSignalLanguageTagIfNeeded;

@end

