//
//  TicketsClaimAlertController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/29/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "TicketsClaimAlertController.h"

@interface TicketsClaimAlertController ()

@property (nonatomic, strong) UIActivityIndicatorView *loadingIndicatorView;

@end

@implementation TicketsClaimAlertController

- (instancetype)init {
  self = [super initWithNibName:NSStringFromClass([self class]) bundle:nil];
  if (self) {
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.view.userInteractionEnabled = YES;
  [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapView:)]];
//  [_imageView sd_setImageWithURL:_ticket.imageURL];
//  _expiryDateLabel.text = [NSString stringWithFormat:@"%@ : %@", NSLocalizedString(@"exp", nil), WTStringFromDate(_ticket.expiryDate)];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  _loadingIndicatorView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
}

- (void)ok:(UIButton *)sender {
  _containerView.hidden = YES;
  _loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
  _loadingIndicatorView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
  [_loadingIndicatorView startAnimating];
  [self.view addSubview:_loadingIndicatorView];
  _claimHandler(self);
}

- (void)cancel:(UIButton *)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didTapView:(UITapGestureRecognizer *)gestureRecognizer {
  CGPoint location = [gestureRecognizer locationInView:gestureRecognizer.view];
  if (!CGRectContainsPoint(_containerView.frame, location)) {
    [self dismissViewControllerAnimated:NO completion:nil];
  }
}

@end
