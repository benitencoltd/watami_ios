//
//  LanguageManager.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/10/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const kLanguageEN = @"en";
static NSString * const kLanguageKM = @"km";
static NSString * const kLanguageJA = @"ja";

@interface LanguageController : NSObject

@property (nonatomic, strong, class) NSString *selectedLanguage;

+ (NSString *)preferredLanguage;
+ (void)sendOneSignalLanguageTag;

@end
