//
//  RouletteGroupTicketCollectionViewCell.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/18/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouletteGroupTicketCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UIImageView *imageView;

+ (CGFloat)rowHeightForWidth:(CGFloat)width;

@end
