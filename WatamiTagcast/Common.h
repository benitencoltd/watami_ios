//
//  Common.h
//  WatamiTagcast
//
//  Created by Mr. Haruhiro Sato on 1/22/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GoogleAnalytics/GAI.h>

@interface Common : NSObject

+ (NSString *) getToken;
+ (NSString *) getLang;
+ (void) showAlert:(NSString *)msg title:(NSString *)msg _self:(UIViewController *)_self;
+ (UIAlertController *) getRetryAlert;
+ (NSMutableURLRequest *)convertToRequest:(NSDictionary<NSString *, id> *)data;

+ (void) trackScreen:(NSString *) name;
+ (void) trackEvent:(NSString *) category action:(NSString *)action label:(NSString *)label;

+ (NSURLRequest *)requestWithParameters:(NSDictionary<NSString *, id> *)parameters dataDictionary:(NSDictionary<NSString *, NSData *> *)dataDictionary; // dataDictionary may be nil
+ (UIAlertController *)retryAlertControllerWithHandler:(void (^)(void))retryHandler;
+ (void)showRetryAlertInViewController:(UIViewController *)viewController handler:(void (^)(void))handler;

@end

@interface NSDateFormatter (Shared)

+ (instancetype)sharedDateFormatter;

@end
