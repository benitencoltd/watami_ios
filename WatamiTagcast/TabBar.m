//
//  TabBar.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/2/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "TabBar.h"

@implementation TabBar

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self commonInit];
  }
  return self;
}

//- (void)setItems:(NSArray<UITabBarItem *> *)items animated:(BOOL)animated {
//  NSDictionary<NSString *, id> *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10.0], NSForegroundColorAttributeName:[UIColor whiteColor]};
//  for (UITabBarItem *item in items) {
//    item.titlePositionAdjustment = UIOffsetMake(0.0, -4.0);
//    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
//    [item setTitleTextAttributes:attributes forState:UIControlStateSelected];
//  }
//  [super setItems:items animated:animated];
//}

- (void)commonInit {
  self.translucent = NO;
  self.barTintColor = [UIColor colorWithRed:65.0/255.0 green:65.0/255.0 blue:65.0/255.0 alpha:1.0];
  self.tintColor = [UIColor whiteColor];
  self.selectionIndicatorImage = [UIImage imageWithColor:[UIColor blackColor] size:CGSizeMake(ceil([UIScreen mainScreen].bounds.size.width / 6.0), 70.0)];
}

- (CGSize)sizeThatFits:(CGSize)size {
  CGSize newSize = [super sizeThatFits:size];
  newSize.height = 70.0;
  return newSize;
}

@end
