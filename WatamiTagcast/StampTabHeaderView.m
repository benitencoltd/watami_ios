//
//  StampTabHeaderView.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "StampTabHeaderView.h"

#define CENTER_IMAGE_RATIO 115.0 / 320.0
#define ARROW_WIDTH 16.0

@interface StampTabHeaderView () <SVTabHeaderViewDelegate>

@property (nonatomic, strong) UIImageView *centerImageView;
@property (nonatomic, strong) UILabel *instructionLabel;
@property (nonatomic, strong) UIImageView *arrowImageView;

@end

@implementation StampTabHeaderView

@synthesize checkInIndicatorView = _checkInIndicatorView;
@synthesize generateIndicatorView = _generateIndicatorView;

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor blackColor];
    self.minimumHeight = 0.0;
    self.userInteractionEnabled = YES;
    self.delegate = self;
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    _rankButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _rankButton.backgroundColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    _rankButton.contentEdgeInsets = UIEdgeInsetsMake(4.0, 12.0, 4.0, 12.0);
    [_rankButton setTitle:NSLocalizedString(@"member_rank", nil).uppercaseString forState:UIControlStateNormal];
    _rankButton.tintColor = [UIColor whiteColor];
    _rankButton.titleLabel.font = [UIFont appFontOfSize: 13.0];
    [_rankButton sizeToFit];
    _rankButton.layer.cornerRadius = _rankButton.frame.size.height / 2;
    _rankButton.frame = CGRectMake(screenWidth - _rankButton.frame.size.width - 12.0, 6.0, _rankButton.frame.size.width, _rankButton.frame.size.height);
    [self addSubview:_rankButton];
    
    CGFloat centerImageHeight = ceil((screenWidth - 16.0) * CENTER_IMAGE_RATIO);
    _centerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8.0, CGRectGetMaxY(_rankButton.frame) + 8.0, screenWidth - 16.0, centerImageHeight)];
    _centerImageView.backgroundColor = [UIColor whiteColor];
    _centerImageView.clipsToBounds = YES;
    _centerImageView.contentMode = UIViewContentModeScaleAspectFill;
    _centerImageView.image = [UIImage imageNamed:@"ic_stamp_s.png"];
    _centerImageView.layer.cornerRadius = 6.0;
    [self addSubview:_centerImageView];
    
    _rankTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _rankTitleLabel.textAlignment = NSTextAlignmentCenter;
    _rankTitleLabel.textColor = [UIColor whiteColor];
    _rankTitleLabel.font = [UIFont appFontOfSize:15.0];
    [self addSubview:_rankTitleLabel];
    
    _instructionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _instructionLabel.numberOfLines = 0;
    _instructionLabel.font = [UIFont appFontOfSize:15.0];
    _instructionLabel.textAlignment = NSTextAlignmentCenter;
    _instructionLabel.textColor = [UIColor whiteColor];
    _instructionLabel.text = NSLocalizedString(@"stamp_instruct", nil);
    [self addSubview:_instructionLabel];
    
    _arrowImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _arrowImageView.image = [[UIImage imageNamed:@"ic_arrow_down.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _arrowImageView.tintColor = [UIColor whiteColor];
    [self addSubview:_arrowImageView];
    
    _checkInButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [_checkInButton setTitleColor:GOLD_COLOR forState:UIControlStateNormal];
    [_checkInButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [_checkInButton setTitle:NSLocalizedString(@"check_in", nil).uppercaseString forState:UIControlStateNormal];
    _checkInButton.titleLabel.font = [UIFont appFontOfSize:15.0];
    [self addSubview:_checkInButton];
    
    _generateButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [_generateButton setTitleColor:GOLD_COLOR forState:UIControlStateNormal];
    [_generateButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [_generateButton setTitle:NSLocalizedString(@"generate", nil).uppercaseString forState:UIControlStateNormal];
    _generateButton.titleLabel.font = [UIFont appFontOfSize:15.0];
    [self addSubview:_generateButton];
    
    
    CGFloat instructionHeight = ceil([_instructionLabel sizeThatFits:CGSizeMake([UIScreen mainScreen].bounds.size.width - 24.0, CGFLOAT_MAX)].height);
    
    self.defaultHeight = 8.0 + ceil(_rankButton.frame.size.height) + 8.0 + _centerImageView.frame.size.height + 12.0 + ceil(_rankTitleLabel.font.lineHeight) + 6.0 + instructionHeight + 6.0 + ARROW_WIDTH + 12.0 + 40.0 + 12.0;
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  
  _rankTitleLabel.frame = CGRectMake(12.0, CGRectGetMaxY(_centerImageView.frame) + 12.0, self.frame.size.width - 24.0, ceil(_rankTitleLabel.font.lineHeight));
  
  CGFloat instructionHeight = ceil([_instructionLabel sizeThatFits:CGSizeMake([UIScreen mainScreen].bounds.size.width - 24.0, CGFLOAT_MAX)].height);
  _instructionLabel.frame = CGRectMake(12.0, CGRectGetMaxY(_rankTitleLabel.frame) + 6.0, self.frame.size.width - 24.0, instructionHeight);
  _arrowImageView.frame = CGRectMake((self.frame.size.width - ARROW_WIDTH) / 2.0, CGRectGetMaxY(_instructionLabel.frame) + 6.0, ARROW_WIDTH, ARROW_WIDTH);
  
  CGFloat checkInWidth = [_checkInButton sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)].width;
  if (_generateButton.isHidden && !_generateIndicatorView.isAnimating) {
    _checkInButton.frame = CGRectMake((self.frame.size.width - checkInWidth) / 2.0, CGRectGetMaxY(_arrowImageView.frame) + 12.0, checkInWidth, 40.0);
    _checkInIndicatorView.center = CGPointMake(CGRectGetMidX(_checkInButton.frame), CGRectGetMidY(_checkInButton.frame));
  } else {
    CGFloat generateWidth = [_generateButton sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)].width;
    CGFloat totalWidth = checkInWidth + 12.0 + generateWidth;
    _checkInButton.frame = CGRectMake((self.frame.size.width - totalWidth) / 2.0, CGRectGetMaxY(_arrowImageView.frame) + 12.0, checkInWidth, 40.0);
    _checkInIndicatorView.center = CGPointMake(CGRectGetMidX(_checkInButton.frame), CGRectGetMidY(_checkInButton.frame));
    _generateButton.frame = CGRectMake(CGRectGetMaxX(_checkInButton.frame) + 12.0, CGRectGetMaxY(_arrowImageView.frame) + 12.0, generateWidth, 40.0);
    _generateIndicatorView.center = CGPointMake(CGRectGetMidX(_generateButton.frame), CGRectGetMidY(_generateButton.frame));
  }
}

- (UIActivityIndicatorView *)checkInIndicatorView {
  if (!_checkInIndicatorView) {
    _checkInIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self addSubview:_checkInIndicatorView];
  }
  return _checkInIndicatorView;
}

- (UIActivityIndicatorView *)generateIndicatorView {
  if (!_generateIndicatorView) {
    _generateIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self addSubview:_generateIndicatorView];
  }
  return _generateIndicatorView;
}

- (NSArray<UIView *> *)interactiveSubviewsInHeaderView:(SVTabHeaderView *)headerView {
  return @[_rankButton, _checkInButton, _generateButton];
}

@end
