//
//  HomeViewController.h
//  WatamiTagcast
//
//  Created by Mr. Haruhiro Sato on 1/4/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultViewController.h"

@interface HomeViewController : DefaultViewController

- (void)reloadVIPInfo;

@end
