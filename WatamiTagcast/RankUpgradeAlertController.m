//
//  RankUpgradeAlertController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/1/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "RankUpgradeAlertController.h"
#import "FBShareButton.h"
#import "WTMemberCard.h"

@interface RankUpgradeAlertController ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel, *detailLabel;

@property (nonatomic, weak) IBOutlet UIImageView *leftImageView;
@property (nonatomic, weak) IBOutlet UIImageView *topLeftImageView;
@property (nonatomic, weak) IBOutlet UILabel *leftTitleLabel;

@property (nonatomic, weak) IBOutlet UIImageView*rightImageView;
@property (nonatomic, weak) IBOutlet UIImageView *topRightImageView;
@property (nonatomic, weak) IBOutlet UILabel *rightTitleLabel;

@property (nonatomic, weak) IBOutlet FBShareButton *shareButton;

- (IBAction)dismiss:(UIButton *)sender;
- (IBAction)share:(FBShareButton *)sender;

@end

@implementation RankUpgradeAlertController

- (instancetype)initWithCurrentRank:(WTRank *)currentRank previousRank:(WTRank *)previousRank {
  self = [super initWithNibName:NSStringFromClass([RankUpgradeAlertController class]) bundle:nil];
  if (self) {
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _currentRank = currentRank;
    _previousRank = previousRank;
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)viewDidLoad {
  [super viewDidLoad];
  _titleLabel.font = [UIFont appFontOfSize:17.0];
  _detailLabel.font = [UIFont appFontOfSize:15.0];
  _titleLabel.text = NSLocalizedString(@"congratulation", nil);
  _detailLabel.text = NSLocalizedString(@"your_rank", nil);
  
  _leftTitleLabel.font = [_leftTitleLabel.font appFont];
  _rightTitleLabel.font = [_rightTitleLabel.font appFont];
  
  _leftTitleLabel.text = _previousRank.title;
  _rightTitleLabel.text = _currentRank.title;
  
  _leftImageView.image = [_leftImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  _leftImageView.tintColor = [UIColor colorWithRed:120.0/255.0 green:144.0/255.0 blue:156.0/255.0 alpha:1.0];
  _topLeftImageView.image = [[_topLeftImageView.image imageWithInsetPercent:0.6] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  _topLeftImageView.backgroundColor = _leftImageView.tintColor;
  
  _rightImageView.image = [_rightImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  _rightImageView.tintColor = [UIColor colorWithRed:1.0 green:171.0/255.0 blue:0.0 alpha:1.0];
  _topRightImageView.image = [[_topRightImageView.image imageWithInsetPercent:0.6] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  _topRightImageView.backgroundColor = _rightImageView.tintColor;
}

- (void)loadMemberCardWithAppLinkURLString:(NSString *)appLinkURLString completion:(void (^)(WTMemberCard *memberCard))completion {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getmembercard",
                                             @"lang":[Common getLang],
                                             @"url":appLinkURLString};
  WTWeakify(self);
  [[[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(RankUpgradeAlertController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
      if (json[@"data"]) {
        WTMemberCard *memberCard = [[WTMemberCard alloc] initWithDictionary:json[@"data"] error:NULL];
        if (memberCard && memberCard.appLinkId.length > 0) {
          completion(memberCard);
        }
      }
    });
  }] resume];
}

- (void)dismiss:(UIButton *)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)share:(FBShareButton *)sender {
  NSString *appLinkURLString = [NSString stringWithFormat:@"com.beniten.tagcast.watami://detail?type=4"];
  WTWeakify(self);
  [self loadMemberCardWithAppLinkURLString:appLinkURLString completion:^(WTMemberCard *memberCard) {
    WTStrongify(RankUpgradeAlertController);
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://fb.me/%@", memberCard.appLinkId]];
    content.contentTitle = kWatamiTitle;
    content.contentDescription = [NSString stringWithFormat:@"You rank has been upgraded from %@ to %@.", strongSelf.previousRank.title, strongSelf.currentRank.title];
    content.imageURL = memberCard.photoURL;
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = strongSelf;
    dialog.mode = FBSDKShareDialogModeNative;
    dialog.shareContent = content;
    [dialog show];
  }];
}

@end
