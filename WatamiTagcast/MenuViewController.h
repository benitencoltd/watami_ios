//
//  MenuViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultViewController.h"
#import "QueryViewController.h"
#import "WTMenuItemCategory.h"

@interface MenuViewController : QueryViewController<WTMenuItemCategory *>

@end
