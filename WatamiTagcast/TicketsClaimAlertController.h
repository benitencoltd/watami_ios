//
//  TicketsClaimAlertController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/29/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketsClaimAlertController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *expiryDateLabel;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UIButton *okButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic, copy) void (^claimHandler)(TicketsClaimAlertController *controller);

- (instancetype)init;

- (IBAction)ok:(UIButton *)sender;
- (IBAction)cancel:(UIButton *)sender;

@end
