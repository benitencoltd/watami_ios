//
//  MenuItemsViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "QueryCollectionViewController.h"
#import "WTMenuItem.h"

@interface MenuItemsViewController : QueryCollectionViewController<WTMenuItem *>

@property (nonatomic, assign, readonly) NSUInteger categoryId;

- (instancetype)initWithCategoryId:(NSUInteger)categoryId;

@end
