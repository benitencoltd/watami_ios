//
//  LanguagePickerController.h
//  WatamiTagcast
//
//  Created by Beniten on 3/4/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguagePickerController : UIViewController

- (IBAction)didSelectLanguage:(UIButton *)sender;

@end
