//
//  FBShareButton.h
//  Khemara
//
//  Created by Beniten on 2/8/17.
//  Copyright © 2017 Beniten. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBShareButton : UIButton

+ (instancetype)button;

@end
