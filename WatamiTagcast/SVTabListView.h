//
//  SVTabListView.h
//  SVKit
//
//  Created by Sereivoan Yong on 8/8/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SVTabListView : UIView {
  @protected
  UICollectionView *_collectionView;
}

@property (nonatomic, strong) NSArray<NSString *> *items;
@property (nonatomic, assign) NSInteger selectedIndex;

// Thise will not be invoked when u set selectedIndex by yourself.
@property (nonatomic, copy, nullable) void (^selectionHandler)(NSInteger);

@property (nonatomic, assign) CGFloat extraTabPadding; // 10.0. Has no effect if fillsEqually is `YES`
@property (nonatomic, assign) BOOL centerTabsIfNotFilled; // YES
@property (nonatomic, assign, getter=isScrollingEnabledIfNotFilled) BOOL scrollingEnabledIfNotFilled; // NO
@property (nonatomic, strong) NSDictionary<NSString *, id> *tabAttributes;
@property (nonatomic, strong, nullable) UIColor *selectedColor;
@property (nonatomic, strong, readonly, nullable) CALayer *bottomBorderLayer;
@property (nonatomic, assign, getter=isBottomBorderEnabled) BOOL bottomBorderEnabled; // NO
@property (nonatomic, assign) BOOL fillsEqually; // NO

- (instancetype)initWithItems:(NSArray<NSString *> *)items;

- (void)setIndicatorProgress:(CGFloat)progress scrollingLeft:(BOOL)scrollingLeft; // 0..<items.count
- (void)refreshTabColorForSelectedIndex:(NSInteger)selectedIndex unselectedIndex:(NSInteger)unselectedIndex;

@end

NS_ASSUME_NONNULL_END
