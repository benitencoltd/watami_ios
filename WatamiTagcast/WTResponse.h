//
//  WTResponse.h
//  Khemara
//
//  Created by Beniten on 8/3/16.
//  Copyright © 2016 Beniten. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTResponse : JSONModel

@property (nonatomic, assign) BOOL succeeded;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSDictionary<NSString *, id> *data;

@end
