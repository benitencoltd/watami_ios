//
//  WTStampStatus.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTStampRanking : JSONModel

@property (nonatomic, copy) NSString *memberType;
@property (nonatomic, copy) NSString *numberOfTicketsRequired;

@end

@interface WTStampStatus : JSONModel

@property (nonatomic, copy) NSString *memberType;
@property (nonatomic, copy) NSArray<WTStampRanking *> *rankings;
@property (nonatomic, assign, getter=isAllowed) BOOL allowed;

@end
