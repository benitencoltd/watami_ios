//
//  StampCollectionViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "StampCollectionViewCell.h"

@implementation StampCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor whiteColor];
    self.clipsToBounds = YES;
    
    _numberLabel = [[UILabel alloc] initWithFrame:self.contentView.bounds];
    _numberLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    _numberLabel.textAlignment = NSTextAlignmentCenter;
    _numberLabel.font = [UIFont fontWithName:@"Bauhaus93" size:55.0];
    _numberLabel.textColor = [UIColor colorWithRed:177.0/255 green:227.0/255 blue:250.0/255 alpha:1.0];
    [self.contentView addSubview:_numberLabel];
    
    _bucketImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _bucketImageView.backgroundColor = [UIColor whiteColor];
    _bucketImageView.contentMode = UIViewContentModeScaleAspectFit;
    _bucketImageView.image = [UIImage imageNamed:@"bg_bucket.png"];
    [self.contentView addSubview:_bucketImageView];
    
    _thankYouImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _thankYouImageView.image = [UIImage imageNamed:@"bg_thank_transparent.png"];
    [self.contentView addSubview:_thankYouImageView];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  self.layer.cornerRadius = self.frame.size.width / 2;
  _bucketImageView.frame = CGRectInset(self.contentView.bounds, 18.0, 18.0);
  _thankYouImageView.frame = CGRectInset(self.contentView.bounds, 12.0, 12.0);
}

@end
