//
//  VIPUserTableViewCell.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/28/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kVIPUserRowHeight = 96.5;

@interface VIPUserTableViewCell : UITableViewCell

@property (nonatomic, strong, readonly) UILabel *numberLabel;
@property (nonatomic, strong, readonly) UIImageView *photoImageView;
@property (nonatomic, strong, readonly) UILabel *nameLabel;
@property (nonatomic, strong, readonly) UILabel *memberTypeLabel;
@property (nonatomic, strong, readonly) UILabel *stampCountLabel;

@end
