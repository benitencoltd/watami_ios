//
//  WTRouletteGroupTicket.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/17/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTRouletteGroupTicket.h"

@implementation WTRouletteGroupTicket

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"tickets":@"data"}];
}

- (void)setTicketsWithNSArray:(NSArray<NSDictionary<NSString *, id> *> *)array {
  _tickets = [WTRouletteTicket arrayOfModelsFromDictionaries:array error:NULL];
}

@end
