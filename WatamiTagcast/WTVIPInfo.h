//
//  WTVIPInfo.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/24/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "WTRank.h"

#define WTInvalidRankNumber -1U

@interface WTVIPInfoTicketInfo : JSONModel

@property (nonatomic, copy) NSString *type;
@property (nonatomic, assign) NSUInteger count;

@end

@interface WTVIPInfo : JSONModel

@property (nonatomic, assign) BOOL isVIP;
@property (nonatomic, assign, getter=isRegistered) BOOL registered;
@property (nonatomic, copy) NSURL *photoURL;

@property (nonatomic, copy) NSArray<WTVIPInfoTicketInfo *> *ticketInfos;
@property (nonatomic, assign) NSUInteger numberOfStamps;
@property (nonatomic, assign) NSUInteger numberOfIncentiveTickets;

@property (nonatomic, assign) NSInteger rankNumber;
@property (nonatomic, assign) NSInteger monthlyRankNumber;

@property (nonatomic, strong) WTRank *currentRank;
@property (nonatomic, strong) WTRank *previousRank;
@property (nonatomic, strong) WTRank *nextRank;

@end
