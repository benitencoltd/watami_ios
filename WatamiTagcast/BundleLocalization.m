//    The MIT License (MIT)
//
//    Copyright (c) 2015 Corneliu Maftuleac
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.

#import "BundleLocalization.h"
#import <objc/runtime.h>

@implementation NSBundle (Localization)

+ (void)load {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    Class class = [self class];
    SEL originalSelector = @selector(localizedStringForKey:value:table:);
    SEL swizzledSelector = @selector(customLocaLizedStringForKey:value:table:);
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    BOOL didAddMethod = class_addMethod(class, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod) {
      class_replaceMethod(class, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    } else {
      method_exchangeImplementations(originalMethod, swizzledMethod);
    }
  });
}

#pragma mark - Method Swizzling

- (NSString *)customLocaLizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName {
  NSBundle *bundle = BundleLocalization.sharedInstance.localizationBundle;
  NSString *string = [bundle customLocaLizedStringForKey:key value:value table:tableName];
  if ([string isEqualToString:key]) {
    return [self customLocaLizedStringForKey:key value:value table:tableName];
  }
  return string;
}

@end

@implementation BundleLocalization {
  NSString *selectedLanguage;
}

+ (BundleLocalization *)sharedInstance {
  static BundleLocalization *instance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    instance = [[BundleLocalization alloc] init];
  });
  return instance;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    NSBundle *bundle;
    NSString *currentLanguage = [[NSUserDefaults standardUserDefaults] stringForKey:@"language"];
    if (currentLanguage) {
      NSString *languageBundlePath = [[NSBundle mainBundle] pathForResource:currentLanguage ofType:@"lproj"];
      bundle = [NSBundle bundleWithPath:languageBundlePath];
    }
    _localizationBundle = bundle ?: [NSBundle mainBundle];
  }
  return self;
}

- (void)setLanguage:(NSString *)lang {
  // path to this languages bundle
  if ([lang isEqualToString:@"en"]) {
    lang = @"Base";
  }
  NSString *path = [NSBundle.mainBundle pathForResource:lang ofType:@"lproj"];
  if (!path) {
    // there is no bundle for that language
    // use main bundle instead
    _localizationBundle = NSBundle.mainBundle;
  } else {
    // use this bundle as my bundle from now on:
    _localizationBundle = [NSBundle bundleWithPath:path];
    // to be absolutely sure (this is probably unnecessary):
    if (!_localizationBundle) {
      _localizationBundle = NSBundle.mainBundle;
    }
    selectedLanguage = lang;
  }
}

- (NSString *)language {
  if (_localizationBundle == NSBundle.mainBundle) {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"language"] ?: NSBundle.mainBundle.preferredLocalizations.firstObject;
  } else {
    return selectedLanguage;
  }
}

@end
