//
//  Common.m
//  WatamiTagcast
//
//  Created by Mr. Haruhiro Sato on 1/22/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "Common.h"
#import "Reachability.h"
#import "LanguageManager.h"
#import "UIFont+App.h"
#import "Constants.h"

@implementation Common

+ (NSString *)getToken {
//  return @"04adb4e2f055c978c9bb101ee1bc5cd4";
  return [[NSUserDefaults standardUserDefaults] stringForKey:kUserAccessTokenKey];
}

+ (UIAlertController *)retryAlertControllerWithHandler:(void (^)(void))retryHandler {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Watami" message:NSLocalizedString(@"retry_message", nil) preferredStyle:UIAlertControllerStyleAlert];
  [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleDefault handler:nil]];
  [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"retry", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    retryHandler();
  }]];
  return alertController;
}

+ (void)showRetryAlertInViewController:(UIViewController *)viewController handler:(void (^)(void))handler {
  [viewController presentViewController:[self retryAlertControllerWithHandler:handler] animated:YES completion:nil];
}

+ (NSString *)getLang {
  NSString *lang = [[NSUserDefaults standardUserDefaults] stringForKey:kUserLanguageKey];
  if ([lang isEqual:kLanguageKM]) {
    return @"km";
  } else if ([lang isEqual:kLanguageJA]) {
    return @"jp";
  } else {
    return @"en";
  }
}

+ (void)showAlert:(NSString *)msg title:(NSString *)title _self:(UIViewController *)_self{
    if (!title) {
        title = @"Watami";
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [_self presentViewController:alertController animated:YES completion:nil];
}

+ (UIAlertController *)getRetryAlert {
  return [UIAlertController alertControllerWithTitle:@"Watami" message:NSLocalizedString(@"retry_message", nil) preferredStyle:UIAlertControllerStyleAlert];
}

+ (NSMutableURLRequest *)convertToRequest:(NSDictionary<NSString *, id> *)data {
  NSError *error;
  NSData *postJSONData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
  
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kWatamiAPIURLString]];
  [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  request.HTTPMethod = @"POST";
  request.HTTPBody = postJSONData;
  return request;
}

+ (void)trackScreen:(NSString *)name {
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:name];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

+ (void) trackEvent:(NSString *)category action:(NSString *)action label:(NSString *)label{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:action
                                                           label:label
                                                           value:nil] build]];
}

+ (NSURLRequest *)requestWithParameters:(NSDictionary<NSString *, id> *)parameters dataDictionary:(NSDictionary<NSString *, NSData *> *)dataDictionary {
//  NSString *abc = @"http://dev-tagcast-watami.beniten.com/upload.php";
  NSString *boundary = @"---------------------------14737809831466499882746641449";
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kWatamiAPIURLString]];
  request.HTTPMethod = @"POST";
  [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
  
  NSMutableData *HTTPBody = [NSMutableData data];
  
  // Add parameters
  [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, id value, BOOL *stop) {
    [HTTPBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [HTTPBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
    [HTTPBody appendData:[[NSString stringWithFormat:@"%@\r\n", value] dataUsingEncoding:NSUTF8StringEncoding]];
  }];
  // Add data
  if (dataDictionary) {
    [dataDictionary enumerateKeysAndObjectsUsingBlock:^(NSString *fileName, NSData *data, BOOL *stop) {
      [HTTPBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
      [HTTPBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
      [HTTPBody appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
      [HTTPBody appendData:data];
      [HTTPBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }];
  }
  
  [HTTPBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
  
  request.HTTPBody = HTTPBody;
  return request;
}

@end

@implementation NSDateFormatter (Shared)

+ (instancetype)sharedDateFormatter {
  static NSDateFormatter *dateFormatter = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd";
  });
  return dateFormatter;
}

@end
