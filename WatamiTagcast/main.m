//
//  main.m
//  WatamiTagcast
//
//  Created by Mr. Haruhiro Sato on 12/31/15.
//  Copyright © 2015 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
