//
//  WTRank.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/3/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

//typedef NS_ENUM(NSInteger, WTRankType) {
//  WTRankTypeUnknown  = 0,
//  WTRankTypeBlack    = 1,
//  WTRankTypePlatinum = 2,
//  WTRankTypeGold     = 3,
//  WTRankTypeSilver   = 4,
//  WTRankTypeBronze   = 5,
//};

@interface WTRank : JSONModel

@property (nonatomic, copy) NSString *title; // Black, Platinum, ... (Original key is `rank`)
@property (nonatomic, assign) NSInteger type;

- (instancetype)initWithTitle:(NSString *)title type:(NSInteger)type; // Convenience init. Some APIs don't use the same key, so `initWithDictionary:` will not always work.

+ (UIColor *)colorFromType:(NSInteger)type;
+ (UIImage *)medalImageForType:(NSInteger)type;

@end
