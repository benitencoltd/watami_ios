//
//  WTMemberCard.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/20/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTMemberCard : JSONModel

@property (nonatomic, copy) NSString *appLinkId;
@property (nonatomic, copy) NSURL *photoURL;

@end
