//
//  HomeRankingItemCollectionViewCell.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/29/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVIPItemCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UIImageView *circleImageView;
@property (nonatomic, strong, readonly) UIButton *circleButton;
@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, copy) void (^actionHandler)(void);

@end

@interface HomeRankingItemCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UIView *circleView;
@property (nonatomic, strong, readonly) UILabel *countLabel;
@property (nonatomic, strong, readonly) UILabel *titleLabel;

@end
