//
//  MenuItemCollectionViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "MenuItemCollectionViewCell.h"

#define kImageHeight 100.0
#define kNameFontSize 14.0
#define kPriceFontSize 18.0

@implementation MenuItemCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 4.0;
    self.clipsToBounds = YES;
    
    _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
    _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _imageView.clipsToBounds = YES;
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:_imageView];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _nameLabel.font = [UIFont appFontOfSize:kNameFontSize];
    _nameLabel.numberOfLines = 2;
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_nameLabel];
    
    _priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _priceLabel.font = [UIFont appFontOfSize:kPriceFontSize];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.textColor = DARK_RED_COLOR;
    [self.contentView addSubview:_priceLabel];
  }
  return self;
}

- (void)prepareForReuse {
  [super prepareForReuse];
  _nameLabel.font = [UIFont appFontOfSize:kNameFontSize];
  _priceLabel.font = [UIFont appFontOfSize:kPriceFontSize];
}

- (void)layoutSubviews {
  [super layoutSubviews];
  _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, kImageHeight);
  _nameLabel.frame = CGRectMake(8.0, CGRectGetMaxY(_imageView.frame) + 6.0, self.contentView.frame.size.width - 16.0, ceil(_nameLabel.font.lineHeight * 2));
  _priceLabel.frame = CGRectMake(8.0, CGRectGetMaxY(_nameLabel.frame) + 6.0, self.contentView.frame.size.width - 16.0, ceil(_priceLabel.font.lineHeight));
}

+ (CGFloat)itemHeight {
  return kImageHeight + 6.0 + ceil([UIFont appFontOfSize:kNameFontSize].lineHeight * 2) + 6.0 + ceil([UIFont appFontOfSize:kPriceFontSize].lineHeight) + 8.0;
}

@end
