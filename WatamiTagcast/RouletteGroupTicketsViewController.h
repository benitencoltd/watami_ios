//
//  RouletteGroupTicketsViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/17/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QueryCollectionViewController.h"
#import "WTRouletteGroupTicket.h"

@interface RouletteGroupTicketsViewController : QueryCollectionViewController<WTRouletteGroupTicket *>

@property (nonatomic, copy, readonly) NSArray<NSDictionary<NSString *, id> *> *tagcasts;

- (instancetype)initWithTagcasts:(NSArray<NSDictionary<NSString *, id> *> *)tagcasts;

@end
