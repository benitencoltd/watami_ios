//
//  WTRank.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/3/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTRank.h"

@implementation WTRank

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"title":@"rank"}];
}

- (instancetype)initWithTitle:(NSString *)title type:(NSInteger)type {
  return [super initWithDictionary:@{@"rank":title, @"type":@(type)} error:NULL];
}

+ (UIColor *)colorFromType:(NSInteger)type {
  switch (type) {
    case 11:     return [UIColor blackColor];
    case 10:  return [UIColor colorWithRed:245.0/255.0 green:0.0 blue:87.0/255.0 alpha:1.0];
    case 9:      return [UIColor colorWithRed:1.0 green:171.0/255.0 blue:0.0 alpha:1.0];
    case 8:    return [UIColor colorWithRed:120.0/255.0 green:144.0/255.0 blue:156.0/255.0 alpha:1.0];
    case 7:    return [UIColor colorWithRed:78.0/255.0 green:52.0/255.0 blue:46.0/255.0 alpha:1.0];
    default:   return [UIColor whiteColor];
  }
}

+ (UIImage *)medalImageForType:(NSInteger)type {
//  if (type == WTRankTypeBlack || type == WTRankTypePlatinum || type == WTRankTypeGold) {
  if (type > 0) {
    return [[UIImage imageNamed:@"small_medal.png"] imageWithInsetPercent:0.6];
  }
  return nil;
}

@end
