//
//  RankingsViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/21/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "RankingsViewController.h"

#import "RankingTableViewCell.h"

@interface RankingsViewController ()

@end

@implementation RankingsViewController

- (instancetype)initWithRankings:(NSArray<WTStampRanking *> *)rankings {
  self = [super initWithNibName:nil bundle:nil];
  if (self) {
    self.title = NSLocalizedString(@"rank_info", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(dismiss:)];
    _rankings = [rankings copy];
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)loadView {
  [super loadView];
  self.tableView.allowsSelection = NO;
  self.tableView.tableFooterView = [[UIView alloc] init];
  self.tableView.rowHeight = 50.5;
  [self.tableView registerClass:[RankingTableViewCell class] forCellReuseIdentifier:NSStringFromClass([RankingTableViewCell class])];
}

- (void)dismiss:(UIBarButtonItem *)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return _rankings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  RankingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RankingTableViewCell class]) forIndexPath:indexPath];
  WTStampRanking *ranking = _rankings[indexPath.row];
  cell.numberLabel.text = [NSString stringWithFormat:@"%i", (int)indexPath.row + 1];
  cell.titleLabel.text = ranking.memberType;
  cell.countLabel.text = ranking.numberOfTicketsRequired;
  return cell;
}

@end
