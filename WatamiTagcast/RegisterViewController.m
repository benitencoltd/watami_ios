//
//  RegisterViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <sys/utsname.h>
#import <TAKUUID/TAKUUIDStorage.h>
#import <MBProgressHUD/MBProgressHUD.h>

#import "RegisterViewController.h"
#import "Constants.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (instancetype)init {
  self = [super initWithNibName:nil bundle:nil];
  if (self) {
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

#pragma mark -

- (void)loadView {
  [super loadView];
  self.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.85];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [TAKUUIDStorage sharedInstance].accessGroup = [NSString stringWithFormat:@"%@.com.beniten.tagcast.watami.dev", kWatamiAppIdPrefix];
  [[TAKUUIDStorage sharedInstance] migrate];
  [self registerDevice];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [self.presentingViewController viewWillAppear:animated];
}

#pragma mark -

- (void)registerDevice {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"Registering Device...";
  NSString *uuid = [[TAKUUIDStorage sharedInstance] createUUIDStringIfNeeded];
  NSDictionary<NSString *, id> *postData = @{@"action":@"register",
                                             @"parameter":@{
                                                 @"udid":uuid,
                                                 @"device_log":@{
                                                     @"device":[self deviceName],
                                                     @"osv":[UIDevice currentDevice].systemVersion,
                                                     @"av":@""}}};
  WTLog(@"Registering...\n%@", postData);
  WTWeakify(self);
  [[NSURLSession.sharedSession dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(RegisterViewController);
    dispatch_async(dispatch_get_main_queue(), ^{
      [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
    });
    JSON(data);
    NSString *accessToken;
    if ([json[@"result"] boolValue]) {
      accessToken = json[@"data"][@"access_token"];
      if (accessToken.length == 0) {
        accessToken = nil;
      }
    }
    if (accessToken) {
      [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:kUserAccessTokenKey];
      [[NSUserDefaults standardUserDefaults] synchronize];
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf dismissViewControllerAnimated:YES completion:nil];
        if (strongSelf.successHandler) {
          strongSelf.successHandler();
        }
      });
    } else {
      dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [Common getRetryAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"retry", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
          [strongSelf registerDevice];
        }]];
        [strongSelf presentViewController:alertController animated:YES completion:nil];
      });
    }
    WTLog(@"Done registering.");
  }] resume];
}

- (NSString *)deviceName {
  struct utsname systemInfo;
  uname(&systemInfo);
  NSString *code = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
  static NSDictionary<NSString *, NSString *> *deviceNamesByCode = nil;
  if (!deviceNamesByCode) {
    deviceNamesByCode = @{@"i386"      :@"Simulator",
                          @"x86_64"    :@"Simulator",
                          
                          // iPhone - Based on https://support.apple.com/en-us/HT201296
                          @"iPhone1,1" :@"iPhone",
                          @"iPhone1,2" :@"iPhone 3G",
                          @"iPhone2,1" :@"iPhone 3GS",
                          @"iPhone3,1" :@"iPhone 4",
                          @"iPhone3,2" :@"iPhone 4",
                          @"iPhone3,3" :@"iPhone 4",
                          @"iPhone4,1" :@"iPhone 4s",
                          @"iPhone5,1" :@"iPhone 5",
                          @"iPhone5,2" :@"iPhone 5",
                          @"iPhone5,3" :@"iPhone 5c",
                          @"iPhone5,4" :@"iPhone 5c",
                          @"iPhone6,1" :@"iPhone 5s",
                          @"iPhone6,2" :@"iPhone 5s",
                          @"iPhone7,1" :@"iPhone 6 Plus",
                          @"iPhone7,2" :@"iPhone 6",
                          @"iPhone8,1" :@"iPhone 6s",
                          @"iPhone8,2" :@"iPhone 6s Plus",
                          @"iPhone8,4" :@"iPhone SE",
                          @"iPhone9,1" :@"iPhone 7",
                          @"iPhone9,2" :@"iPhone 7 Plus",
                          @"iPhone9,3" :@"iPhone 7",
                          @"iPhone9,4" :@"iPhone 7 Plus",
                          
                          // iPod - Based on https://support.apple.com/en-us/HT204217
                          @"iPod1,1"   :@"iPod touch",
                          @"iPod2,1"   :@"iPod touch (2nd generation)",
                          @"iPod3,1"   :@"iPod touch (3rd generation)",
                          @"iPod4,1"   :@"iPod touch (4th generation)",
                          @"iPod5,1"   :@"iPod touch (5th generation)",
                          @"iPod7,1"   :@"iPod touch (6th generation)",
                          
                          // iPad - Based on https://support.apple.com/en-us/HT201471
                          @"iPad1,1"   :@"iPad 1",
                          @"iPad2,1"   :@"iPad 2",
                          @"iPad2,2"   :@"iPad 2",
                          @"iPad2,3"   :@"iPad 2",
                          @"iPad2,4"   :@"iPad 2",
                          @"iPad2,5"   :@"iPad mini",
                          @"iPad2,6"   :@"iPad mini",
                          @"iPad2,7"   :@"iPad mini",
                          @"iPad3,1"   :@"iPad (3rd generation)",
                          @"iPad3,2"   :@"iPad (3rd generation)",
                          @"iPad3,3"   :@"iPad (3rd generation)",
                          @"iPad3,4"   :@"iPad (4th generation)",
                          @"iPad3,5"   :@"iPad (4th generation)",
                          @"iPad3,6"   :@"iPad (4th generation)",
                          @"iPad4,1"   :@"iPad Air",
                          @"iPad4,2"   :@"iPad Air",
                          @"iPad4,3"   :@"iPad Air",
                          @"iPad4,4"   :@"iPad mini 2",
                          @"iPad4,5"   :@"iPad mini 2",
                          @"iPad4,6"   :@"iPad mini 2",
                          @"iPad4,7"   :@"iPad mini 3",
                          @"iPad4,8"   :@"iPad mini 3",
                          @"iPad4,9"   :@"iPad mini 3",
                          @"iPad5,1"   :@"iPad mini 4",
                          @"iPad5,2"   :@"iPad mini 4",
                          @"iPad5,3"   :@"iPad Air 2",
                          @"iPad5,4"   :@"iPad Air 2",
                          @"iPad6,3"   :@"iPad Pro (9.7-inch)",
                          @"iPad6,4"   :@"iPad Pro (9.7-inch)",
                          @"iPad6,7"   :@"iPad Pro (12.9-inch)",
                          @"iPad6,8"   :@"iPad Pro (12.9-inch)",
                          @"iPad6,11"  :@"iPad (5th generation)",
                          @"iPad6,12"  :@"iPad (5th generation)"};
  }
  
  NSString *deviceName = deviceNamesByCode[code];
  if (!deviceName) {
    if ([code containsString:@"iPod"]) {
      deviceName = @"iPod touch";
    }
    else if ([code containsString:@"iPad"]) {
      deviceName = @"iPad";
    }
    else if ([code containsString:@"iPhone"]) {
      deviceName = @"iPhone";
    }
    else {
      deviceName = @"Unknown";
    }
  }
  return deviceName;
}

@end
