//
//  QueryViewController.m
//  Khemara
//
//  Created by Beniten on 6/21/16.
//  Copyright © 2016 Beniten. All rights reserved.
//

#import <SVPullToRefresh/SVPullToRefresh.h>
#import "QueryViewController.h"

@interface QueryViewController ()

@end

@implementation QueryViewController

- (instancetype)initWithObjectClass:(Class)ObjectClass {
  self = [super initWithNibName:nil bundle:nil];
  if (self){
    _objectClass = ObjectClass;
    [self _commonInit];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self _commonInit];
  }
  return self;
}

- (void)_commonInit {
  _objects = [NSMutableArray array];
  _loadsObjestsOnViewDidLoad = YES;
  _showsStatusView = YES;
}

- (void)dealloc {
  [_task cancel];
}

#pragma mark -

- (void)loadView {
  [super loadView];
  
  if (_showsStatusView) {
    _statusView = [[SVStatusView alloc] initWithFrame:self.view.bounds];
    _statusView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    WTWeakify(self);
    _statusView.reloadHandler = ^{
      WTStrongify(QueryViewController);
      if (strongSelf.isPagingEnabled) {
        [strongSelf loadObjectsForPage:strongSelf.page reset:NO];
      } else {
        [strongSelf reloadObjects];
      }
    };
    [self.view addSubview:_statusView];
  }
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self.view bringSubviewToFront:_statusView];
  _scrollView.infiniteScrollingView.activityIndicatorViewStyle = _statusView.indicatorView.activityIndicatorViewStyle;
  
  NSAssert(_objectClass, @"objectClass cannot be nil.");
  if (_loadsObjestsOnViewDidLoad) {
    [self reloadObjects];
  }
}

#pragma mark -

- (void)reloadObjects {
  [self loadObjectsForPage:1 reset:YES];
}

- (void)loadObjectsForNextPage {
  if (!self.loading) {
    [self loadObjectsForPage:_page + 1 reset:NO];
  }
}

- (void)loadObjectsForPage:(NSInteger)page reset:(BOOL)reset {
  [_task cancel];
  if (_objects.count == 0) {
    _statusView.state = SVStatusViewStateAnimating;
  }
  NSURLRequest *request = self.isPagingEnabled ? [self requestForPage:page] : [self request];
  WTWeakify(self);
  _task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(QueryViewController);
    dispatch_async(dispatch_get_main_queue(), ^{
      strongSelf->_task = nil;
      strongSelf->_error = error;
      JSON(data);
      id dictionaries = json;
      WTAssert(strongSelf.key.length > 0, @"Key must be set.");
      for (NSString *key in [strongSelf.key componentsSeparatedByString:@"."]) {
        dictionaries = dictionaries[key];
      }
      NSError *jsonError;
      NSArray *objects = [_objectClass arrayOfModelsFromDictionaries:dictionaries error:&jsonError];
      if (!objects && json[@"data"]) {
        objects = @[];
      }
      strongSelf->_jsonError = jsonError;
      [strongSelf.statusView setStateFromObject:objects];
      if (strongSelf.isPagingEnabled) {
        [strongSelf.scrollView.infiniteScrollingView stopAnimating];
      }
      if (objects) {
        strongSelf->_page = page;
        if (reset) {
          [_objects removeAllObjects];
        }
        [_objects addObjectsFromArray:objects];
        [strongSelf reloadDataWithObjects:objects reset:reset];
      }
      [strongSelf taskDidCompleteWithResponse:json error:error];
      if (strongSelf.isPagingEnabled && !error) {
        WTAssert(strongSelf.numberOfPages > 0, @"Could not determine numberOfPages after `taskDidCompleteWithResponse:errror:` is invoked.");
        BOOL showsInfiniteScrolling = page < strongSelf.numberOfPages;
        if (strongSelf.scrollView.showsInfiniteScrolling != showsInfiniteScrolling) {
          strongSelf.scrollView.showsInfiniteScrolling = showsInfiniteScrolling;
        }
      }
    });
  }];
  [_task resume];
}

- (void)resetData {
  [_task cancel];
  _task = nil;
  _error = nil;
  _jsonError = nil;
  _page = 0;
  [_objects removeAllObjects];
  [self reloadDataWithObjects:@[] reset:YES];
}

- (BOOL)loading {
  return _task && _task.state == NSURLSessionTaskStateRunning;
}

- (void)reloadDataWithObjects:(NSArray *)objects reset:(BOOL)reset {
  
}

- (void)taskDidCompleteWithResponse:(NSDictionary<NSString *, id> *)response error:(NSError *)error {
  if (self.isPagingEnabled) {
    WTAssert(NO, @"Subclass must override when `isPagingEnabled` is `YES`");
  }
}

- (NSURLRequest *)request {
  NSAssert(NO, @"Subclass must override.");
  return nil;
}

- (NSURLRequest *)requestForPage:(NSInteger)page {
  NSAssert(NO, @"Subclass must override.");
  return nil;
}

- (void)setScrollView:(UIScrollView *)scrollView {
  _scrollView = scrollView;
  if (scrollView) {
    if (_pagingEnabled) {
      WTWeakify(self);
      [_scrollView addInfiniteScrollingWithHandler:^{
        WTStrongify(QueryViewController);
        [strongSelf loadObjectsForNextPage];
      }];
      _scrollView.infiniteScrollingView.activityIndicatorViewStyle = _statusView.indicatorView.activityIndicatorViewStyle;
      _scrollView.showsInfiniteScrolling = NO;
    } else {
      [_scrollView.infiniteScrollingView removeFromSuperview];
    }
  }
}

@end
