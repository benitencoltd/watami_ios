//
//  RouletteTicketsViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/15/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTicketsViewController.h"
#import "WTRouletteTicket.h"

static NSNotificationName const kUserDidClaimRouletteTicketsNotification = @"kUserDidClaimRouletteTicketNotification";

@interface RouletteTicketsViewController : BaseTicketsViewController<WTRouletteTicket *>

@property (nonatomic, assign, readonly) WTRouletteTicketType type;

- (instancetype)initWithType:(WTRouletteTicketType)type tickets:(NSArray<WTRouletteTicket *> *)tickets tagcasts:(NSArray<NSDictionary<NSString *, id> *> *)tagcasts;

@end
