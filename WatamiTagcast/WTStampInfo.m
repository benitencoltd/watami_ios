//
//  WTStampInfo.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/25/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTStampInfo.h"

@implementation WTStampInfo

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"numberOfStamps":@"nstamps",
                                                                @"numberOfTickets":@"nticket",
                                                                @"generatable":@"prize"}];
}

@end
