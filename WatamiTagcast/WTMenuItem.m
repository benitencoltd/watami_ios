//
//  WTMenuItem.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTMenuItem.h"

@implementation WTMenuItem

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"id":@"menu_id",
                                                                @"name":@"menu_name",
                                                                @"text":@"menu_text",
                                                                @"imageURL":@"menu_image",
                                                                @"thumbnailURL":@"menu_thumbnail",
                                                                @"price":@"menu_price"}];
}

- (void)setImageURLWithNSArray:(NSArray<NSString *> *)array {
  _imageURL = [NSURL URLWithString:array.firstObject];
}

- (void)setThumbnailURLWithNSArray:(NSArray<NSString *> *)array {
  _thumbnailURL = [NSURL URLWithString:array.firstObject];
}

@end
