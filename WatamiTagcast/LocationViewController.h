//
//  LocationViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/3/167.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "DefaultViewController.h"

@interface LocationViewController : DefaultViewController

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIImageView *headerImageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@property (nonatomic, weak) IBOutlet UIImageView *staticMapImageView;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@end
