//
//  StampsViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StampsViewController : UICollectionViewController

@property (nonatomic, assign) NSUInteger numberOfStamps;

@end
