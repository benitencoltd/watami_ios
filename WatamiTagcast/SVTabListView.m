//
//  SVTabListView.m
//  SVKit
//
//  Created by Sereivoan Yong on 8/8/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import "SVTabListView.h"

@interface SVTabListViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UILabel *textLabel;

@end

@implementation SVTabListViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    _textLabel = [[UILabel alloc] initWithFrame:CGRectInset(self.contentView.bounds, 8.0, 8.0)];
    _textLabel.font = [UIFont systemFontOfSize:15.0];
    _textLabel.textAlignment = NSTextAlignmentCenter;
    _textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.contentView addSubview:_textLabel];
  }
  return self;
}

@end

@interface SVTabListView () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIView *indicatorView;

@end

@implementation SVTabListView {
  NSArray<NSNumber *> *_widths;
  BOOL _ignoresScroll;
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (instancetype)initWithItems:(NSArray<NSString *> *)items {
  self = [super initWithFrame:CGRectZero];
  if (self) {
    [self commonInit];
    self.items = items;
  }
  return self;
}

- (void)commonInit {
  self.userInteractionEnabled = YES;
  _extraTabPadding = 10.0;
  _centerTabsIfNotFilled = YES;
  _tabAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0], NSForegroundColorAttributeName:[UIColor blackColor]};
  
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.minimumLineSpacing = 0.0;
  flowLayout.minimumInteritemSpacing = 0.0;
  flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
  
  _collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
  _collectionView.alwaysBounceHorizontal = YES;
  _collectionView.backgroundColor = [UIColor clearColor];
  _collectionView.dataSource = self;
  _collectionView.delegate = self;
  _collectionView.directionalLockEnabled = YES;
  _collectionView.showsHorizontalScrollIndicator = NO;
  _collectionView.showsVerticalScrollIndicator = NO;
  [_collectionView registerClass:[SVTabListViewCell class] forCellWithReuseIdentifier:@"Cell"];
  [self addSubview:_collectionView];
  
  _indicatorView = [[UIView alloc] initWithFrame:CGRectZero];
  _indicatorView.backgroundColor = [UIColor blackColor];
  _indicatorView.layer.cornerRadius = 1.25;
  _indicatorView.layer.masksToBounds = YES;
  [_collectionView addSubview:_indicatorView];
  self.selectedColor = [UIApplication sharedApplication].delegate.window.tintColor;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  _collectionView.frame = self.bounds;
  [_collectionView.collectionViewLayout invalidateLayout];
  if (_bottomBorderLayer) {
    _bottomBorderLayer.frame = CGRectMake(0.0, self.frame.size.height - 0.5, self.frame.size.width, 0.5);
  }
  if (_centerTabsIfNotFilled) {
    _collectionView.scrollEnabled = !_scrollingEnabledIfNotFilled && ([self totalWidth] > _collectionView.frame.size.width);
  }
  _indicatorView.frame = [self indicatorFrameForItemAtIndex:_selectedIndex];
}

#pragma mark - Helpers

- (CGFloat)remainingPaddingWithExtraTabPadding {
  CGFloat totalWidth = [self totalWidth];
  if (totalWidth > _collectionView.frame.size.width || !_centerTabsIfNotFilled) {
    return _extraTabPadding;
  }
  return (_collectionView.frame.size.width - totalWidth) / 2.0 + _extraTabPadding;
}

- (void)setExtraTabPadding:(CGFloat)extraTabPadding {
  _extraTabPadding = extraTabPadding;
  [self refreshLayout];
  _indicatorView.frame = [self indicatorFrameForItemAtIndex:_selectedIndex];
}

- (CGFloat)totalWidth {
  CGFloat totalWidth = _extraTabPadding * 2.0;
  for (NSNumber *width in _widths) {
    totalWidth += width.floatValue;
  }
  return totalWidth;
}

- (CGFloat)offsetXForItemAtIndex:(NSInteger)index {
  return [self offsetXForItemAtIndex:index withInitialValue:[self remainingPaddingWithExtraTabPadding]];
}

- (CGFloat)offsetXForItemAtIndex:(NSInteger)index withInitialValue:(CGFloat)initialValue {
  CGFloat offsetX = initialValue;
  for (NSInteger i = 0; i < index; i++) {
    offsetX += _widths[i].floatValue;
  }
  return offsetX;
}

- (void)refreshLayout {
  if (!_fillsEqually) {
    NSMutableArray<NSNumber *> *widths = [NSMutableArray array];
    for (NSString *item in _items) {
      CGFloat width = ceil([item sizeWithAttributes:_tabAttributes].width + _extraTabPadding * 2.0);
      [widths addObject:@(width)];
    }
    _widths = widths;
  }
  [_collectionView reloadData];
}

- (void)setSelectedColor:(UIColor *)selectedColor {
  _selectedColor = selectedColor;
  [_collectionView reloadData];
  _indicatorView.backgroundColor = selectedColor;
}

- (CGRect)indicatorFrameForItemAtIndex:(NSInteger)index {
  return CGRectMake([self offsetXForItemAtIndex:index], _collectionView.frame.size.height - 2.5, _widths[index].floatValue, 2.5);
}

#pragma mark - Properties

- (void)setItems:(NSArray<NSString *> *)items {
  _selectedIndex = 0;
  _items = items;
  [self refreshLayout];
}

- (void)setTabAttributes:(NSDictionary<NSString *, id> *)tabAttributes {
  _tabAttributes = tabAttributes;
  if (!_selectedColor) {
    _selectedColor = tabAttributes[NSForegroundColorAttributeName];
    _indicatorView.backgroundColor = _selectedColor;
  }
  [self refreshLayout];
  _indicatorView.frame = [self indicatorFrameForItemAtIndex:_selectedIndex];
}

- (void)setBottomBorderEnabled:(BOOL)bottomBorderEnabled {
  if (_bottomBorderEnabled == bottomBorderEnabled) return;
  
  _bottomBorderEnabled = bottomBorderEnabled;
  if (bottomBorderEnabled) {
    if (!_bottomBorderLayer) {
      _bottomBorderLayer = [[CALayer alloc] init];
      _bottomBorderLayer.frame = CGRectMake(0.0, self.frame.size.height - 0.5, self.frame.size.width, 0.5);
      _bottomBorderLayer.backgroundColor = [UIColor groupTableViewBackgroundColor].CGColor;
      [self.layer addSublayer:_bottomBorderLayer];
    }
  } else {
    [_bottomBorderLayer removeFromSuperlayer];
    _bottomBorderLayer = nil;
  }
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
  [self setSelectedIndex:selectedIndex animated:NO];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated {
  [self setSelectedIndex:selectedIndex animated:animated fromPrivate:NO];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated fromPrivate:(BOOL)fromPrivate {
  if (_selectedIndex == selectedIndex) return;
  
  _ignoresScroll = YES;
  [UIView animateWithDuration:animated ? 0.4 : 0.0 delay:0.0 usingSpringWithDamping:0.75 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
    _indicatorView.frame = [self indicatorFrameForItemAtIndex:selectedIndex];
  } completion:^(BOOL finished) {
    _ignoresScroll = NO;
  }];
  UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:selectedIndex inSection:0]];
  CGRect cellFrame = cell.frame;
  CGFloat padding = [self remainingPaddingWithExtraTabPadding];
  if (!CGRectContainsRect(_collectionView.bounds, cellFrame)) {
    if (selectedIndex > _selectedIndex) {
      cellFrame.origin.x += padding;
    } else {
      cellFrame.origin.x -= padding;
    }
    [_collectionView scrollRectToVisible:cellFrame animated:YES];
  }
  [self refreshTabColorForSelectedIndex:selectedIndex unselectedIndex:_selectedIndex];
  _selectedIndex = selectedIndex;
  if (fromPrivate && _selectionHandler) {
    _selectionHandler(selectedIndex);
  }
}

- (void)setIndicatorProgress:(CGFloat)progress scrollingLeft:(BOOL)scrollingLeft {
  if (_ignoresScroll) return;
  
  NSInteger lowerIndex = floor(progress);
  NSInteger upperIndex = lowerIndex + 1;
  CGFloat lowerProgress = progress - lowerIndex;
  CGFloat upperProgress = 1 - lowerProgress;
  CGFloat padding = [self remainingPaddingWithExtraTabPadding];
  
  if (lowerIndex < 0) {
    _indicatorView.frame = CGRectMake(padding, self.frame.size.height - 2.5, _widths[0].floatValue * (1 - -progress), 2.5);
  } else if (upperIndex > _items.count - 1) {
    CGFloat upperItemWidth = _widths[lowerIndex].floatValue * upperProgress;
    CGFloat offsetX = [self offsetXForItemAtIndex:lowerIndex withInitialValue:padding] + _widths[lowerIndex].floatValue * lowerProgress;
    _indicatorView.frame = CGRectMake(offsetX, self.frame.size.height - 2.5, upperItemWidth, 2.5);
  } else {
    CGFloat upperItemWidth = _widths[upperIndex].floatValue * lowerProgress;
    CGFloat lowerItemWidth = _widths[lowerIndex].floatValue * upperProgress;
    CGFloat offsetX = [self offsetXForItemAtIndex:lowerIndex withInitialValue:padding] + (_widths[lowerIndex].floatValue * lowerProgress);
    _indicatorView.frame = CGRectMake(offsetX, self.frame.size.height - 2.5, lowerItemWidth + upperItemWidth, 2.5);
  }
  
  if (_collectionView.contentOffset.x > _indicatorView.frame.origin.x - padding) {
    _collectionView.contentOffset = CGPointMake(_indicatorView.frame.origin.x - padding, 0.0);
  } else if (_collectionView.contentOffset.x + _collectionView.frame.size.width - padding < CGRectGetMaxX(_indicatorView.frame)) {
    _collectionView.contentOffset = CGPointMake(CGRectGetMaxX(_indicatorView.frame) - _collectionView.frame.size.width + padding, 0.0);
  }
  
  BOOL shouldUpdateNewIndex = (scrollingLeft ? lowerProgress : upperProgress) > 0.5;
  if (shouldUpdateNewIndex) {
    NSInteger newIndex = scrollingLeft ? upperIndex : lowerIndex;
    NSInteger oldIndex = scrollingLeft ? lowerIndex : upperIndex;
    _selectedIndex = newIndex;
    [self refreshTabColorForSelectedIndex:newIndex unselectedIndex:oldIndex];
  }
}

- (void)refreshTabColorForSelectedIndex:(NSInteger)selectedIndex unselectedIndex:(NSInteger)unselectedIndex {
  SVTabListViewCell *selectedCell = (id)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:selectedIndex inSection:0]];
  selectedCell.textLabel.textColor = _selectedColor;
  SVTabListViewCell *unselectedCell = (id)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:unselectedIndex inSection:0]];
  unselectedCell.textLabel.textColor = _tabAttributes[NSForegroundColorAttributeName];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return _items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  return [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
  if (!_centerTabsIfNotFilled || [self totalWidth] > collectionView.frame.size.width) {
    return UIEdgeInsetsMake(0.0, _extraTabPadding, 0.0, _extraTabPadding);
  } else {
    CGFloat padding = [self remainingPaddingWithExtraTabPadding];
    return UIEdgeInsetsMake(0.0, padding, 0.0, padding);
  }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return CGSizeMake(_widths[indexPath.item].floatValue, collectionView.frame.size.height);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(SVTabListViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
  cell.textLabel.font = _tabAttributes[NSFontAttributeName];
  cell.textLabel.textColor = indexPath.item == _selectedIndex ? _selectedColor : _tabAttributes[NSForegroundColorAttributeName];
  cell.textLabel.text = _items[indexPath.item];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  [self setSelectedIndex:indexPath.item animated:YES fromPrivate:YES];
}

@end
