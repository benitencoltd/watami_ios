//
//  WTVIPInfo.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/24/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTVIPInfo.h"

@implementation WTVIPInfoTicketInfo

@end

@implementation WTVIPInfo

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"isVIP":@"rank.vip",
                                                                @"registered":@"rank.member",
                                                                @"photoURL":@"rank.photo",
                                                                @"ticketInfos":@"lotteries",
                                                                @"numberOfStamps":@"num_stamp",
                                                                @"numberOfIncentiveTickets":@"num_ticket_incentive",
                                                                
                                                                @"rankNumber":@"ranklevel.allranklevel",
                                                                @"monthlyRankNumber":@"ranklevel.monthlyranklevel",
                                                                
                                                                @"currentRank":@"rank",
                                                                @"previousRank":@"prev",
                                                                @"nextRank":@"next"}];
}

- (void)setPhotoURLWithNSString:(NSString *)string {
  if (string.length > 0) {
    _photoURL = [NSURL URLWithString:string];
  }
}

- (void)setTicketInfosWithNSArray:(NSArray<NSDictionary<NSString *, id> *> *)array {
  _ticketInfos = [WTVIPInfoTicketInfo arrayOfModelsFromDictionaries:array error:NULL];
}

- (void)setRankLevelWithNSString:(NSString *)string {
  _rankNumber = string.length > 0 ? string.integerValue : WTInvalidRankNumber;
}

- (void)setMonthlyRankLevelWithNSString:(NSString *)string {
  _monthlyRankNumber = string.length > 0 ? string.integerValue : WTInvalidRankNumber;
}

- (void)setCurrentRankWithNSDictionary:(NSDictionary<NSString *, id> *)dictionary {
  _currentRank = [[WTRank alloc] initWithTitle:dictionary[@"type"] type:[dictionary[@"rank_type"] integerValue]];
}

@end
