//
//  VIPUsersViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/28/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <SVPullToRefresh/UIScrollView+SVInfiniteScrolling.h>

#import "VIPUsersViewController.h"
#import "LanguageManager.h"

#import "VIPUserTableViewCell.h"
#import "SVStatusView.h"

#import "WTVIPUser.h"

@interface VIPUsersViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) SVStatusView *statusView;

@end

@implementation VIPUsersViewController {
  NSUInteger _numberOfPages;
  NSUInteger _currentPage;
  NSMutableArray<WTVIPUser *> *_users;
  NSURLSessionDataTask *_task;
  UILabel *_headerMonthLabel;
}

- (instancetype)init {
  self = [super initWithNibName:nil bundle:nil];
  if (self) {
    self.title = NSLocalizedString(@"rank_users", nil);
    _users = [NSMutableArray array];
    _currentPage = 1;
  }
  return self;
}

- (void)dealloc {
  [_task cancel];
  WTDeallocLog(self);
}

#pragma mark -

- (void)loadView {
  [super loadView];
  
  _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
  _tableView.allowsSelection = NO;
  _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
  _tableView.backgroundColor = [UIColor whiteColor];
  _tableView.rowHeight = kVIPUserRowHeight;
  _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  [_tableView registerClass:[VIPUserTableViewCell class] forCellReuseIdentifier:NSStringFromClass([VIPUserTableViewCell class])];
  _tableView.dataSource = self;
  _tableView.delegate = self;
  [self.view addSubview:_tableView];
  
  _statusView = [[SVStatusView alloc] initWithFrame:self.view.bounds];
  _statusView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
  WTWeakify(self);
  _statusView.reloadHandler = ^{
    WTStrongify(VIPUsersViewController);
    [strongSelf reloadUsers];
  };
  [self.view addSubview:_statusView];
  
  if (_isMonthly) {
    [self setupHeaderViewForMonthly];
    _statusView.insets = UIEdgeInsetsMake(_tableView.tableHeaderView.frame.size.height, 0.0, 0.0, 0.0);
  }
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self reloadUsers];
}

#pragma mark -

- (void)setupHeaderViewForMonthly {
  UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _tableView.frame.size.width, 84.0)];
  headerView.backgroundColor = [UIColor whiteColor];
  _tableView.tableHeaderView = headerView;
  
  UIImageView *calenderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12.0, 12.0, 60.0, 60.0)];
  calenderImageView.contentMode = UIViewContentModeCenter;
  calenderImageView.backgroundColor = DARK_RED_COLOR;
  calenderImageView.tintColor = [UIColor whiteColor];
  calenderImageView.image = [[[UIImage imageNamed:@"calendar.png"] imageByScalingAspectFitSize:CGSizeMake(30.0, 30.0)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  calenderImageView.layer.cornerRadius = 30.0;
  calenderImageView.layer.masksToBounds = YES;
  [headerView addSubview:calenderImageView];
  
  UIFont *titleFont = [UIFont appFontOfSize:14.0], *monthFont = [UIFont appFontOfSize:18.0];
  CGFloat titleHeight = ceil(titleFont.lineHeight), monthHeight = ceil(monthFont.lineHeight);
  
  UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(calenderImageView.frame) + 8.0, (CGRectGetHeight(headerView.frame) - (titleHeight + 4.0 + monthHeight)) / 2, CGRectGetWidth(headerView.frame) - CGRectGetMaxX(calenderImageView.frame) - 16.0, titleHeight)];
  titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
  titleLabel.font = titleFont;
  titleLabel.textColor = [UIColor darkGrayColor];
  titleLabel.text = NSLocalizedString(@"ranking_within", nil);
  [headerView addSubview:titleLabel];
  
  UILabel *monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame) + 4.0, CGRectGetWidth(titleLabel.frame), monthHeight)];
  monthLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
  monthLabel.font = monthFont;
  [headerView addSubview:monthLabel];
  _headerMonthLabel = monthLabel;
  NSInteger month = [[NSCalendar currentCalendar] component:NSCalendarUnitMonth fromDate:[[NSDate alloc] init]];
  if ([[[NSUserDefaults standardUserDefaults] stringForKey:kUserLanguageKey] isEqualToString:kLanguageKM]) {
    NSArray<NSString *> *months = @[@"មករា", @"កុម្ភះ", @"មីនា", @"មេសា", @"ឧសភា", @"មិថុនា", @"កក្កដា", @"សីហា", @"កញ្ញា", @"តុលា", @"វិច្ឆិកា", @"ធ្នូ"];
    monthLabel.text = months[month - 1].uppercaseString;
  } else {
    NSArray<NSString *> *months = @[@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December"];
    monthLabel.text = months[month - 1].uppercaseString;
  }
}

#pragma mark -

- (void)reloadUsers {
  [self loadUsersForPage:1];
}

- (void)loadUsersForPage:(NSInteger)page {
  if (_numberOfPages > 0 && page > _numberOfPages) {
    return;
  }
  [_task cancel];
  if (page == 1) {
    self.statusView.state = SVStatusViewStateAnimating;
  }
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":_isMonthly ? @"getvipmembermonthlyrank" : @"getvipmemberlist",
                                             @"lang":[Common getLang],
                                             @"parameter":@{
                                                 @"page":@(page)}};
  WTWeakify(self);
  _task = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(VIPUsersViewController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      [strongSelf.tableView.infiniteScrollingView stopAnimating];
      [strongSelf.statusView setStateFromObject:json[@"data"][@"member"]];
    });
    if (json[@"data"][@"member"]) {
      NSArray<WTVIPUser *> *users = [WTVIPUser arrayOfModelsFromDictionaries:json[@"data"][@"member"] error:NULL];
      strongSelf->_numberOfPages = [json[@"data"][@"totalpage"] unsignedIntegerValue];
      strongSelf->_currentPage = page;
      [strongSelf->_users addObjectsFromArray:users];
      
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf.tableView reloadData];
        if (page == 1 && page < strongSelf->_numberOfPages) {
          [strongSelf.tableView addInfiniteScrollingWithHandler:^{
            WTStrongify(VIPUsersViewController);
            [strongSelf loadUsersForPage:strongSelf->_currentPage + 1];
          }];
          strongSelf.tableView.infiniteScrollingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        }
        if (page >= strongSelf->_numberOfPages) {
          strongSelf.tableView.showsInfiniteScrolling = NO;
        }
      });
    }
  }];
  [_task resume];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return _users.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  VIPUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([VIPUserTableViewCell class]) forIndexPath:indexPath];
  WTVIPUser *user = _users[indexPath.row];
  cell.numberLabel.text = [NSString stringWithFormat:@"%i", (int)indexPath.row + 1];
  if (user.photoURL) {
    cell.photoImageView.backgroundColor = [UIColor whiteColor];
    [cell.photoImageView sd_setImageWithURL:user.photoURL];
  } else {
    cell.photoImageView.backgroundColor = [UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:1.0];
    cell.photoImageView.image = [[[UIImage imageNamed:@"user_placeholder.png"] imageWithInsetPercent:0.7] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  }
  cell.nameLabel.text = user.name;
  cell.memberTypeLabel.text = user.rank.title;
  cell.stampCountLabel.text = [NSString stringWithFormat:@"%u", (unsigned)user.numberOfStamps];
  return cell;
}

@end
