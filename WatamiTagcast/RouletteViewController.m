//
//  RouletteViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/15/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <Tagcast/TGCTagcastManager.h>
#import "RouletteViewController.h"
#import "RouletteGroupTicketsViewController.h"
#import "Constants.h"

// prizetype gold 1, silver 2, bronze 3, lose 4

@interface RouletteViewController () <TGCTagcastMangerDelegate, CBCentralManagerDelegate>

@end

@implementation RouletteViewController {
  CBCentralManager *_bluetoothManager;
  NSArray<NSDictionary<NSString *, id> *> *_enabledTagcasts;
  BOOL _spinning;
  BOOL _hasMatchedTagcasts;
  BOOL _shouldScanTagcasts;
  
  NSArray<NSNumber *> *_prizeRadians;
  NSInteger _prizeType;
  NSTimer *_timer;
  NSURLSessionDataTask *_reloadEnabledTagcastsTask, *_reloadRouletteStatusTask;
  
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    self.title = NSLocalizedString(@"roulette", nil);
    _bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionShowPowerAlertKey:@NO}];
    _prizeRadians = @[@0.0f, @-M_PI_4, @(-M_PI_4 * 2), @(-M_PI_4 * 3)];
  }
  return self;
}

- (void)dealloc {
  [_reloadEnabledTagcastsTask cancel];
  [_reloadRouletteStatusTask cancel];
  WTDeallocLog(self);
}

#pragma mark - View Life Cycle

- (void)viewDidLoad {
  [super viewDidLoad];
  
  _containerView.hidden = YES;
  [TGCTagcastManager provideAPIKey:kTagcastAPIKey];
  [[TGCTagcastManager sharedManager] prepare];
  CGRect rect = CGRectMake(0.0, 0.0, UIScreen.mainScreen.bounds.size.width - 48.0, UIScreen.mainScreen.bounds.size.height - CGRectGetMaxY(self.navigationController.navigationBar.frame) - self.tabBarController.tabBar.frame.size.height - 48.0);
  AddShadowToViewWithRect(_containerView, rect);
  [self reloadStatus];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  [Common trackScreen:@"lottery_screen"];
  [TGCTagcastManager sharedManager].delegate = self;
  if (_shouldScanTagcasts) {
    [[TGCTagcastManager sharedManager] startScan];
  }
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  
  [[TGCTagcastManager sharedManager] stopScan];
}

#pragma mark - Networking

- (void)reloadStatus {
  [_reloadRouletteStatusTask cancel];
  [self.loadingIndicatorView startAnimating];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getlotterystatus",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _reloadRouletteStatusTask = [NSURLSession.sharedSession dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(RouletteViewController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      [strongSelf.loadingIndicatorView stopAnimating];
      if (json) {
        NSInteger statusCode = [json[@"data"][@"status"] integerValue];
        if (statusCode == 1) {
          strongSelf.containerView.hidden = NO;
          [strongSelf reloadEnabledTagcasts];
        }
        else if (statusCode == 2) {
          strongSelf.containerView.hidden = NO;
          [strongSelf reloadEnabledTagcasts];
          [Common showAlert:json[@"data"][@"description"] title:nil _self:strongSelf];
        }
        else {
          strongSelf.containerView.hidden = YES;
//          [Common showAlert:json[@"message"] title:nil _self:strongSelf];
          UILabel *messageLabel = [[UILabel alloc] init];
          messageLabel.numberOfLines = 0;
          messageLabel.textAlignment = NSTextAlignmentCenter;
          messageLabel.textColor = UIColor.whiteColor;
          messageLabel.text = json[@"message"];
          messageLabel.font = [UIFont appFontOfSize:16.0];
          CGFloat height = [messageLabel sizeThatFits:CGSizeMake(self.view.frame.size.width - 40.0, CGFLOAT_MAX)].height;
          messageLabel.frame = CGRectMake(20.0, (self.view.frame.size.height - height) / 2, self.view.frame.size.width - 40.0, height);
          messageLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin;
          [self.view addSubview:messageLabel];
        }
      } else {
        
      }
    });
  }];
  [_reloadRouletteStatusTask resume];
}

- (void)reloadEnabledTagcasts {
  _shouldScanTagcasts = YES;
  [[TGCTagcastManager sharedManager] startScan]; // Start scan to save user's time, stop if not available
  [_reloadEnabledTagcastsTask cancel];
  [_playLoadingIndicatorView startAnimating];
  _startButton.hidden = YES;
  _enabledTagcasts = nil;
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"gettagcast",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _reloadEnabledTagcastsTask = [NSURLSession.sharedSession dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(RouletteViewController);
    JSON(data);
    if (json) {
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf->_playLoadingIndicatorView stopAnimating];
        strongSelf->_startButton.hidden = NO;
      });
      if ([json[@"result"] boolValue]) {
        strongSelf->_enabledTagcasts = json[@"data"][@"tagcasts"];
        strongSelf->_hasMatchedTagcasts = NO;
      } else {
        dispatch_async(dispatch_get_main_queue(), ^{
          [Common showAlert:json[@"message"] title:nil _self:strongSelf];
        });
        strongSelf->_shouldScanTagcasts = NO;
        [[TGCTagcastManager sharedManager] stopScan];
      }
    } else {
      [strongSelf reloadEnabledTagcasts];
    }
  }];
  [_reloadEnabledTagcastsTask resume];
}

- (void)start:(UIButton *)sender {
  if (_spinning) {
    [_timer invalidate];
    _timer = nil;
    _spinning = NO;
    NSString *prizeImageName;
    if (_prizeType == 1) {
      prizeImageName = @"goldLottery.png";
    } else if (_prizeType == 2) {
      prizeImageName = @"silverLottery.png";
    } else if (_prizeType == 3) {
      prizeImageName = @"bronzeLottery.png";
    } else if (_prizeType == 4) {
      prizeImageName = @"loseLottery.png";
    }
    _wheelImageView.transform = CGAffineTransformMakeRotation(_prizeRadians[_prizeType - 1].floatValue);
    _startButton.hidden = YES;
    _hasMatchedTagcasts = NO;
    
    UIImageView *prizeImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    prizeImageView.alpha = 0.0;
    prizeImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    prizeImageView.backgroundColor = self.view.backgroundColor;
    prizeImageView.userInteractionEnabled = YES;
    [prizeImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePrizeImageView:)]];
    [self.view addSubview:prizeImageView];
    
    double delayInSeconds = 0.75;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^{
      _prizeType = 0;
      prizeImageView.image = [UIImage imageNamed:prizeImageName];
      [UIView animateWithDuration:0.5 animations:^{
        prizeImageView.alpha = 1.0;
      } completion:^(BOOL finished) {
        [_startButton setImage:[UIImage imageNamed:@"btnStartLottery.png"] forState:UIControlStateNormal];
        _startButton.hidden = NO;
        self.allowsUserInteraction = YES;
      }];
    });
    return;
  } else {
    if (!_hasMatchedTagcasts) {
      [Common showAlert:NSLocalizedString(@"lottery_message", nil) title:nil _self:self];
      return;
    }
    self.allowsUserInteraction = NO;
    [_playLoadingIndicatorView startAnimating];
    _startButton.hidden = YES;
    
    NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                               @"action":@"getlotteryresult",
                                               @"lang":[Common getLang]};
    WTWeakify(self);
    [[[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
      WTStrongify(RouletteViewController);
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf->_playLoadingIndicatorView stopAnimating];
        strongSelf->_startButton.hidden = NO;
      });
      JSON(data)
      if (json) {
        if ([json[@"data"][@"status"] boolValue]) {
          dispatch_async(dispatch_get_main_queue(), ^{
            [strongSelf->_startButton setImage:[UIImage imageNamed:@"btnStopLottery.png"] forState:UIControlStateNormal];
            strongSelf->_prizeType = [json[@"data"][@"prize"][@"type"] integerValue];
            strongSelf->_spinning = YES;
            strongSelf->_timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:strongSelf selector:@selector(rotate:) userInfo:nil repeats:YES];
          });
          [Common trackEvent:@"play_roulette" action:@"play_roulette" label:@"play_roulette"];
        } else {
          [Common showAlert:json[@"message"] title:nil _self:strongSelf];
        }
      } else {
        dispatch_async(dispatch_get_main_queue(), ^{
          UIAlertController *alertController = [Common getRetryAlert];
          [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"retry", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [strongSelf start:sender];
          }]];
          [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleDefault handler:nil]];
          [strongSelf presentViewController:alertController animated:YES completion:nil];
        });
      }
    }] resume];
  }
}

- (void)showTickets:(UIButton *)sender {
  if (_spinning) {
    return;
  }
  RouletteGroupTicketsViewController *groupTicketsViewController = [[RouletteGroupTicketsViewController alloc] initWithTagcasts:_enabledTagcasts];
  [self.navigationController pushViewController:groupTicketsViewController animated:YES];
}

- (void)rotate:(NSTimer *)timer {
  [UIView animateWithDuration:0.5 animations:^{
    _wheelImageView.transform = CGAffineTransformRotate(_wheelImageView.transform, M_PI_4);
  }];
}

- (void)hidePrizeImageView:(UITapGestureRecognizer *)gestureRecognizer {
  UIImageView *prizeImageView = (id)gestureRecognizer.view;
  [UIView animateWithDuration:0.5 animations:^{
    prizeImageView.alpha = 0.0;
  } completion:^(BOOL finished) {
    [prizeImageView removeFromSuperview];
  }];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
  if (central.state != CBCentralManagerStatePoweredOn) {
    _hasMatchedTagcasts = NO;
  }
}

#pragma mark - TGCTagcastMangerDelegate

- (void)tagcastManager:(TGCTagcastManager *)manager didDiscoveredTagcast:(NSDictionary<NSString *, id> *)tagcast {
  NSString *serialIdentifier = tagcast[kTGCKeyData][kTGCKeySerialIdentifier];
  for (NSDictionary<NSString *, id> *enabledTagcast in _enabledTagcasts) {
    if ([enabledTagcast[@"enable_lottery"] boolValue] && [enabledTagcast[@"serial_number"] isEqualToString:serialIdentifier]) {
      _hasMatchedTagcasts = YES;
      break;
    }
  }
}

- (void)tagcastManager:(TGCTagcastManager *)manager didScannedTagcasts:(NSArray<NSDictionary<NSString *, id> *> *)tagcasts {
  if (tagcasts.count == 0) {
    _hasMatchedTagcasts = NO;
  }
}

@end
