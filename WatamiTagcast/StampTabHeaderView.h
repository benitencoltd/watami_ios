//
//  StampTabHeaderView.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "SVTabHeaderView.h"

@interface StampTabHeaderView : SVTabHeaderView

@property (nonatomic, strong, readonly) UIButton *rankButton;

@property (nonatomic, strong, readonly) UILabel *rankTitleLabel;

@property (nonatomic, strong, readonly) UIButton *checkInButton;
@property (nonatomic, strong, readonly) UIButton *generateButton;
@property (nonatomic, strong, readonly) UIActivityIndicatorView *checkInIndicatorView;
@property (nonatomic, strong, readonly) UIActivityIndicatorView *generateIndicatorView;

@end
