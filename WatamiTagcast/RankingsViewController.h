//
//  RankingsViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/21/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WTStampStatus.h"

@interface RankingsViewController : UITableViewController

@property (nonatomic, strong, readonly) NSArray<WTStampRanking *> *rankings;

- (instancetype)initWithRankings:(NSArray<WTStampRanking *> *)rankings;

@end
