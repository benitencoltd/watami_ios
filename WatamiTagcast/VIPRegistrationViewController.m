//
//  VIPRegistrationViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/27/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "VIPRegistrationViewController.h"
#import "SVKeyboardControl.h"

@interface VIPRegistrationViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) WTVIPInfo *vipInfo;

@end

@implementation VIPRegistrationViewController {
  NSURLSessionDataTask *_task;
  BOOL _loadedBefore;
  NSString *_gender;
  BOOL _photoChanged;
}

- (instancetype)initWithProfile:(WTProfile *)profile vipInfo:(WTVIPInfo *)vipInfo {
  self = [super initWithNibName:NSStringFromClass([VIPRegistrationViewController class]) bundle:nil];
  if (self) {
    self.hidesBottomBarWhenPushed = YES;
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStylePlain target:self action:@selector(reset:)];
    _profile = profile;
    _gender = @"Male";
    _vipInfo = vipInfo;
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self.navigationController.navigationBar removeBackBarButtonItemTitle];
  
  _photoImageView.image = [[_photoImageView.image imageWithInsetPercent:0.75] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  _titleLabel.font = [UIFont boldAppFontOfSize:_titleLabel.font.pointSize];
  _titleLabel.text = NSLocalizedString(_profile ? @"edit_profile_mem" : @"create_profile_mem", nil);
  
  _genderTitleLabel.text = NSLocalizedString(@"your_gender", nil);
  _dobTitleLabel.text = NSLocalizedString(@"your_dob", nil);
  _mLabel.text = NSLocalizedString(@"your_male", nil);
  _fLabel.text = NSLocalizedString(@"your_female", nil);
  _usernameTextField.placeholder = NSLocalizedString(@"your_name", nil);
  _emailTextField.placeholder = NSLocalizedString(@"your_email", nil);
  _dobYearTextField.placeholder = NSLocalizedString(@"your_year", nil);
  _dobMonthTextField.placeholder = NSLocalizedString(@"your_month", nil);
  _dobDayTextField.placeholder = NSLocalizedString(@"your_day", nil);
  [_saveButton setTitle:NSLocalizedString(@"save_vip", nil) forState:UIControlStateNormal];
  
  [_uploadButton addBorderColor:[UIColor whiteColor] borderWidth:3.0 cornerRadius:16.0];
  UIImage *uploadButtonImage = [[[_uploadButton imageForState:UIControlStateNormal] imageWithInsetPercent:0.75] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  [_uploadButton setImage:uploadButtonImage forState:UIControlStateNormal];
  [_mCheckBoxButton setImage:[[_mCheckBoxButton imageForState:UIControlStateNormal] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
  [_mCheckBoxButton addBorderColor:[UIColor whiteColor] borderWidth:2.0 cornerRadius:10.0];
  [_fCheckBoxButton setImage:[[_fCheckBoxButton imageForState:UIControlStateNormal] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
  [_fCheckBoxButton addBorderColor:[UIColor whiteColor] borderWidth:2.0 cornerRadius:10.0];
  
  for (ACFloatingTextField *textField in @[_usernameTextField, _emailTextField, _dobYearTextField, _dobMonthTextField, _dobDayTextField]) {
    textField.selectedLineColor = [UIColor blackColor];
    textField.selectedPlaceholderColor = [UIColor blackColor];
    textField.inputAccessoryView = [[UIView alloc] initWithFrame:CGRectZero];
  }
  
  WTWeakify(self);
  [self.view addKeyboardPanningWithFrameHandler:nil constraintHandler:^(CGRect keyboardFrame, SVKeyboardState state) {
    WTStrongify(VIPRegistrationViewController);
    CGPoint previousOffset = strongSelf.scrollView.contentOffset;
    CGFloat visibleKeyboardHeight = [UIScreen mainScreen].bounds.size.height - keyboardFrame.origin.y;
    
    CGFloat constant = MAX((strongSelf.view.frame.size.height - visibleKeyboardHeight - strongSelf.contentView.frame.size.height) / 2, 24.0);
    strongSelf.topScrollViewConstraint.constant = constant;
    strongSelf.bottomScrollViewConstraint.constant = constant + visibleKeyboardHeight;
    [strongSelf.view layoutIfNeeded]; // This will reset scrollView's offset.
    
    if (state == SVKeyboardStateDragging) {
      strongSelf.scrollView.contentOffset = previousOffset;
    }
  }];
  
  if (_profile) {
    [self fillData];
  }
  [self.view setNeedsLayout];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  if (!CGSizeEqualToSize(_scrollView.contentSize, _contentView.bounds.size)) {
    _scrollView.contentSize = _contentView.bounds.size;
  }
  if (!_loadedBefore) {
    _loadedBefore = YES;
    _topScrollViewConstraint.constant = MAX((self.view.frame.size.height - _contentView.frame.size.height) / 2, 24.0);
    _bottomScrollViewConstraint.constant = _topScrollViewConstraint.constant;
  }
}

- (void)didTapView:(UITapGestureRecognizer *)gestureRecognizer {
  [self.view endEditing:YES];
}

- (void)selectGender:(UITapGestureRecognizer *)gestureRecognizer {
  UILabel *label = (UILabel *)gestureRecognizer.view;
  BOOL isMale = label.tag == 1;
  if (isMale) {
    _mLabel.backgroundColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    _fLabel.backgroundColor = [UIColor colorWithRed:120.0/255.0 green:144.0/255.0 blue:156.0/255.0 alpha:1.0];
    _mCheckBoxButton.hidden = NO;
    _fCheckBoxButton.hidden = YES;
  } else {
    _fLabel.backgroundColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    _mLabel.backgroundColor = [UIColor colorWithRed:120.0/255.0 green:144.0/255.0 blue:156.0/255.0 alpha:1.0];
    _mCheckBoxButton.hidden = YES;
    _fCheckBoxButton.hidden = NO;
  }
  _gender = isMale ? @"Male" : @"Female";
}

- (void)fillData {
  [_photoImageView sd_setImageWithURL:_profile.photoURL];
  _usernameTextField.text = _profile.name;
  _emailTextField.text = _profile.email;
  NSArray<NSString *> *dobComponents = [_profile.dob componentsSeparatedByString:@"-"];
  _dobYearTextField.text = dobComponents[0];
  _dobMonthTextField.text = dobComponents[1];
  _dobDayTextField.text = dobComponents[2];
  if ([_profile.gender.lowercaseString isEqualToString:@"female"]) {
    [self selectGender:_fLabel.gestureRecognizers.firstObject];
  }
}

- (void)reset:(UIBarButtonItem *)sender {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  NSDictionary<NSString *, id> *postData = @{@"action":@"addvipmember",
                                             @"access_token":[Common getToken],
                                             @"lang":[Common getLang],
                                             @"reset":@YES};
  WTWeakify(self);
  NSURLRequest *request = [Common requestWithParameters:postData dataDictionary:nil];
  [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(VIPRegistrationViewController);
    dispatch_async(dispatch_get_main_queue(), ^{
      [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
      JSON(data)
      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Watami" message:json[@"message"] preferredStyle:UIAlertControllerStyleAlert];
      [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:nil]];
      [strongSelf presentViewController:alertController animated:YES completion:nil];
    });
  }] resume];
}

- (void)uploadPhoto:(UIButton *)sender {
  UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
  imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
  imagePickerController.mediaTypes = @[(NSString *)kUTTypeImage];
  imagePickerController.delegate = self;
  imagePickerController.allowsEditing = YES;
  [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)editProfile:(UIButton *)sender {
  [self.view endEditing:YES];
  self.navigationController.navigationBar.userInteractionEnabled = NO;
  NSData *imageData;
  if (_photoChanged && _photoImageView.image) {
    imageData = UIImageJPEGRepresentation(_photoImageView.image, 1.0);
  }
  [_task cancel];
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  NSDictionary<NSString *, id> *postData = @{@"action":@"addvipmember",
                                             @"access_token":[Common getToken],
                                             @"lang":[Common getLang],
                                             @"edit":@(_profile != nil),
                                             @"name":_usernameTextField.text,
                                             @"email":_emailTextField.text,
                                             @"dob":[NSString stringWithFormat:@"%@-%@-%@", _dobYearTextField.text, _dobMonthTextField.text, _dobDayTextField.text],
                                             @"gender":_gender};
  WTWeakify(self);
  NSURLRequest *request = [Common requestWithParameters:postData dataDictionary:imageData ? @{@"user.jpg":imageData} : nil];
  _task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(VIPRegistrationViewController);
    dispatch_async(dispatch_get_main_queue(), ^{
      [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
      self.navigationController.navigationBar.userInteractionEnabled = YES;
    });
    JSON(data)
    if (json) {
      dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Watami" message:json[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:nil]];
        [strongSelf presentViewController:alertController animated:YES completion:nil];
      });
      WTProfile *profile = [[WTProfile alloc] initWithDictionary:json[@"data"] error:NULL];
      strongSelf->_profile = profile;
      [[NSNotificationCenter defaultCenter] postNotificationName:WTUserDidCreateOrUpdateProfileNotificationName object:profile];
    } else {
      [Common showRetryAlertInViewController:strongSelf handler:^{
        [strongSelf editProfile:sender];
      }];
    }
  }];
  [_task resume];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
  [picker dismissViewControllerAnimated:YES completion:nil];
  _photoImageView.image = info[UIImagePickerControllerEditedImage];
  _photoChanged = YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  if (textField.tag < 5) {
    UITextField *nextTextField = [_contentView viewWithTag:textField.tag + 1];
    [nextTextField becomeFirstResponder];
    return NO;
  }
  return YES;
}

@end
