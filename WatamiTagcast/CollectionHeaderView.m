//
//  CollectionHeaderView.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/18/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "CollectionHeaderView.h"

@implementation CollectionHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.font = [UIFont appFontOfSize:17.0];
    _titleLabel.textColor = [UIColor whiteColor];
    [self addSubview:_titleLabel];
    
    _detailLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _detailLabel.numberOfLines = 0;
    _detailLabel.font = [UIFont appFontOfSize:15.0];
    _detailLabel.textColor = [UIColor whiteColor];
    [self addSubview:_detailLabel];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  _titleLabel.frame = CGRectMake(15.0, 15.0, self.frame.size.width - 10.0, ceil(_titleLabel.font.lineHeight));
  CGFloat labelWidth = self.frame.size.width - 30.0;
  _detailLabel.frame = CGRectMake(15.0, CGRectGetMaxY(_titleLabel.frame) + 4.0, labelWidth, self.frame.size.height - 15.0 - 4.0 - CGRectGetMaxY(_titleLabel.frame));
}

+ (CGFloat)headHeightForWidth:(CGFloat)width text:(NSString *)text {
  CGFloat labelWidth = width - 30.0;
  CGFloat titleHeight = ceil([UIFont appFontOfSize:17.0].lineHeight);
  CGFloat detailHeight = ceil([text boundingRectWithSize:CGSizeMake(labelWidth, CGFLOAT_MAX)
                                                 options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:[UIFont appFontOfSize:15.0]}
                                                 context:NULL].size.height);
  return 15.0 + titleHeight + 4.0 + detailHeight + 15.0;
}

@end
