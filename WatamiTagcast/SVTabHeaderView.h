//
//  SVTabHeaderView.h
//  SVKit
//
//  Created by Sereivoan Yong on 7/31/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SVTabHeaderView;

@protocol SVTabHeaderViewDelegate <NSObject>

- (NSArray<UIView *> *)interactiveSubviewsInHeaderView:(SVTabHeaderView *)headerView;

@end

typedef NS_ENUM(NSUInteger, SVTabHeaderViewShrinkStyle) {
  SVTabHeaderViewShrinkStyleScrollUp = 0,
  SVTabHeaderViewShrinkStyleResize = 1,
} NS_SWIFT_NAME(SVTabHeaderView.ShrinkStyle);

@interface SVTabHeaderView : UIView

@property (nonatomic, weak, nullable) id<SVTabHeaderViewDelegate> delegate;
@property (nonatomic, assign) CGFloat defaultHeight;
@property (nonatomic, assign) CGFloat minimumHeight;

/// Set this to `YES` to add navigationBar's height to `minHeight`.
@property (nonatomic, assign) BOOL includesNavigationInMinHeight;

@property (nonatomic, assign) BOOL bounces;

@property (nonatomic, assign) SVTabHeaderViewShrinkStyle shrinkStyle;

- (instancetype)initWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
