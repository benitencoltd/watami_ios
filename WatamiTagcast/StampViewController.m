//
//  StampViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <Tagcast/TGCTagcastManager.h>
#import "StampViewController.h"
#import "StampsViewController.h"
#import "StampTicketsViewController.h"
#import "StampTabHeaderView.h"
#import "Constants.h"
#import "StampAddedAlertController.h"
#import "WTStampStatus.h"
#import "LanguagePickerController.h"
#import "RankingsViewController.h"

@interface StampViewController () <CBCentralManagerDelegate, TGCTagcastMangerDelegate>

@property (nonatomic, strong) UIActivityIndicatorView *loadingIndicatorView;
@property (nonatomic, strong, readonly) StampTabHeaderView *headerView;
@property (nonatomic, assign) BOOL generatable;

@end

@implementation StampViewController {
  CBCentralManager *_bluetoothManager;
  StampsViewController *_stampsViewController;
  StampTicketsViewController *_ticketsViewController;
  
  NSURLSessionDataTask *_statusTask, *_numberOfStampsTask, *_enabledTagcastsTask, *_checkInTask, *_generateTask;
  BOOL _checkInEnabled;
  NSArray<NSDictionary<NSString *, id> *> *_enabledTagcasts;
  BOOL _hasMatchedTagcasts;
  BOOL _shouldScanTagcasts;
  
  WTStampStatus *_status;
}

@dynamic headerView;

- (instancetype)init {
  _stampsViewController = [[StampsViewController alloc] init];
  _stampsViewController.title = NSLocalizedString(@"stamps", nil).uppercaseString;
  
  _ticketsViewController = [[StampTicketsViewController alloc] init];
  _ticketsViewController.title = NSLocalizedString(@"tickets", nil).uppercaseString;
  _ticketsViewController.actionNavigationItem = self.navigationItem;
  _ticketsViewController.loadsTagcastsOnViewDidLoad = NO;
  _ticketsViewController.shouldManageScanningTagcasts = NO;
  _ticketsViewController.showsStatusView = NO;
  self = [super initWithViewControllers:@[_stampsViewController, _ticketsViewController] headerViewClass:[StampTabHeaderView class]];
  if (self) {
    [self.headerView.rankButton addTarget:self action:@selector(showRankInfo:) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView.checkInButton addTarget:self action:@selector(checkIn:) forControlEvents:UIControlEventTouchUpInside];
    self.headerView.checkInButton.enabled = NO;
    [self.headerView.generateButton addTarget:self action:@selector(generate:) forControlEvents:UIControlEventTouchUpInside];
    self.headerView.generateButton.hidden = YES;
    self.tabBarHeight = 44.0;
    self.tabBar.backgroundColor = [UIColor whiteColor];
    self.tabBar.selectedColor = DARK_RED_COLOR;
    self.tabBar.tabAttributes = @{NSFontAttributeName:[UIFont appFontOfSize:16.0], NSForegroundColorAttributeName:[UIColor blackColor]};
    self.title = NSLocalizedString(@"stamp", nil);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(showLanguagePicker:)];
    _bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionShowPowerAlertKey:@NO}];
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

#pragma mark -

- (void)loadView {
  [super loadView];
  self.view.backgroundColor = DARK_RED_COLOR;
  self.containerView.backgroundColor = [UIColor clearColor];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [TGCTagcastManager provideAPIKey:kTagcastAPIKey];
//  [[TGCTagcastManager sharedManager] prepare];
  [self reloadNumberOfStamps];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [Common trackScreen:@"stamp_screen"];
  [TGCTagcastManager sharedManager].delegate = self;
//  if (_shouldScanTagcasts) {
    [[TGCTagcastManager sharedManager] startScan];
//  }
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [[TGCTagcastManager sharedManager] stopScan];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  _loadingIndicatorView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
}

#pragma mark -

- (BOOL)generatable {
  return self.headerView.generateButton.isHidden;
}

- (void)setGeneratable:(BOOL)generatable {
  self.headerView.generateButton.hidden = !generatable;
  [self.headerView setNeedsLayout];
}

- (void)setSubviewsHidden:(BOOL)hidden {
  self.containerView.hidden = hidden;
  self.headerView.hidden = hidden;
  self.tabBar.hidden = hidden;
}

- (void)showLanguagePicker:(UIBarButtonItem *)sender {
  LanguagePickerController *languagePickerController = [[LanguagePickerController alloc] init];
  [self.tabBarController presentViewController:[languagePickerController embedInNavigationController] animated:YES completion:nil];
}

#pragma mark -

- (void)reloadNumberOfStamps {
  [_numberOfStampsTask cancel];
  [self setSubviewsHidden:YES];
  [self.loadingIndicatorView startAnimating];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getstamp",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _numberOfStampsTask = [NSURLSession.sharedSession dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(StampViewController);
    JSON(data)
    if (json) {
      NSUInteger numberOfStamps = [json[@"data"][@"nstamps"] unsignedIntegerValue];
      //NSUInteger numberOfTickets = [json[@"data"][@"nticket"] unsignedIntegerValue];
      NSUInteger generatable = [json[@"data"][@"prize"] boolValue];
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf setGeneratable:generatable];
        strongSelf->_stampsViewController.numberOfStamps = numberOfStamps;
        [strongSelf reloadStatus];
      });
    } else {
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf.loadingIndicatorView stopAnimating];
        [strongSelf setSubviewsHidden:NO];
      });
    }
  }];
  [_numberOfStampsTask resume];
}

- (void)reloadStatus {
  [_statusTask cancel];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getallowaddstamp",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _statusTask = [NSURLSession.sharedSession dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(StampViewController);
    JSON(data)
    if (json) {
      WTStampStatus *status = [[WTStampStatus alloc] initWithDictionary:json[@"data"] error:NULL];
      strongSelf->_status = status;
      if (status.allowed) {
        [strongSelf reloadTagcasts];
      }
      dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableAttributedString *statusRank = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"rank", nil), status.memberType.uppercaseString]
                                                                                       attributes:@{NSFontAttributeName:strongSelf.headerView.rankTitleLabel.font, NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [statusRank addAttribute:NSForegroundColorAttributeName value:GOLD_COLOR range:NSMakeRange(statusRank.length - status.memberType.length, status.memberType.length)];
        strongSelf.headerView.rankTitleLabel.attributedText = statusRank;
      });
    }
    dispatch_async(dispatch_get_main_queue(), ^{
      [strongSelf.loadingIndicatorView stopAnimating];
      [strongSelf setSubviewsHidden:NO];
    });
  }];
  [_statusTask resume];
}

- (void)reloadTagcasts {
  _shouldScanTagcasts = YES;
  [[TGCTagcastManager sharedManager] startScan]; // Start scan to save user's time, stop if not available
  [_enabledTagcastsTask cancel];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"gettagcast",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _enabledTagcastsTask = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(StampViewController);
    JSON(data)
    if (json) {
      if ([json[@"result"] boolValue]) {
        strongSelf->_enabledTagcasts = json[@"data"][@"tagcasts"];
        strongSelf->_hasMatchedTagcasts = NO;
        strongSelf->_ticketsViewController.tagcasts = strongSelf->_enabledTagcasts;
      } else {
        dispatch_async(dispatch_get_main_queue(), ^{
          [Common showAlert:json[@"message"] title:nil _self:strongSelf];
        });
        strongSelf->_shouldScanTagcasts = NO;
        [[TGCTagcastManager sharedManager] stopScan];
      }
    } else {
      [strongSelf reloadTagcasts];
    }
  }];
  [_enabledTagcastsTask resume];
}

- (void)showRankInfo:(UIButton *)sender {
  RankingsViewController *rankingsViewController = [[RankingsViewController alloc] initWithRankings:_status.rankings];
  [self presentViewController:[rankingsViewController embedInNavigationController] animated:YES completion:nil];
}

- (void)checkIn:(UIButton *)sender {
  if (!_hasMatchedTagcasts) {
    [Common showAlert:NSLocalizedString(@"stamp_instruct", nil) title:nil _self:self];
    return;
  }
  [_checkInTask cancel];
  [self.headerView.checkInIndicatorView startAnimating];
  self.headerView.checkInButton.hidden = YES;
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"addstamp",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _checkInTask = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(StampViewController);
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.headerView.checkInIndicatorView stopAnimating];
      self.headerView.checkInButton.hidden = NO;
    });
    JSON(data)
    if (json) {
      dispatch_async(dispatch_get_main_queue(), ^{
        self.headerView.checkInButton.hidden = NO;
        [self.headerView.checkInIndicatorView stopAnimating];
        if ([json[@"result"] boolValue]) {
          if ([json[@"data"][@"status"] boolValue]) {
            strongSelf->_stampsViewController.numberOfStamps = [json[@"data"][@"nstamps"] unsignedIntegerValue];
            strongSelf.generatable = [json[@"data"][@"prize"] boolValue];
            
            StampAddedAlertController *alertController = [[StampAddedAlertController alloc] init];
            [strongSelf.tabBarController presentViewController:alertController animated:YES completion:nil];
            
            [Common trackEvent:@"checkin_stamp" action:@"checkin" label:@"checkin"];
          } else {
            [Common showAlert:json[@"data"][@"message"] title:nil _self:self];
          }
        }
      });
    } else {
      dispatch_async(dispatch_get_main_queue(), ^{
        [Common showRetryAlertInViewController:strongSelf handler:^{
          [strongSelf checkIn:sender];
        }];
      });
    }
  }];
  [_checkInTask resume];
}

- (IBAction)generate:(UIButton *)sender {
  if (!_hasMatchedTagcasts) {
    [Common showAlert:NSLocalizedString(@"stamp_instruct", nil) title:nil _self:self];
    return;
  }
  [_generateTask cancel];
  [self.headerView.generateIndicatorView startAnimating];
  self.headerView.generateButton.hidden = YES;
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"generateticket",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _generateTask = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(StampViewController);
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.headerView.generateIndicatorView stopAnimating];
      self.headerView.generateButton.hidden = NO;
    });
    JSON(data)
    if (json) {
      if ([json[@"result"] boolValue]) {
        dispatch_async(dispatch_get_main_queue(), ^{
          UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"WATAMI" message:json[@"data"][@"message"] preferredStyle:UIAlertControllerStyleAlert];
          [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:nil]];
          [strongSelf presentViewController:alertController animated:YES completion:nil];
          
          if ([json[@"data"][@"status"] boolValue]) {
            strongSelf->_stampsViewController.numberOfStamps = [json[@"data"][@"nstamps"] unsignedIntegerValue];
            strongSelf.generatable = [json[@"data"][@"prize"] boolValue];
            NSArray<WTStampTicket *> *tickets = [WTStampTicket arrayOfModelsFromDictionaries:[json objectForKey:@"data"][@"tickets"] error:NULL];
            [strongSelf->_ticketsViewController reloadDataFromTickets:tickets];
            
            [Common trackEvent:@"generate_stamp_ticket" action:@"generate_ticket" label:@"generate_ticket"];
          }
        });
      }
    } else {
      dispatch_async(dispatch_get_main_queue(), ^{
        [Common showRetryAlertInViewController:strongSelf handler:^{
          [strongSelf generate:sender];
        }];
      });
    }
  }];
  [_generateTask resume];
}

- (UIActivityIndicatorView *)loadingIndicatorView {
  if (!_loadingIndicatorView) {
    _loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _loadingIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_loadingIndicatorView];
  }
  return _loadingIndicatorView;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
  if (central.state != CBCentralManagerStatePoweredOn) {
    _hasMatchedTagcasts = NO;
    self.headerView.checkInButton.enabled = NO;
  }
}

#pragma mark - TGCTagcastMangerDelegate

- (void)tagcastManager:(TGCTagcastManager *)manager didDiscoveredTagcast:(NSDictionary<NSString *, id> *)tagcast {
  NSString *serialIdentifier = tagcast[kTGCKeyData][kTGCKeySerialIdentifier];
  for (NSDictionary<NSString *, id> *enabledTagcast in _enabledTagcasts) {
    if ([enabledTagcast[@"enable_stamp"] boolValue] && [enabledTagcast[@"serial_number"] isEqualToString:serialIdentifier]) {
      _hasMatchedTagcasts = YES;
      self.headerView.checkInButton.enabled = YES;
      break;
    }
  }
}

- (void)tagcastManager:(TGCTagcastManager *)manager didScannedTagcasts:(NSArray<NSDictionary<NSString *, id> *> *)tagcasts {
  if (tagcasts.count == 0) {
    _hasMatchedTagcasts = NO;
    self.headerView.checkInButton.enabled = NO;
  }
}

@end
