//
//  DAKeyboardControl.h
//  DAKeyboardControlExample
//
//  Created by Daniel Amitay on 7/14/12.
//  Copyright (c) 2012 Daniel Amitay. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SVKeyboardState) {
  SVKeyboardStatePresenting,
  SVKeyboardStateDismissing,
  SVKeyboardStateDragging,
  SVKeyboardStateRotating,
};

typedef void (^SVKeyboardFrameChangeHandler)(CGRect keyboardFrame, SVKeyboardState state);

/** DAKeyboardControl allows you to easily add keyboard awareness and scrolling
 dismissal (a receding keyboard ala iMessages app) to any UIView, UIScrollView
 or UITableView with only 1 line of code. DAKeyboardControl automatically
 extends UIView and provides a block callback with the keyboard's current origin.
 */

@interface UIView (DAKeyboardControl)

@property (nonatomic, copy, readonly, nullable) SVKeyboardFrameChangeHandler keyboardFrameHandler;
@property (nonatomic, copy, readonly, nullable) SVKeyboardFrameChangeHandler keyboardConstraintHandler;

/** The keyboardTriggerOffset property allows you to choose at what point the
 user's finger "engages" the keyboard.
 */
@property (nonatomic, assign) CGFloat keyboardTriggerOffset;
@property (nonatomic, assign, readonly) BOOL keyboardWillRecede;

/** Adding pan-to-dismiss (functionality introduced in iMessages)
 @param frameHandler called everytime the keyboard is moved so you can update
 the frames of your views
 @see addKeyboardNonpanningWithActionHandler:
 @see removeKeyboardControl
 */
- (void)addKeyboardPanningWithFrameHandler:(nullable SVKeyboardFrameChangeHandler)frameHandler;
- (void)addKeyboardPanningWithFrameHandler:(nullable SVKeyboardFrameChangeHandler)frameHandler
                         constraintHandler:(nullable SVKeyboardFrameChangeHandler)constraintHandler;

/** Adding keyboard awareness (appearance and disappearance only)
 @param frameHandler called everytime the keyboard is moved so you can update
 the frames of your views
 @see addKeyboardPanningWithActionHandler:
 @see removeKeyboardControl
 */
- (void)addKeyboardNonpanningWithFrameHandler:(SVKeyboardFrameChangeHandler)frameHandler;
- (void)addKeyboardNonpanningWithFrameHandler:(SVKeyboardFrameChangeHandler)frameHandler
                            constraintHandler:(SVKeyboardFrameChangeHandler)constraintHandler;

/** Remove the keyboard action handler
 @note You MUST call this method to remove the keyboard handler before the view
 goes out of memory.
 */
- (void)removeKeyboardControl;

/** Returns the keyboard frame in the view */
@property (nonatomic, assign, readonly) CGRect keyboardFrameInView;
@property (nonatomic, assign, readonly, getter=isKeyboardPresenting) BOOL keyboardPresenting;

/** Convenience method to dismiss the keyboard */
- (void)hideKeyboard;

@end

NS_ASSUME_NONNULL_END
