//
//  WTStampStatus.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTStampStatus.h"

@implementation WTStampRanking

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"memberType":@"type",
                                                                @"numberOfTicketsRequired":@"rank"}];
}

@end

@implementation WTStampStatus

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"memberType":@"type",
                                                                @"rankings":@"rank_conf",
                                                                @"allowed":@"status"}];
}

- (void)setRankingsWithNSArray:(NSArray<NSDictionary<NSString *, id> *> *)array {
  _rankings = [WTStampRanking arrayOfModelsFromDictionaries:array error:NULL];
}

@end
