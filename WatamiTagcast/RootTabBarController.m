//
//  RootTabBarController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <OneSignal/OneSignal.h>
#import "RootTabBarController.h"
#import "RegisterViewController.h"
#import "LanguagePickerController.h"
#import "HomeViewController.h"
#import "Constants.h"
#import "LanguageManager.h"

#import "WTVersionCheck.h"
#import "WTStampTicket.h"

static BOOL alreadyCheckForUpdate;
static BOOL alreadyScaled;

@interface RootTabBarController ()

@end

@implementation RootTabBarController

- (void)dealloc {
  WTDeallocLog(self);
}

#pragma mark -

- (void)viewDidLoad {
  [super viewDidLoad];
  if (!alreadyScaled) {
    alreadyScaled = YES;
    [self scaleLaunchScreen];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  dispatch_async(dispatch_get_main_queue(), ^{
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] stringForKey:kUserAccessTokenKey];
    if (!accessToken || accessToken.length == 0) {
      RegisterViewController *registerViewController = [[RegisterViewController alloc] init];
      registerViewController.successHandler = ^{
        [OneSignal registerForPushNotifications];
      };
      [self presentViewController:registerViewController animated:YES completion:nil];
      
    } else if (![[NSUserDefaults standardUserDefaults] stringForKey:kUserLanguageKey]) {
      LanguagePickerController *languagePickerController = [[LanguagePickerController alloc] init];
      [self presentViewController:[languagePickerController embedInNavigationController] animated:YES completion:nil];
      
    } else {
      [self checkVersion];
    }
  });
}

#pragma mark -

- (void)scaleLaunchScreen {
  UIView *containerView = [[UIView alloc] initWithFrame:self.view.bounds];
  [self.view addSubview:containerView];
  
  UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
  backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
  backgroundImageView.image = [UIImage imageNamed:@"background_splash.jpg"];
  [containerView addSubview:backgroundImageView];
  
  UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20.0, 30.0, 132.0, 170.0)];
  logoImageView.contentMode = UIViewContentModeScaleAspectFit;
  logoImageView.image = [UIImage imageNamed:@"s_icon.png"];
  [containerView addSubview:logoImageView];
  
  [UIView animateWithDuration:1.0 animations:^{
    backgroundImageView.transform = CGAffineTransformMakeScale(1.2, 1.2);
  } completion:^(BOOL finished) {
    [UIView animateWithDuration:1.0 animations:^{
      containerView.alpha = 0.0;
    } completion:^(BOOL finished) {
      [containerView removeFromSuperview];
    }];
  }];
}

- (void)alertUpdateRecursivelyWithTitle:(NSString *)title message:(NSString *)message forced:(BOOL)forced {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
  [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"update", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    NSString *appURLString = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", kWatamiAppId];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appURLString]];
    if (forced) {
      [self alertUpdateRecursivelyWithTitle:title message:message forced:forced];
    }
  }]];
  if (!forced) {
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"later", nil) style:UIAlertActionStyleCancel handler:nil]];
  }
  [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -

- (void)checkVersion {
  if (alreadyCheckForUpdate) {
    return;
  }
  WTLog(@"Checking version...");
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"checkversion",
                                             @"lang":[Common getLang],
                                             @"current_version":[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
                                             @"type":@"ios"};
  WTWeakify(self);
  [[[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(RootTabBarController);
    JSON(data)
    if (json) {
      alreadyCheckForUpdate = YES;
      WTVersionCheck *versionCheck = [[WTVersionCheck alloc] initWithDictionary:json[@"data"] error:NULL];
      if (versionCheck) {
        if ([json[@"result"] boolValue]) { // new version available
          dispatch_async(dispatch_get_main_queue(), ^{
            [strongSelf alertUpdateRecursivelyWithTitle:json[@"message"] message:json[@"data"][@"message"] forced:versionCheck.forced];
          });
        } else if (json[@"data"][@"incentive"] && !versionCheck.isIncentiveClaimed) {
          [strongSelf claimIncentiveStampTicket];
        }
      }
      WTLog(@"Done checking version (%@)", json[@"message"]);
    }
  }] resume];
}

- (void)claimIncentiveStampTicket {
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"addincentive",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  [[[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(RootTabBarController);
    JSON(data)
    if (json) {
      WTStampTicket *ticket = [WTStampTicket arrayOfModelsFromDictionaries:json[@"data"] error:NULL].firstObject;
      if (ticket) {
        dispatch_async(dispatch_get_main_queue(), ^{
          UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"WATAMI" message:ticket.text preferredStyle:UIAlertControllerStyleAlert];
          [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            UINavigationController *selectedViewConroller = (UINavigationController *)strongSelf.selectedViewController;
            [(HomeViewController *)selectedViewConroller.viewControllers.firstObject reloadVIPInfo];
          }]];
          [strongSelf presentViewController:alertController animated:YES completion:nil];
        });
      }
    }
  }] resume];
}

- (UITraitCollection *)traitCollection { // To make all tabBarItems fits.
  UITraitCollection *traitCollection = [super traitCollection];
  UITraitCollection *fakeTraitCollection = [UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassRegular];
  return [UITraitCollection traitCollectionWithTraitsFromCollections:@[traitCollection, fakeTraitCollection]];
}

@end
