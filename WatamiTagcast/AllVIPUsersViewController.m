//
//  AllVIPUsersViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/30/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <CarbonKit/CarbonKit.h>

#import "AllVIPUsersViewController.h"
#import "VIPUsersViewController.h"

@interface AllVIPUsersViewController () <CarbonTabSwipeNavigationDelegate>

@end

@implementation AllVIPUsersViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = NSLocalizedString(@"ranking", nil);
  [self.navigationController.navigationBar removeBackBarButtonItemTitle];
  
  CarbonTabSwipeNavigation *tabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:@[NSLocalizedString(@"total_rank", nil), NSLocalizedString(@"monthly_rank", nil)] delegate:self];
  tabSwipeNavigation.pagesScrollView.backgroundColor = [UIColor whiteColor];
  CGFloat itemWidth = [UIScreen mainScreen].bounds.size.width / 2;
  [tabSwipeNavigation.carbonSegmentedControl setWidth:itemWidth forSegmentAtIndex:0];
  [tabSwipeNavigation.carbonSegmentedControl setWidth:itemWidth forSegmentAtIndex:1];
  [tabSwipeNavigation setIndicatorColor:[UIColor whiteColor]];
  [tabSwipeNavigation setNormalColor:[UIColor whiteColor] font:[UIFont appFontOfSize:15.0]];
  [tabSwipeNavigation setSelectedColor:[UIColor whiteColor] font:[UIFont appFontOfSize:15.0]];
  [tabSwipeNavigation insertIntoRootViewController:self];
}

#pragma mark - CarbonTabSwipeNavigationDelegate

- (UIViewController *)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbonTabSwipeNavigation viewControllerAtIndex:(NSUInteger)index {
  if (index == 0) {
    return [[VIPUsersViewController alloc] init];
  } else {
    VIPUsersViewController *rankUsersViewController = [[VIPUsersViewController alloc] init];;
    rankUsersViewController.isMonthly = YES;
    return rankUsersViewController;
  }
}

@end
