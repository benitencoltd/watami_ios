//
//  UIFont+App.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/10/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "UIFont+App.h"
#import "LanguageManager.h"
#import "Constants.h"

@implementation UIFont (App)

- (UIFont *)appFont {
  return [UIFont appFontOfSize:self.pointSize];
}

- (UIFont *)boldAppFont {
  return [UIFont boldAppFontOfSize:self.pointSize];
}

+ (UIFont *)appFontOfSize:(CGFloat)fontSize {
  NSString *language = [NSUserDefaults.standardUserDefaults stringForKey:kUserLanguageKey];
  if ([language isEqualToString:kLanguageKM]) {
    return [UIFont fontWithName:@"KhmerOSContent" size:fontSize - 1];
  } else {
    return [UIFont systemFontOfSize:fontSize];
  }
}

+ (UIFont *)boldAppFontOfSize:(CGFloat)fontSize {
  NSString *language = [NSUserDefaults.standardUserDefaults stringForKey:kUserLanguageKey];
  if ([language isEqualToString:kLanguageKM]) {
    return [UIFont fontWithName:@"KhmerOSContent-Bold" size:fontSize - 1];
  } else {
    return [UIFont boldSystemFontOfSize:fontSize];
  }
}

@end
