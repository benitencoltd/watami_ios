//
//  AppDelegate.m
//  WatamiTagcast
//
//  Created by Choung Chamnab on 12/31/15.
//  Copyright © 2015 BENITEN CO., LTD. All rights reserved.
//

#import <OneSignal/OneSignal.h>
#import <GoogleAnalytics/GAI.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MBProgressHUD/MBProgressHUD.h>

#import "AppDelegate.h"
#import "Constants.h"
#import "LanguageManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [self setupGoogleServices];
  [self setupOneSignalWithLaunchOption:launchOptions];
  [self reloadAppearances];
  [self sendOneSignalLanguageTagIfNeeded];
  return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(id)annotation {
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Public

- (void)reloadAppearances {
  [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
  [UINavigationBar appearance].shadowImage = [[UIImage alloc] init];
  [UINavigationBar appearance].translucent = NO;
  [UINavigationBar appearance].tintColor = [UIColor whiteColor];
  [UINavigationBar appearance].barTintColor = [UIColor blackColor];
  [UINavigationBar appearance].titleTextAttributes = @{NSFontAttributeName:[UIFont boldAppFontOfSize:18.0], NSForegroundColorAttributeName:[UIColor whiteColor]};
  
  [[UIToolbar appearance] setBackgroundImage:[[UIImage alloc] init] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
  [[UIToolbar appearance] setShadowImage:[[UIImage alloc] init] forToolbarPosition:UIToolbarPositionAny];
  [UIToolbar appearance].translucent = NO;
  [UIToolbar appearance].tintColor = [UIColor whiteColor];
  [UIToolbar appearance].barTintColor = [UIColor blackColor];
  
  [UITabBar appearance].backgroundImage = [[UIImage alloc] init];
  [UITabBar appearance].shadowImage = [[UIImage alloc] init];
  [UITabBar appearance].translucent = NO;
  [UITabBar appearance].tintColor = [UIColor whiteColor];
  [UITabBar appearance].barTintColor = [UIColor colorWithWhite:65.0/255.0 alpha:1.0];
  
  [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont appFontOfSize:17.0], NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
  
  NSDictionary<NSString *, id> *attributes = @{NSFontAttributeName:[UIFont appFontOfSize:11.0], NSForegroundColorAttributeName:[UIColor whiteColor]};
  [[UITabBarItem appearance] setTitleTextAttributes:attributes forState:UIControlStateNormal];
  [[UITabBarItem appearance] setTitleTextAttributes:attributes forState:UIControlStateSelected];
  if ([[[NSUserDefaults standardUserDefaults] stringForKey:kUserLanguageKey] isEqualToString:kLanguageKM]) {
    [UITabBarItem appearance].titlePositionAdjustment = UIOffsetMake(0.0, -1.0);
  } else {
    [UITabBarItem appearance].titlePositionAdjustment = UIOffsetMake(0.0, -4.0);
  }
  
  [MBProgressHUD appearance].margin = 30.0;
}

- (void)sendOneSignalLanguageTagIfNeeded {
  NSString *languageValue = [[NSUserDefaults standardUserDefaults] objectForKey:kOneSignalLanguageValueToSendKey];
  if (languageValue) {
    [OneSignal sendTag:@"language" value:languageValue onSuccess:^(NSDictionary *result) {
      [[NSUserDefaults standardUserDefaults] removeObjectForKey:kOneSignalLanguageValueToSendKey];
      [[NSUserDefaults standardUserDefaults] synchronize];
    } onFailure:nil];
  }
}

#pragma mark - Private

- (void)setupGoogleServices {
  // Analytics
  GAI *gai = [GAI sharedInstance];
  gai.trackUncaughtExceptions = YES;
  // Maps
  [GMSServices provideAPIKey:kGMSServicesAPIKey];
}

- (void)setupOneSignalWithLaunchOption:(NSDictionary *)launchOptions {
  WTWeakify(self);
  OSHandleNotificationReceivedBlock receivedHandler = ^(OSNotification *notification) {
    WTStrongify(AppDelegate);
    NSDictionary<NSString *, id> *data = notification.payload.additionalData[@"data"];
    NSInteger type = [data[@"type"] integerValue];
    dispatch_async(dispatch_get_main_queue(), ^{
      UITabBarController *tabBarController = (UITabBarController *)strongSelf.window.rootViewController;
      if (type == 1) {
        tabBarController.selectedIndex = 3;
      } else if (type == 2) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:data[@"title"] message:data[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:nil]];
        [tabBarController presentViewController:alertController animated:YES completion:nil];
      }
    });
  };
  
  OSHandleNotificationActionBlock actionHandler = ^(OSNotificationOpenedResult *result) {
    receivedHandler(result.notification);
  };
  
  [OneSignal initWithLaunchOptions:launchOptions
                             appId:kOneSignalAppId
        handleNotificationReceived:receivedHandler
          handleNotificationAction:actionHandler
                          settings:@{kOSSettingsKeyAutoPrompt:@NO, kOSSettingsKeyInAppAlerts:@NO}];
}

@end
