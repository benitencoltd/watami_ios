//
//  StampTicketsViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/9/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "StampTicketsViewController.h"
#import "TicketsClaimAlertController.h"

@interface StampTicketsViewController ()

@end

@implementation StampTicketsViewController

- (void)dealloc {
  WTDeallocLog(self);
}

- (instancetype)init {
  self = [super initWithObjectClass:[WTStampTicket class] tagcasts:nil];
  if (self) {
    self.key = @"data.tickets";
  }
  return self;
}

- (NSURLRequest *)request {
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getstamptickets",
                                             @"lang":[Common getLang],
                                             @"parameter":@{
                                                 @"incentive":@(_includesIncentiveTickets)}};
  return [Common convertToRequest:postData];
}

- (void)claim {
  NSArray<NSIndexPath *> *indexPathsForSelectedItems = [self sortedIndexPathsForSelectedItems];
  NSArray<WTStampTicket *> *selectedTickets = [indexPathsForSelectedItems objectsByTransform:^WTStampTicket *(NSIndexPath *indexPath) {
    return self.objects[indexPath.item];
  }];
  
  TicketsClaimAlertController *claimAlertController = [[TicketsClaimAlertController alloc] init];
  [claimAlertController view];
  [claimAlertController.imageView sd_setImageWithURL:selectedTickets.firstObject.imageURL];
  if (selectedTickets.count > 1) {
    claimAlertController.expiryDateLabel.hidden = YES;
  } else {
    claimAlertController.expiryDateLabel.text = [NSString stringWithFormat:@"%@ : %@", NSLocalizedString(@"exp", nil), WTStringFromDate(selectedTickets.firstObject.expiryDate)];
  }
  claimAlertController.claimHandler = ^(TicketsClaimAlertController *controller) {
    NSAssert(indexPathsForSelectedItems.count > 0, @"Number of selected items must be greater than 0");
    // get joined ids as string descending. `7,3,1`
    NSString *stringIds = [[indexPathsForSelectedItems objectsByTransform:^NSString *(NSIndexPath *indexPath) {
      return [NSString stringWithFormat:@"%u", (unsigned int)self.objects[indexPath.item].id];
    }] componentsJoinedByString:@","];
    
    NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                               @"action":@"claimticket",
                                               @"lang":[Common getLang],
                                               @"parameter":@{
                                                   @"tickets_id":stringIds}};
    WTWeakify(self);
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
      WTStrongify(StampTicketsViewController);
      dispatch_async(dispatch_get_main_queue(), ^{
        [controller dismissViewControllerAnimated:YES completion:nil];
      });
      JSON(data)
      if (json) {
        void (^okActionHandler)(UIAlertAction *action);
        if ([json[@"data"][@"status"] boolValue]) {
          [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidClaimStampTicketsNotification object:selectedTickets];
          okActionHandler = ^(UIAlertAction *action) {
            for (NSIndexPath *indexPath in indexPathsForSelectedItems) {
              [strongSelf.objects removeObjectAtIndex:indexPath.item];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
              [strongSelf->_collectionView deleteItemsAtIndexPaths:indexPathsForSelectedItems];
              [strongSelf updateSelections];
            });
          };
        }
        dispatch_async(dispatch_get_main_queue(), ^{
          UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"WATAMI" message:json[@"data"][@"message"] preferredStyle:UIAlertControllerStyleAlert];
          [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:okActionHandler]];
          [strongSelf presentViewController:alertController animated:YES completion:nil];
        });
        [Common trackEvent:@"claim_stamp_ticket" action:@"claim_ticket" label:@"claim_ticket"];
      } else {
        
      }
    }];
    [task resume];
  };
  [self.tabBarController presentViewController:claimAlertController animated:YES completion:nil];
}

- (void)reloadDataFromTickets:(NSArray<WTStampTicket *> *)tickets {
  [self.objects removeAllObjects];
  [self.objects addObjectsFromArray:tickets];
  [_collectionView reloadData];
}

- (BOOL)isTagcastEnabledForCurrentFeature:(NSDictionary<NSString *, id> *)tagcast {
  return [tagcast[@"enable_stamp"] boolValue];
}

- (void)requestAppLinkWithURLString:(NSString *)URLString completion:(void (^)(NSString *appLinkId))completion {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getapplink",
                                             @"lang":[Common getLang],
                                             @"url":URLString};
  WTWeakify(self);
  [[[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(StampTicketsViewController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
      NSString *appLinkId = json[@"data"][@"app_link_id"];
      if (appLinkId.length > 0) {
        completion(appLinkId);
      }
    });
  }] resume];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  TicketCollectionViewCell *cell = [super collectionView:collectionView cellForItemAtIndexPath:indexPath];
  WTStampTicket *ticket = self.objects[indexPath.item];
  cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
  [cell.imageView sd_setImageWithURL:ticket.imageURL];
  cell.imageTextLabel.text = ticket.text;
  cell.expiryDateLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"exp", nil), WTStringFromDate(ticket.expiryDate)];
  WTWeakify(self);
  cell.shareHandler = ^(UIButton *sender) {
    WTStrongify(StampTicketsViewController);
    NSString *appLinkURLString = [NSString stringWithFormat:@"com.beniten.tagcast.watami://detail?type=0&desc=%@&title=%@&id=%u", ticket.text, ticket.text, (unsigned)ticket.id];
    [strongSelf requestAppLinkWithURLString:appLinkURLString completion:^(NSString *appLinkId) {
      WTStrongify(StampTicketsViewController);
      FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
      content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://fb.me/%@", appLinkId]];
      content.contentTitle = kWatamiTitle;
      content.contentDescription = ticket.text;
      content.imageURL = ticket.imageURL;
      
      FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
      dialog.fromViewController = strongSelf;
      dialog.mode = FBSDKShareDialogModeNative;
      dialog.shareContent = content;
      [dialog show];
    }];
  };
  return cell;
}

- (UIScrollView *)stretchableSubViewInSubViewController:(UIViewController *)subViewController {
  return _collectionView;
}

@end
