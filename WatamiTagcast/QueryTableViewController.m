//
//  QueryTableViewController.m
//  Khemara
//
//  Created by Beniten on 11/18/16.
//  Copyright © 2016 Beniten. All rights reserved.
//

#import "QueryTableViewController.h"

static NSString * const kQueryCellIdentifier = @"QueryCell";

@implementation QueryTableViewController
//{
//  CGFloat _rowHeight;
//  NSMutableDictionary<NSIndexPath *, NSNumber *> *_indexPathRowHeight;
//}

- (instancetype)initWithObjectClass:(Class)objectClass {
  return [self initWithStyle:UITableViewStylePlain objectClass:objectClass];
}

- (instancetype)initWithStyle:(UITableViewStyle)style objectClass:(Class)objectClass {
  return [self initWithStyle:style objectClass:objectClass cellClass:nil];
}

- (instancetype)initWithStyle:(UITableViewStyle)style objectClass:(Class)objectClass cellClass:(Class)cellClass {
  self = [super initWithObjectClass:objectClass];
  if (self) {
    _style = style;
//    _rowHeight = 44.0;
    _cellClass = cellClass;
  }
  return self;
}

- (void)loadView {
  [super loadView];
  _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:_style];
  _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  _tableView.backgroundColor = [UIColor whiteColor];
  _tableView.dataSource = self;
  _tableView.delegate = self;
  _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  if (_cellClass) {
    [_tableView registerClass:_cellClass forCellReuseIdentifier:kQueryCellIdentifier];
//    if ([(Class)_cellClass respondsToSelector:@selector(height)]) {
//      _rowHeight = [_cellClass height];
//    }
//    if ([(Class)_cellClass respondsToSelector:@selector(heightForObject:)]) {
//      _rowHeight = 0.0;
//      _indexPathRowHeight = [[NSMutableDictionary alloc] init];
//    }
  }
  [self.view addSubview:_tableView];
  
  self.scrollView = _tableView;
}


- (void)reloadDataWithObjects:(NSArray *)objects reset:(BOOL)reset {
//  if (_indexPathRowHeight) {
//    [_indexPathRowHeight removeAllObjects];
//    for (int i = 0; i < self.objects.count; i++) {
//      _indexPathRowHeight[[NSIndexPath indexPathForRow:i inSection:0]] = @([_cellClass heightForObject:(self.objects[i])]);
//    }
//    dispatch_async(dispatch_get_main_queue(), ^{
//      [_tableView reloadData];
//    });
//  } else {
    [_tableView reloadData];
//  }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (_cellClass) {
    return [tableView dequeueReusableCellWithIdentifier:kQueryCellIdentifier forIndexPath:indexPath];
  }
  return nil;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//  if (_rowHeight > 0.0) {
//    return _rowHeight;
//  } else {
//    return _indexPathRowHeight[indexPath].doubleValue;
//  }
//}

#pragma mark - UICollectionViewDelegate

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//  [self tableView:tableView willDisplayCell:cell withObject:self.objects[indexPath.item]];
//}
//
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell withObject:(__kindof JSONModel *)object {
//  if (_cellClass) {
//    [(UITableViewCell<QueryViewCell> *)cell setObject:object];
//  } else {
//    assert(@"Must override");
//  }
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//  [tableView deselectRowAtIndexPath:indexPath animated:YES];
//  [self tableView:tableView didSelectRowWithObject:self.objects[indexPath.row]];
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowWithObject:(__kindof JSONModel *)object {
//  
//}

@end
