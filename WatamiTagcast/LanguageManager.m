//
//  LanguageManager.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/10/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <OneSignal/OneSignal.h>
#import "LanguageManager.h"
#import "Constants.h"
#import "BundleLocalization.h"

@implementation LanguageController

+ (NSString *)selectedLanguage {
  return [[NSUserDefaults standardUserDefaults] stringForKey:kUserLanguageKey];
}

+ (void)setSelectedLanguage:(NSString *)selectedLanguage {
  [BundleLocalization sharedInstance].language = selectedLanguage;
  [[NSUserDefaults standardUserDefaults] setObject:selectedLanguage forKey:kUserLanguageKey];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)preferredLanguage {
  return [NSBundle mainBundle].preferredLocalizations.firstObject ?: kLanguageEN;
}

+ (void)sendOneSignalLanguageTag {
  NSString *language = [self selectedLanguage];
  NSString *languageValue;
  if ([language isEqualToString:kLanguageKM]) {
    languageValue = @"kh";
  } else if ([language isEqualToString:kLanguageJA]) {
    languageValue = @"jp";
  } else {
    languageValue = @"en";
  }
  
  [OneSignal sendTag:@"language" value:languageValue onSuccess:^(NSDictionary *result) {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kOneSignalLanguageValueToSendKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
  } onFailure:^(NSError *error) {
    [[NSUserDefaults standardUserDefaults] setObject:languageValue forKey:kOneSignalLanguageValueToSendKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }];
}

@end
