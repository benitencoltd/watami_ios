//
//  FBShareButton.m
//  Khemara
//
//  Created by Beniten on 2/8/17.
//  Copyright © 2017 Beniten. All rights reserved.
//

#import "FBShareButton.h"

#define SIZE CGSizeMake(140.0, 36.0)

@implementation FBShareButton

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self _commonInit];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self _commonInit];
  }
  return self;
}

- (void)_commonInit {
  self.backgroundColor = [UIColor whiteColor];
  self.layer.cornerRadius = 18.0;
  self.tintColor = [UIColor blackColor];
  self.imageEdgeInsets = UIEdgeInsetsMake(-2.0, 0.0, 0.0, 6.0);
  self.titleEdgeInsets = UIEdgeInsetsMake(0.0, 6.0, 0.0, 6.0);
  [self setImage:[[[UIImage imageNamed:@"facebook.png"] imageByResizingToSize:CGSizeMake(26.0, 26.0)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
          forState:UIControlStateNormal];
  if (self.buttonType != UIButtonTypeSystem) {
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  }
  [self setTitle:NSLocalizedString(@"share", nil) forState:UIControlStateNormal];
  [self sizeToFit];
  AddShadowToViewWithPath(self, [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.bounds.size.height / 2.0].CGPath);
}

+ (instancetype)button {
  return [FBShareButton buttonWithType:UIButtonTypeSystem];
}

- (CGSize)intrinsicContentSize {
  return SIZE;
}

- (void)sizeToFit {
  CGRect frame = self.frame;
  frame.size = SIZE;
  self.frame = frame;
}

@end
