//
//  WTStampTicket.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTStampTicket.h"
#import "NSDateFormatter+PrivateResponse.h"

@implementation WTStampTicket

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"id":@"ticket_id",
                                                                @"text":@"description",
                                                                @"imageURL":@"image",
                                                                @"date":@"ticket_date",
                                                                @"expiryDate":@"expiry_date"}];
}

- (void)setImageURLWithNSString:(NSString *)string {
  _imageURL = [NSURL URLWithString:string];
}

- (void)setDateWithNSString:(NSString *)string {
  _date = [[NSDateFormatter responseSharedInstance] dateFromString:[string componentsSeparatedByString:@" "].firstObject];
}

- (void)setExpiryDateWithNSString:(NSString *)string {
  _expiryDate = [[NSDateFormatter responseSharedInstance] dateFromString:[string componentsSeparatedByString:@" "].firstObject];
}

@end
