//
//  VIPUserTableViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/28/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "VIPUserTableViewCell.h"

#define NUMBER_LABEL_WIDTH 32.0
#define PHOTO_WIDTH 80.0

@implementation VIPUserTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    self.backgroundColor = [UIColor whiteColor];
    
    _numberLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _numberLabel.backgroundColor = DARK_RED_COLOR;
    _numberLabel.clipsToBounds = YES;
    _numberLabel.font = [UIFont appFontOfSize:16.0];
    _numberLabel.textAlignment = NSTextAlignmentCenter;
    _numberLabel.textColor = [UIColor whiteColor];
    _numberLabel.layer.cornerRadius = NUMBER_LABEL_WIDTH / 2;
    [self.contentView addSubview:_numberLabel];
    
    _photoImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _photoImageView.clipsToBounds = YES;
    _photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    _photoImageView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    _photoImageView.layer.borderWidth = 0.5;
    _photoImageView.layer.cornerRadius = PHOTO_WIDTH / 2;
    _photoImageView.tintColor = [UIColor whiteColor];
    [self.contentView addSubview:_photoImageView];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _nameLabel.font = [UIFont appFontOfSize:17.0];
    [self.contentView addSubview:_nameLabel];
    
    _memberTypeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _memberTypeLabel.font = [UIFont appFontOfSize:13.0];
    [self.contentView addSubview:_memberTypeLabel];
    
    _stampCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _stampCountLabel.font = [UIFont appFontOfSize:16.0];
    _stampCountLabel.textAlignment = NSTextAlignmentCenter;
    _stampCountLabel.textColor = DARK_RED_COLOR;
    [self.contentView addSubview:_stampCountLabel];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  _numberLabel.frame = CGRectMake(15.0, (self.contentView.frame.size.height - NUMBER_LABEL_WIDTH) / 2.0, NUMBER_LABEL_WIDTH, NUMBER_LABEL_WIDTH);
  
  _photoImageView.frame = CGRectMake(CGRectGetMaxX(_numberLabel.frame) + 8.0, (self.contentView.frame.size.height - PHOTO_WIDTH) / 2, PHOTO_WIDTH, PHOTO_WIDTH);
  
  CGFloat nameHeight = ceil(_nameLabel.font.lineHeight);
  CGFloat memberTypeHeight = ceil(_memberTypeLabel.font.lineHeight);
  
  CGSize stampCountSize = [_stampCountLabel sizeThatFits:CGSIZE_MAX];
  _stampCountLabel.frame = CGRectMake(self.contentView.frame.size.width - stampCountSize.width - 12, (self.contentView.frame.size.height - stampCountSize.height) / 2, stampCountSize.width, stampCountSize.height);
  
  _nameLabel.frame = CGRectMake(CGRectGetMaxX(_photoImageView.frame) + 12.0, (self.contentView.frame.size.height - nameHeight - 4.0 - memberTypeHeight) / 2, CGRectGetMinX(_stampCountLabel.frame) - 24.0 - CGRectGetMaxX(_photoImageView.frame), nameHeight);
  _memberTypeLabel.frame = CGRectMake(CGRectGetMinX(_nameLabel.frame), CGRectGetMaxY(_nameLabel.frame) + 4.0, CGRectGetWidth(_nameLabel.frame), memberTypeHeight);
}

@end
