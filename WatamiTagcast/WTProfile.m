//
//  WTProfile.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/1/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTProfile.h"

@implementation WTProfile

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"photoURL":@"photo"}];
}

- (void)setPhotoURLWithNSString:(NSString *)string {
  _photoURL = [NSURL URLWithString:string];
}

@end
