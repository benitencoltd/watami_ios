//
//  CouponViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 1/11/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>
#import "CouponViewController.h"
#import "StampViewController.h"

@interface CouponViewController () <CBCentralManagerDelegate>

@end

@implementation CouponViewController {
  CBCentralManager *_bluetoothManager;
  BOOL _isVisible;
  BOOL _alreadyAlerted;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    _bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionShowPowerAlertKey:@NO}];
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [Common trackScreen:@"coupon_screen"];
  _isVisible = YES;
  if (_bluetoothManager.state != CBCentralManagerStatePoweredOn && !_alreadyAlerted) {
    _alreadyAlerted = YES;
    [Common showAlert:NSLocalizedString(@"bluetooth_mes", nil) title:nil _self:self];
  }
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  _isVisible = NO;
  _alreadyAlerted = NO;
}

- (void)showStamp:(UITapGestureRecognizer *)gestureRecognizer {
  BOOL poweredOn = _bluetoothManager.state == CBCentralManagerStatePoweredOn;
  if (!poweredOn) {
    [Common showAlert:NSLocalizedString(@"bluetooth_mes", nil) title:nil _self:self];
  } else {
    StampViewController *stampViewController = [[StampViewController alloc] init];
    [self.navigationController pushViewController:stampViewController animated:YES];
  }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
  BOOL poweredOn = _bluetoothManager.state == CBCentralManagerStatePoweredOn;
  if (!poweredOn) {
    [Common showAlert:NSLocalizedString(@"bluetooth_mes", nil) title:nil _self:self];
  }
  return poweredOn;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
  if (_bluetoothManager.state != CBCentralManagerStatePoweredOn && _isVisible && !_alreadyAlerted) {
    _alreadyAlerted = YES;
    [Common showAlert:NSLocalizedString(@"bluetooth_mes", nil) title:nil _self:self];
  }
}

@end
