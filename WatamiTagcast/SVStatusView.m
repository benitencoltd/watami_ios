//
//  SVStatusView.m
//  SVKit
//
//  Created by Sereivoan Yong on 3/31/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "SVStatusView.h"

#define IMAGE_WIDTH 60.0

@implementation SVStatusView {
  NSMutableDictionary<NSNumber *, NSString *> *_titles;
  NSMutableDictionary<NSNumber *, NSString *> *_subtitles;
  NSMutableDictionary<NSNumber *, UIImage *> *_images;
}

@synthesize indicatorView = _indicatorView;
@synthesize imageView = _imageView;
@synthesize titleLabel = _titleLabel;
@synthesize subtitleLabel = _subtitleLabel;

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor clearColor];
    _images = [NSMutableDictionary dictionary];
    _titles = [NSMutableDictionary dictionary];
    _subtitles = [NSMutableDictionary dictionary];
    
    [self setSubtitle:NSLocalizedString(@"loading", nil).uppercaseString forState:SVStatusViewStateAnimating];
    [self setTitle:NSLocalizedString(@"nothing_here", nil) forState:SVStatusViewStateEmpty];
    [self setTitle:NSLocalizedString(@"cannot_connect", nil) forState:SVStatusViewStateError];
    [self setSubtitle:NSLocalizedString(@"tap_to_retry", nil) forState:SVStatusViewStateError];
    
    [self setImage:[[UIImage imageNamed:@"data_empty.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:SVStatusViewStateEmpty];
    [self setImage:[[UIImage imageNamed:@"data_error.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:SVStatusViewStateError];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    [self addSubview:_indicatorView];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    [_imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)]];
    [self addSubview:_imageView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.font = [UIFont appFontOfSize:14.0];
    _titleLabel.numberOfLines = 1;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titleLabel];
    
    _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _subtitleLabel.numberOfLines = 2;
    _subtitleLabel.font = [UIFont appFontOfSize:12.0];
    _subtitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_subtitleLabel];
    
    self.style = SVStatusViewStyleBlack;
    self.state = SVStatusViewStateNone;
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  CGFloat totalHeight = 0.0;
  if (_imageView.image) {
    totalHeight += IMAGE_WIDTH;
  } else {
    totalHeight += self.indicatorView.frame.size.height;
  }
  
  CGSize titleSize = CGSizeZero;
  if (_titleLabel.text.length > 0) {
    titleSize.width = ceil(self.frame.size.width / 3);
    titleSize.height = ceil(_titleLabel.font.lineHeight);
    totalHeight += titleSize.height;
  }
  
  CGSize subtitleSize = CGSizeZero;
  if (_subtitleLabel.text.length > 0) {
    subtitleSize.width = ceil(self.frame.size.width / 2);
    subtitleSize.height = ceil([_subtitleLabel sizeThatFits:CGSizeMake(ceil(self.frame.size.width / 3), CGFLOAT_MAX)].height);
    totalHeight += subtitleSize.height;
  }
  
  CGFloat offsetY = _insets.top + (self.frame.size.height - _insets.top - totalHeight) / 2;
  if (_imageView.image) {
    _imageView.frame = CGRectMake((self.frame.size.width - IMAGE_WIDTH) / 2, offsetY, IMAGE_WIDTH, IMAGE_WIDTH);
    offsetY += IMAGE_WIDTH + 4.0;
  } else {
    self.indicatorView.center = CGPointMake(self.frame.size.width / 2, offsetY  + _indicatorView.frame.size.height / 2);
    offsetY += self.indicatorView.frame.size.height;
  }
  if (_titleLabel.text.length > 0) {
    _titleLabel.frame = CGRectMake((self.frame.size.width - titleSize.width) / 2, offsetY, titleSize.width, titleSize.height);
    offsetY = CGRectGetMaxY(_titleLabel.frame) + 4.0;
  }
  
  if (_subtitleLabel.text.length > 0) {
    _subtitleLabel.frame = CGRectMake((self.frame.size.width - subtitleSize.width) / 2, offsetY, subtitleSize.width, subtitleSize.height);
  }
}

- (void)didTap:(UITapGestureRecognizer *)recognizer {
  if (_state == SVStatusViewStateError && _reloadHandler) {
    _reloadHandler();
  }
}

- (void)setStyle:(SVStatusViewStyle)style {
  _style = style;
  if (style == SVStatusViewStyleBlack) {
    _indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    _imageView.tintColor = [UIColor blackColor];
    _titleLabel.textColor = [UIColor blackColor];
    _subtitleLabel.textColor = [UIColor blackColor];
  } else {
    _indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    _imageView.tintColor = [UIColor whiteColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _subtitleLabel.textColor = [UIColor whiteColor];
  }
}

- (void)setState:(SVStatusViewState)state {
  _state = state;
  NSNumber *stateNumber = @(state);
  self.userInteractionEnabled = state == SVStatusViewStateError;
  _imageView.userInteractionEnabled = self.userInteractionEnabled;
  
  if (state == SVStatusViewStateAnimating) {
    [_indicatorView startAnimating];
  } else {
    [_indicatorView stopAnimating];
  }
  _imageView.image = _images[stateNumber] ?: nil;
  _titleLabel.text = _titles[stateNumber] ?: nil;
  _subtitleLabel.text = _subtitles[stateNumber] ?: nil;
  
  self.hidden = state == SVStatusViewStateNone;
  self.imageView.userInteractionEnabled = state == SVStatusViewStateError;
  [self setNeedsLayout];
}

- (void)setStateFromObject:(id)object {
  if (object) {
    if ([object isKindOfClass:[NSArray class]]) {
      self.state = [(NSArray *)object count] > 0 ? SVStatusViewStateNone : SVStatusViewStateEmpty;
    } else {
      self.state = SVStatusViewStateNone;
    }
  } else {
    self.state = SVStatusViewStateError;
  }
}

- (void)setImage:(UIImage *)image forState:(SVStatusViewState)state {
  _images[@(state)] = image;
}

- (void)setTitle:(NSString *)title forState:(SVStatusViewState)state {
  _titles[@(state)] = title;
}

- (void)setSubtitle:(NSString *)subtitle forState:(SVStatusViewState)state {
  _subtitles[@(state)] = subtitle;
}

- (UIImage *)imageForState:(SVStatusViewState)state {
  return _images[@(state)];
}

- (NSString *)titleForState:(SVStatusViewState)state {
  return _titles[@(state)];
}

- (NSString *)subtitleForState:(SVStatusViewState)state {
  return _subtitles[@(state)];
}

@end
