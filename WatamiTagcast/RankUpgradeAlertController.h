//
//  RankUpgradeAlertController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/1/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WTRank.h"

@interface RankUpgradeAlertController : UIViewController

@property (nonatomic, strong, readonly) WTRank *currentRank;
@property (nonatomic, strong, readonly) WTRank *previousRank;

- (instancetype)initWithCurrentRank:(WTRank *)currentRank previousRank:(WTRank *)previousRank;

@end
