//
//  TicketCollectionViewCell.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/15/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBShareButton.h"

@interface TicketCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong, readonly) UILabel *imageTextLabel;
@property (nonatomic, strong, readonly) UILabel *expiryDateLabel;
@property (nonatomic, strong, readonly) FBShareButton *shareButton;

@property (nonatomic, copy) void (^shareHandler)(UIButton *sender); // must not be nil

+ (CGFloat)rowHeightForWidth:(CGFloat)width;

@end

