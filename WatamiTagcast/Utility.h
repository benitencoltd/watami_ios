//
//  Utility.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/3/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CGSIZE_MAX CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)

@interface UIViewController (Util)

@property (nonatomic, assign) BOOL allowsUserInteraction;
@property (nonatomic, assign, readonly, getter=isVisible) BOOL visible;

- (UINavigationController *)embedInNavigationController;

@end

@interface UINavigationBar (Utils)

- (void)removeBackBarButtonItemTitle;

@end

@interface UIButton (Utils)

// backgroundColor must be set before calling this method and will not be shown unless image is set.
- (void)addBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius;

@end

@interface NSArray<ObjectType> (Map)

- (NSArray *)objectsByTransform:(id (^)(ObjectType obj))transform;
- (id)objectByReduceTo:(id)initial reduce:(id (^)(id result, ObjectType obj))reduce;

@end

@interface UIImage (Utils)

- (UIImage *)imageWithInsetPercent:(CGFloat)percent;
- (UIImage *)imageByResizingToSize:(CGSize)size;
- (UIImage *)imageByScalingAspectFitSize:(CGSize)size;
- (UIImage *)imageWithSize:(CGSize)size cornerRadius:(CGFloat)cornerRadius;
- (UIImage *)imageWithTintColor:(UIColor *)tintColor;

+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

@end
