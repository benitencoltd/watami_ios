//
//  BaseTicketsViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <Tagcast/TGCTagcastManager.h>
#import "Constants.h"
#import "BaseTicketsViewController.h"
#import "LanguagePickerController.h"
#import "CollectionHeaderView.h"
#import "TicketCollectionViewCell.h"

@interface BaseTicketsViewController () <CBCentralManagerDelegate, TGCTagcastMangerDelegate>

@end

@implementation BaseTicketsViewController  {
  CBCentralManager *_bluetoothManager;
  
  NSArray<UIBarButtonItem *> *_previousLeftBarButtonItems;
  NSArray<UIBarButtonItem *> *_previousRightBarButtonItems;
  NSString *_previousNavigationItemTitle;
  BOOL _actionNavigationSet;
  
  NSURLSessionDataTask *_tagcastsTask;
}

- (instancetype)initWithObjectClass:(Class)objectClass tagcasts:(NSArray<NSDictionary<NSString *,id> *> *)tagcasts {
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.sectionInset = UIEdgeInsetsMake(10.0, 9.0, 10.0, 9.0);
  flowLayout.minimumLineSpacing = 10.0;
  self = [super initWithObjectClass:objectClass cellClass:[TicketCollectionViewCell class] layout:flowLayout];
  if (self) {
    self.title = NSLocalizedString(@"tickets", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(showLanguagePicker:)];
    _sectionHeaderHeight = [CollectionHeaderView headHeightForWidth:[UIScreen mainScreen].bounds.size.width text:NSLocalizedString(@"coupon_message", nil)];
    _bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionShowPowerAlertKey:@NO}];
    _loadsTagcastsOnViewDidLoad = YES;
    _tagcasts = tagcasts;
    _shouldManageScanningTagcasts = YES;
  }
  return self;
}

- (void)dealloc {
  [_tagcastsTask cancel];
}

#pragma mark - View Life Cycle

- (void)loadView {
  [super loadView];
  self.view.backgroundColor = DARK_RED_COLOR;
  
  _collectionView.allowsMultipleSelection = YES;
  _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
  [_collectionView registerClass:[CollectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([CollectionHeaderView class])];
  
  self.statusView.style = SVStatusViewStyleWhite;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self.navigationController.navigationBar removeBackBarButtonItemTitle];
  self.statusView.insets = UIEdgeInsetsMake(_sectionHeaderHeight, 0.0, 0.0, 0.0);
  if (!_tagcasts && _loadsTagcastsOnViewDidLoad) {
    [self reloadTagcasts];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [_bluetoothManager.delegate centralManagerDidUpdateState:_bluetoothManager];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [[TGCTagcastManager sharedManager] stopScan];
}

#pragma mark -

- (void)clearSelections:(UIBarButtonItem *)sender {
  for (NSIndexPath *indexPath in [_collectionView indexPathsForSelectedItems]) {
    [_collectionView deselectItemAtIndexPath:indexPath animated:NO];
  }
  [self updateSelections];
}

- (void)claim:(UIButton *)sender {
  if (!_hasMatchedTagcasts) {
    [Common showAlert:NSLocalizedString(@"stamp_instruct", nil) title:nil _self:self];
    return;
  }
  [self claim];
}

- (void)showLanguagePicker:(UIBarButtonItem *)sender {
  LanguagePickerController *languagePickerController = [[LanguagePickerController alloc] init];
  [self.tabBarController presentViewController:[languagePickerController embedInNavigationController] animated:YES completion:nil];
}

#pragma mark -

- (void)reloadTagcasts {
  [_tagcastsTask cancel];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"gettagcast",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _tagcastsTask = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(BaseTicketsViewController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      if ([json[@"result"] boolValue]) {
        strongSelf->_tagcasts = json[@"data"][@"tagcasts"];
        strongSelf->_hasMatchedTagcasts = NO;
      } else {
        [Common showAlert:json[@"message"] title:nil _self:strongSelf];
      }
    });
  }];
  [_tagcastsTask resume];
}

- (void)updateSelections {
  UINavigationItem *navigationItem = _actionNavigationItem ?: self.navigationItem;
  NSArray<NSIndexPath *> *indexPathsForSelectedItems = [_collectionView indexPathsForSelectedItems];
  if (indexPathsForSelectedItems.count > 0) {
    if (!_actionNavigationSet) { // avoid duplicated setting up
      _previousLeftBarButtonItems = navigationItem.leftBarButtonItems;
      _previousRightBarButtonItems = navigationItem.rightBarButtonItems;
      _previousNavigationItemTitle = navigationItem.title;
      _actionNavigationSet = YES;
      
      UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"cross"] style:UIBarButtonItemStylePlain target:self action:@selector(clearSelections:)];
      navigationItem.leftBarButtonItems = @[clearBarButtonItem];
      
      UIButton *claimButton = [UIButton buttonWithType:UIButtonTypeCustom];
      claimButton.backgroundColor = [UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:1.0];
      claimButton.clipsToBounds = YES;
      claimButton.contentEdgeInsets = UIEdgeInsetsMake(3.0, 16.0, 3.0, 16.0);
      claimButton.titleLabel.font = [UIFont appFontOfSize:17.0];
      [claimButton setTitle:@"GET" forState:UIControlStateNormal];
      [claimButton sizeToFit];
      claimButton.layer.cornerRadius = claimButton.frame.size.height / 2;
      [claimButton addTarget:self action:@selector(claim:) forControlEvents:UIControlEventTouchUpInside];
      UIBarButtonItem *claimBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:claimButton];
      navigationItem.rightBarButtonItems = @[claimBarButtonItem];
    }
    navigationItem.title = [NSString stringWithFormat:@"%u %@", (unsigned)indexPathsForSelectedItems.count, NSLocalizedString(@"selected", nil)];
  } else {
    _actionNavigationSet = NO;
    navigationItem.leftBarButtonItems = _previousLeftBarButtonItems;
    _previousLeftBarButtonItems = nil;
    navigationItem.rightBarButtonItems = _previousRightBarButtonItems;
    _previousRightBarButtonItems = nil;
    navigationItem.title = _previousNavigationItemTitle;
    _previousNavigationItemTitle = nil;
  }
}

- (void)claim {
  NSAssert(NO, @"Subclass must override.");
}

//- (void)reloadTickets {
//  NSAssert(NO, @"Subclass must override.");
//}

- (BOOL)isTagcastEnabledForCurrentFeature:(NSDictionary<NSString *, id> *)tagcast {
  NSAssert(NO, @"Subclass must override.");
  return NO;
}

- (NSArray<NSIndexPath *> *)sortedIndexPathsForSelectedItems {
  return [[_collectionView indexPathsForSelectedItems] sortedArrayUsingComparator:^NSComparisonResult(NSIndexPath *indexPath1, NSIndexPath *indexPath2) {
    return indexPath1.item > indexPath2.item ? NSOrderedAscending : NSOrderedDescending;
  }];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
  return CGSizeMake(collectionView.frame.size.width, _sectionHeaderHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewFlowLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  CGFloat itemWidth = collectionView.frame.size.width - collectionViewLayout.sectionInset.left - collectionViewLayout.sectionInset.right;
  return CGSizeMake(itemWidth, [TicketCollectionViewCell rowHeightForWidth:itemWidth]);
}

#pragma mark - UICollectionViewDelegate

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
  if (![kind isEqualToString:UICollectionElementKindSectionHeader]) {
    return nil;
  }
  CollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass([CollectionHeaderView class]) forIndexPath:indexPath];
  headerView.titleLabel.text = NSLocalizedString(@"important_note", nil);
  headerView.detailLabel.text = NSLocalizedString(@"coupon_message", nil);
  return headerView;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  [self updateSelections];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
  [self updateSelections];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
  if (!_shouldManageScanningTagcasts) {
    return;
  }
  if (central.state != CBCentralManagerStatePoweredOn) {
    [[TGCTagcastManager sharedManager] stopScan];
    _hasMatchedTagcasts = NO;
  } else {
    [TGCTagcastManager provideAPIKey:kTagcastAPIKey];
    [TGCTagcastManager sharedManager].delegate = self;
    [[TGCTagcastManager sharedManager] startScan];
  }
}

#pragma mark - TGCTagcastMangerDelegate

- (void)tagcastManager:(TGCTagcastManager *)manager didDiscoveredTagcast:(NSDictionary<NSString *, id> *)tagcast {
  NSString *serialIdentifier = tagcast[kTGCKeyData][kTGCKeySerialIdentifier];
  for (NSDictionary<NSString *, id> *enabledTagcast in _tagcasts) {
    if ([self isTagcastEnabledForCurrentFeature:enabledTagcast] && [enabledTagcast[@"serial_number"] isEqualToString:serialIdentifier]) {
      _hasMatchedTagcasts = YES;
      break;
    }
  }
}

- (void)tagcastManager:(TGCTagcastManager *)manager didScannedTagcasts:(NSArray<NSDictionary<NSString *, id> *> *)tagcasts {
  if (tagcasts.count == 0) {
    _hasMatchedTagcasts = NO;
  }
}

@end
