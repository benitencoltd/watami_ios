//
//  BaseTicketsViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QueryCollectionViewController.h"
#import "TicketCollectionViewCell.h"

// This class is not for directly used. Instead, use Roulette/StampTicketsViewController
@interface BaseTicketsViewController<Ticket: JSONModel *> : QueryCollectionViewController<Ticket> {
  @protected
  BOOL _hasMatchedTagcasts;
}

@property (nonatomic, strong) UINavigationItem *actionNavigationItem; // If nil, self.navigationItem will be used.
@property (nonatomic, copy) NSArray<NSDictionary<NSString *, id> *> *tagcasts;
@property (nonatomic, assign, readonly) CGFloat sectionHeaderHeight;
@property (nonatomic, assign) BOOL loadsTagcastsOnViewDidLoad; // Default to YES.
@property (nonatomic, assign) BOOL shouldManageScanningTagcasts; // Default to YES.

- (instancetype)initWithObjectClass:(Class)objectClass tagcasts:(NSArray<NSDictionary<NSString *, id> *> *)tagcasts;

- (NSArray<NSIndexPath *> *)sortedIndexPathsForSelectedItems;
- (BOOL)isTagcastEnabledForCurrentFeature:(NSDictionary<NSString *, id> *)tagcast;
- (void)reloadTagcasts;
- (void)claim;
- (void)updateSelections;

@end
