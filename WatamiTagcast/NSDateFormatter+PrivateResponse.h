//
//  NSDateFormatter+PrivateResponse.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (PrivateResponse)

@property (nonatomic, strong, readonly, class) NSDateFormatter *responseSharedInstance;

@end
