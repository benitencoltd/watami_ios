//
//  WTVIPUser.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/28/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "WTRank.h"

@interface WTVIPUser : JSONModel

@property (nonatomic, assign) NSUInteger accessId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSURL *photoURL;
@property (nonatomic, assign) NSUInteger numberOfStamps;
@property (nonatomic, strong) WTRank *rank;

@end
