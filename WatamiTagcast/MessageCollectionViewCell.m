//
//  MessageCollectionViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "MessageCollectionViewCell.h"

#define kTitleFont [UIFont systemFontOfSize:15.0]
#define kTextFont [UIFont systemFontOfSize:15.0]
#define kDateFont [UIFont systemFontOfSize:13.0]

static const CGFloat imageHeight = 120.0;

@implementation MessageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    UIImageView *backgroundView = [[UIImageView alloc] initWithFrame:CGRectZero];
    backgroundView.contentMode = UIViewContentModeScaleToFill;
    backgroundView.image = [UIImage imageNamed:@"message_background.jpg"];
    self.backgroundView = backgroundView;
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.font = kTitleFont;
    [self.contentView addSubview:_titleLabel];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    _imageView.userInteractionEnabled = YES;
    [_imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapImage:)]];
    [self.contentView addSubview:_imageView];
    
    _textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _textLabel.font = kTextFont;
    _textLabel.numberOfLines = 0;
    [self.contentView addSubview:_textLabel];
    
    _dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _dateLabel.font = kDateFont;
    _dateLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_dateLabel];
    
    _shareButton = [FBShareButton button];
    [_shareButton addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_shareButton];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  CGFloat contentWidth = self.contentView.frame.size.width - 16.0;
  _titleLabel.frame = CGRectMake(8.0, 8.0, contentWidth, ceil(_titleLabel.font.lineHeight));
  _imageView.frame = CGRectMake(8.0, CGRectGetMaxY(_titleLabel.frame) + 8.0, contentWidth, imageHeight);
  CGFloat dateHeight = ceil(_dateLabel.font.lineHeight);
  _dateLabel.frame = CGRectMake(8.0, self.contentView.frame.size.height - dateHeight - 8.0, contentWidth, dateHeight);
  
  AddShadowToView(self);
  [_shareButton sizeToFit];
  _shareButton.frame = CGRectMake(8.0, self.contentView.frame.size.height - _shareButton.frame.size.height - 8.0, _shareButton.frame.size.width, _shareButton.frame.size.height);
  
  CGFloat textHeight = CGRectGetMinY(_shareButton.frame) - 8.0 - 8.0 - CGRectGetMaxY(_imageView.frame);
  _textLabel.frame = CGRectMake(8.0, CGRectGetMaxY(_imageView.frame) + 8.0, contentWidth, textHeight);
}

- (void)didTapImage:(UITapGestureRecognizer *)gestureRecognizer {
  _imageTapHandler();
}

- (void)share:(UIButton *)sender {
  _shareHandler();
}

+ (CGFloat)itemHeightForMessage:(WTMessage *)message width:(CGFloat)width {
  CGFloat textHeight = ceil([message.text boundingRectWithSize:CGSizeMake(width - 16.0, CGFLOAT_MAX)
                                                       options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{NSFontAttributeName:kTextFont}
                                                       context:NULL].size.height);
  return 8.0 + ceil(kTitleFont.lineHeight) + 8.0 + imageHeight + 8.0 + textHeight + 8.0 + 36.0 + 8.0;
}

@end
