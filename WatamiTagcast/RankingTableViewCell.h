//
//  RankingTableViewCell.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/21/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RankingTableViewCell : UITableViewCell

@property (nonatomic, strong, readonly) UILabel *numberLabel;
@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, strong, readonly) UILabel *countLabel;

@end
