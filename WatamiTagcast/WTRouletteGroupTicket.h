//
//  WTRouletteGroupTicket.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/17/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "WTRouletteTicket.h"

@interface WTRouletteGroupTicket : JSONModel

@property (nonatomic, copy) NSArray<WTRouletteTicket *> *tickets;
@property (nonatomic, assign) WTRouletteTicketType type;

@end
