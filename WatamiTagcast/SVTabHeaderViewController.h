//
//  SVTabHeaderViewController.h
//  SVKit
//
//  Created by Sereivoan Yong on 7/29/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVTabHeaderView.h"
#import "SVTabListView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol VOStretchableHeaderTabItem <NSObject>

@required
- (UIScrollView *)stretchableSubViewInSubViewController:(UIViewController *)subViewController;

@end

@interface SVTabHeaderViewController : UIViewController <UIScrollViewDelegate>

/// automaticallyAdjustsScrollViewInsets can only be changed before viewDidLoad(_:) is called
@property (nonatomic, strong, readonly) NSArray<UIViewController *> *viewControllers;

/// Return headerView which is of headerClass type passed in initializer.
@property (nonatomic, strong, readonly) SVTabHeaderView *headerView;

/// Return tabBar that contains viewControllers' titles as items or `nil` if showsTabBar is NO
@property (nonatomic, strong, readonly, null_resettable) SVTabListView *tabBar;

/// Return containerView managing viewControllers' view or `nil` if view is not loaded yet.
@property (nonatomic, strong, readonly, null_resettable) UIScrollView *containerView;

/// Current selected viewController index.
@property (nonatomic, assign) NSInteger selectedIndex;

/// Set to `YES` to setup tabBar, `NO` to completely remove tabBar.
@property (nonatomic, assign) BOOL showsTabBar;

/// Set to `YES` to put tabBar inside headerView as subview. Otherwise, below headerView.
@property (nonatomic, assign, getter=isTabBarInsideHeader) BOOL tabBarInsideHeader;

/// height of tabBar. Has no effect if `showsTabBar` is `NO`.
@property (nonatomic, assign) CGFloat tabBarHeight;

/// Return progress [0.0 - 0.1] (minimumHeight compared to defaultHeight).
@property (nonatomic, assign, readonly) CGFloat headerProgress;

// viewControllers must be non-empty.
- (instancetype)initWithViewControllers:(NSArray<UIViewController *> *)viewControllers;

- (instancetype)initWithViewControllers:(NSArray<UIViewController *> *)viewControllers headerViewClass:(Class)headerViewClass;

// Layout
- (void)layoutHeaderViewAndTabBar;

// This will reset frame of view controller's view only if it is different
- (void)layoutViewControllersIfNeeded;

- (void)layoutSubViewControllerToSelectedViewController;
- (void)adjustScrollIndicatorInsetsOfScrollView:(UIScrollView *)scrollView;
- (void)adjustSelectedScrollViewOffsetIfNotFits;

@end

NS_ASSUME_NONNULL_END
