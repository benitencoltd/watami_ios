//
//  RouletteGroupTicketCollectionViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/18/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "RouletteGroupTicketCollectionViewCell.h"

#define IMAGE_RATIO (159.0 / 320.0)

@implementation RouletteGroupTicketCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 4.0;
    AddShadowToView(self);
    self.contentView.clipsToBounds = YES;
    self.contentView.layer.cornerRadius = 4.0;
    
    _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
    _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:_imageView];
  }
  return self;
}

+ (CGFloat)rowHeightForWidth:(CGFloat)width {
  return ceil(width * IMAGE_RATIO);
}

@end
