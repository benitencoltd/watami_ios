//
//  CollectionHeaderView.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/18/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionHeaderView : UICollectionReusableView

@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, strong, readonly) UILabel *detailLabel;

+ (CGFloat)headHeightForWidth:(CGFloat)width text:(NSString *)text;

@end
