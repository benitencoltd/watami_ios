//
//  WTMemberCard.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/20/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTMemberCard.h"

@implementation WTMemberCard

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"appLinkId":@"app_link",
                                                                @"photoURL":@"photo"}];
}

- (void)setPhotoURLWithNSString:(NSString *)string {
  _photoURL = [NSURL URLWithString:string];
}

@end
