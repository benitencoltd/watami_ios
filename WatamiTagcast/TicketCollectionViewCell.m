//
//  TicketCollectionViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/15/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "TicketCollectionViewCell.h"

#define kClaimButtonHeight 36.0

static const CGFloat kImageRatio = 159.0/320.0;

@interface TicketCollectionViewCell ()

@property (nonatomic, strong) UIImageView *selectedCheckBox;

@end

@implementation TicketCollectionViewCell {
  CAGradientLayer *_gradientLayer;
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 4.0;
    AddShadowToView(self);
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.4];
    selectedBackgroundView.layer.cornerRadius = 4.0;
    self.selectedBackgroundView = selectedBackgroundView;
    
    _selectedCheckBox = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 50.0, 50.0)];
    _selectedCheckBox.contentMode = UIViewContentModeScaleAspectFit;
    _selectedCheckBox.image = [[UIImage imageNamed:@"check.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _selectedCheckBox.tintColor = DARK_RED_COLOR;
    [selectedBackgroundView addSubview:_selectedCheckBox];
    
    self.contentView.clipsToBounds = YES;
    self.contentView.layer.cornerRadius = 4.0;
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _imageView.clipsToBounds = YES;
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:_imageView];
    
    _gradientLayer = [[CAGradientLayer alloc] init];
    _gradientLayer.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor colorWithWhite:0.0 alpha:0.05].CGColor, (id)[UIColor colorWithWhite:0.0 alpha:0.275].CGColor, (id)[UIColor colorWithWhite:0.0 alpha:0.5].CGColor];
    _gradientLayer.locations = @[@0.0, @(1.0 / 3.0), @(1.0 / 3.0 * 2.0), @1.0];
    [_imageView.layer addSublayer:_gradientLayer];
    
    _imageTextLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _imageTextLabel.font = [UIFont appFontOfSize:15.0];
    _imageTextLabel.textColor = [UIColor whiteColor];
    [_imageView addSubview:_imageTextLabel];
    
    _expiryDateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _expiryDateLabel.font = [UIFont appFontOfSize:16.0];
    [self.contentView addSubview:_expiryDateLabel];
    
    _shareButton = [FBShareButton button];
    _shareButton.titleLabel.font = [UIFont appFontOfSize:15.0];
    [_shareButton addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_shareButton];
    
    [self bringSubviewToFront:self.selectedBackgroundView];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  // + 2.0 coz the image has empty area at right edge
  _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width + 1.0, ceil(self.contentView.frame.size.width * kImageRatio));
  CGFloat imageTextHeight = ceil(_imageTextLabel.font.lineHeight);
  CGFloat gradientHeight = imageTextHeight + 24.0;
  _gradientLayer.frame = CGRectMake(0.0, _imageView.frame.size.height - gradientHeight, _imageView.frame.size.width, gradientHeight);
  _imageTextLabel.frame = CGRectMake(12.0, CGRectGetMinY(_gradientLayer.frame) + 12.0, self.contentView.frame.size.width - 24.0, imageTextHeight);
  
  [_shareButton sizeToFit];
  CGRect shareRect = _shareButton.frame;
  shareRect.origin.x = self.contentView.frame.size.width - shareRect.size.width - 8.0;
  shareRect.origin.y = CGRectGetMaxY(_imageView.frame) + 8.0;
  _shareButton.frame = shareRect;
  CGFloat expiryDateHeight = ceil(_expiryDateLabel.font.lineHeight);
  _expiryDateLabel.frame = CGRectMake(12.0, CGRectGetMidY(_shareButton.frame) - expiryDateHeight / 2.0, CGRectGetMinX(_shareButton.frame) - 24.0, expiryDateHeight);
  
  _selectedCheckBox.center = CGPointMake(self.selectedBackgroundView.frame.size.width / 2, self.selectedBackgroundView.frame.size.height / 2);
}

- (void)share:(UIButton *)sender {
  _shareHandler(sender);
}

+ (CGFloat)rowHeightForWidth:(CGFloat)width {
  return ceil(width * kImageRatio) + 8.0 + kClaimButtonHeight + 8.0;
}

@end
