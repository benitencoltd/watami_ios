//
//  WTRouletteTicket.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/15/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTRouletteTicket.h"
#import "NSDateFormatter+PrivateResponse.h"

NSString *WTRouletteTicketStringFromType(WTRouletteTicketType type) {
  switch (type) {
    case WTRouletteTicketTypeGold:
      return @"Gold";
    case WTRouletteTicketTypeSilver:
      return @"Silver";
    case WTRouletteTicketTypeBronze:
      return @"Bronze";
    case WTRouletteTicketTypeLose:
      return @"Lose";
  }
}

@implementation WTRouletteTicket

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"id":@"lottery_id",
                                                                @"text":@"description",
                                                                @"imageURL":@"image",
                                                                @"date":@"lottery_date",
                                                                @"expiryDate":@"expiry_date"}];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

- (void)setImageURLWithNSString:(NSString *)string {
  _imageURL = [NSURL URLWithString:string];
}

- (void)setDateWithNSString:(NSString *)string {
  _date = [[NSDateFormatter responseSharedInstance] dateFromString:[string componentsSeparatedByString:@" "].firstObject];
}

- (void)setExpiryDateWithNSString:(NSString *)string {
  _expiryDate = [[NSDateFormatter responseSharedInstance] dateFromString:[string componentsSeparatedByString:@" "].firstObject];
}

@end
