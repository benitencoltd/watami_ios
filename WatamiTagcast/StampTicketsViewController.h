//
//  StampTicketsViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/9/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTicketsViewController.h"
#import "WTStampTicket.h"
#import "SVTabHeaderViewController.h"

static NSNotificationName const kUserDidClaimStampTicketsNotification = @"kUserDidClaimStampTicketsNotification";

@interface StampTicketsViewController : BaseTicketsViewController<WTStampTicket *> <VOStretchableHeaderTabItem>

@property (nonatomic, assign) BOOL includesIncentiveTickets; // Default to NO

- (void)reloadDataFromTickets:(NSArray<WTStampTicket *> *)tickets;

@end
