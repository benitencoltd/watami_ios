//
//  VIPUsersViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/28/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultViewController.h"

@interface VIPUsersViewController : DefaultViewController

@property (nonatomic, assign) BOOL isMonthly;

- (instancetype)init;

@end
