//
//  WTMessage.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTMessage : JSONModel

@property (nonatomic, assign) NSUInteger id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSURL *imageURL;

@end
