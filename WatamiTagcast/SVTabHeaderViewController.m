//
//  SVTabHeaderViewController.m
//  SVKit
//
//  Created by Sereivoan Yong on 7/29/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

//#import <MJRefresh/MJRefresh.h>
#import "SVTabHeaderViewController.h"

@implementation SVTabHeaderViewController {
  CGFloat _lastOffsetX;
}

- (instancetype)initWithViewControllers:(NSArray<UIViewController *> *)viewControllers {
  self = [super initWithNibName:nil bundle:nil];
  if (self) {
    _headerView = [[SVTabHeaderView alloc] initWithFrame:CGRectZero];
    [self _commonInitWithViewControllers:viewControllers];
  }
  return self;
}

- (instancetype)initWithViewControllers:(NSArray<UIViewController *> *)viewControllers headerViewClass:(Class)headerViewClass {
  self = [super initWithNibName:nil bundle:nil];
  if (self) {
    _headerView = [[headerViewClass alloc] initWithFrame:CGRectZero];
    [self _commonInitWithViewControllers:viewControllers];
  }
  return self;
}

- (void)_commonInitWithViewControllers:(NSArray<UIViewController *> *)viewControllers {
  self.automaticallyAdjustsScrollViewInsets = NO;
  _selectedIndex = 0;
  _viewControllers = viewControllers;
  _tabBarInsideHeader = NO;
  self.showsTabBar = YES;
  _tabBarHeight = _showsTabBar ? 40.0 : 0.0;
}

- (void)dealloc {
  for (UIViewController *viewController in _viewControllers) {
    UIScrollView *scrollView = [self scrollViewInViewController:viewController];
    if (scrollView) {
      [scrollView removeObserver:self forKeyPath:@"contentOffset"];
    }
    [viewController removeFromParentViewController];
  };
}

#pragma mark - View Life Cycle

- (void)loadView {
  [super loadView];
  self.view.backgroundColor = [UIColor whiteColor];
  
  _containerView = [[UIScrollView alloc] initWithFrame:CGRectZero];
  _containerView.alwaysBounceHorizontal = YES;
  _containerView.backgroundColor = [UIColor blackColor];
  _containerView.delegate = self;
  _containerView.pagingEnabled = YES;
  _containerView.showsHorizontalScrollIndicator = NO;
  _containerView.showsVerticalScrollIndicator = NO;
  [self.view addSubview:_containerView];
  [self updateContentSizeAndContentOffsetWithSize:_containerView.bounds.size];
  
  [self.view addSubview:_headerView];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  if (_tabBar && !_tabBar.superview) {
    CGFloat tabBarOffsetY = MAX(CGRectGetMaxY(_headerView.frame), CGRectGetMaxY(_tabBar.frame)) - _tabBarHeight;
    _tabBar.frame = CGRectMake(0.0, tabBarOffsetY, self.view.frame.size.width, _tabBarHeight);
    _tabBar.selectedIndex = _selectedIndex;
    [_tabBar layoutIfNeeded];
    [self.view addSubview:_tabBar];
  }
  
  for (NSInteger index = 0; index < _viewControllers.count; index++) {
    UIViewController *viewController = _viewControllers[index];
    BOOL automaticallyAdjustsScrollViewInsets = viewController.automaticallyAdjustsScrollViewInsets;
    [viewController willMoveToParentViewController:self];
    viewController.automaticallyAdjustsScrollViewInsets = NO;
    [self addChildViewController:viewController];
    
    UIScrollView *scrollView = [self scrollViewInViewController:viewController];
    if (scrollView) {
      scrollView.tag = index;
      UIEdgeInsets contentInset = scrollView.contentInset;
      contentInset.top += _headerView.defaultHeight;
      if (!_tabBarInsideHeader) {
        contentInset.top += _tabBarHeight;
      }
      if (automaticallyAdjustsScrollViewInsets && self.tabBarController.tabBar.isTranslucent) {
        contentInset.bottom += self.tabBarController.tabBar.frame.size.height;
      }
//      if (scrollView.mj_footer) {
//        contentInset.bottom += MJRefreshFooterHeight;
//      }
      scrollView.contentInset = contentInset;
      scrollView.contentOffset = CGPointMake(0.0, -contentInset.top);
      [scrollView addObserver:self forKeyPath:@"contentOffset" options:0 context:NULL];
    }
    
    UIView *view = viewController.view;
    view.frame = CGRectMake(_containerView.frame.size.width * (CGFloat)index, 0.0, _containerView.frame.size.width, _containerView.frame.size.height);
    [_containerView addSubview:view];
    
    [viewController didMoveToParentViewController:self];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [_viewControllers[_selectedIndex] beginAppearanceTransition:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [_viewControllers[_selectedIndex] endAppearanceTransition];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [_viewControllers[_selectedIndex] beginAppearanceTransition:NO animated:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [_viewControllers[_selectedIndex] endAppearanceTransition];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  if (!self.navigationController) {
    return;
  }
    
  CGFloat offsetY = [UIApplication sharedApplication].statusBarFrame.size.height - self.navigationController.navigationBar.frame.origin.y;
  _containerView.frame = CGRectMake(0.0, offsetY, self.view.frame.size.width, self.view.frame.size.height - offsetY);
  _containerView.contentSize = CGSizeMake(_containerView.frame.size.width * (CGFloat)_viewControllers.count, _containerView.frame.size.height);
  [self layoutViewControllersIfNeeded];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
  [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
  NSInteger selectedIndex = _selectedIndex;
  [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    _selectedIndex = selectedIndex;
    [self updateContentSizeAndContentOffsetWithSize:size];
    [self layoutHeaderViewAndTabBar];
    [self layoutViewControllersIfNeeded];
  } completion:nil];
}

- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
  return NO;
}

#pragma mark - Properties

- (void)setShowsTabBar:(BOOL)showsTabBar {
  _showsTabBar = showsTabBar;
  if (showsTabBar) {
    __weak typeof(self) weakSelf = self;
    NSMutableArray<NSString *> *titles = [NSMutableArray arrayWithCapacity:_viewControllers.count];
    for (UIViewController *viewController in _viewControllers) {
      [titles addObject:viewController.title];
    }
    _tabBar = [[SVTabListView alloc] initWithItems:titles];
    _tabBar.selectionHandler = ^(NSInteger index) {
      [weakSelf.containerView setContentOffset:CGPointMake((CGFloat)index * weakSelf.containerView.frame.size.width, weakSelf.containerView.contentOffset.y) animated:YES];
      [weakSelf layoutSubViewControllerToSelectedViewController];
    };
    _tabBar.backgroundColor = [UIColor clearColor];
    if ([self isViewLoaded]) {
      [self.view addSubview:_tabBar];
    }
  } else {
    [_tabBar removeFromSuperview];
    _tabBar = nil;
  }
}

- (CGFloat)headerProgress {
  CGFloat minHeight = [self headerMinHeight];
  return (MIN(self.headerView.frame.size.height, self.headerView.defaultHeight) - minHeight) / (self.headerView.defaultHeight - minHeight);
}

- (CGFloat)headerMinHeight {
  if (_headerView.includesNavigationInMinHeight) {
    return CGRectGetMaxY(self.navigationController.navigationBar.frame) + _headerView.minimumHeight;
  }
  return _headerView.minimumHeight;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey, id> *)change context:(void *)context {
  if ([keyPath isEqualToString:@"contentOffset"] && [object isKindOfClass:[UIScrollView class]]) {
    UIScrollView *scrollView = object;
    if (scrollView.tag == _selectedIndex && self.navigationController) {
      [self layoutHeaderViewAndTabBar];
    }
  } else {
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
  }
}

#pragma mark - Layout

- (void)updateContentSizeAndContentOffsetWithSize:(CGSize)size {
  _containerView.contentSize = CGSizeMake(size.width * (CGFloat)_viewControllers.count, size.height);
  _containerView.contentOffset = CGPointMake((CGFloat)_selectedIndex * size.width, 0.0);
}

- (void)adjustScrollIndicatorInsetsOfScrollView:(UIScrollView *)scrollView {
  CGFloat insetTop = MAX(CGRectGetMaxY(_headerView.frame), CGRectGetMaxY(_tabBar.frame));
  UIEdgeInsets scrollIndicatorInsets = UIEdgeInsetsMake(insetTop, 0.0, scrollView.contentInset.bottom, 0.0);
//  if (scrollView.mj_footer) {
//    scrollIndicatorInsets.bottom -= MJRefreshFooterHeight;
//  }
  scrollView.scrollIndicatorInsets = scrollIndicatorInsets;
}

- (void)adjustSelectedScrollViewOffsetIfNotFits {
  CGFloat height = _containerView.frame.size.height - MAX(CGRectGetMaxY(_headerView.frame), CGRectGetMaxY(_tabBar.frame));
  UIScrollView *selectedScrollView = [self scrollViewInViewController:_viewControllers[_selectedIndex]];
  CGFloat contentHeight = selectedScrollView.contentSize.height;
  if (contentHeight < height) {
    [selectedScrollView setContentOffset:CGPointMake(0.0, MAX(contentHeight - _containerView.frame.size.height, -selectedScrollView.contentInset.top)) animated:YES];
  }
}

- (void)layoutHeaderViewAndTabBar {
  UIScrollView *scrollView = [self scrollViewInViewController:_viewControllers[_selectedIndex]];
  if (scrollView) {
    CGFloat headerOffsetY = UIApplication.sharedApplication.statusBarFrame.size.height - self.navigationController.navigationBar.frame.origin.y;
    CGFloat headerHeight = MAX(_headerView.defaultHeight - (scrollView.contentInset.top + scrollView.contentOffset.y), [self headerMinHeight] - headerOffsetY);
    
    if (!_headerView.bounces) {
      headerHeight = MIN(headerHeight, _headerView.defaultHeight);
    }
//    else if (scrollView.mj_header) {
//      headerHeight = MIN(headerHeight, _headerView.maxHeight + MJRefreshHeaderHeight);
//    }
    if (_headerView.shrinkStyle == SVTabHeaderViewShrinkStyleResize) {
      _headerView.frame = CGRectMake(0.0, headerOffsetY, self.view.frame.size.width, headerHeight);
    } else {
      _headerView.frame = CGRectMake(0.0, -_headerView.defaultHeight + headerHeight, self.view.frame.size.width, _headerView.defaultHeight);
    }
  } else {
    CGRect frame = _headerView.frame;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = _headerView.defaultHeight;
    _headerView.frame = frame;
  }
  
  CGFloat tabBarOffsetY = _tabBarInsideHeader ? CGRectGetMaxY(_headerView.frame) - _tabBarHeight : CGRectGetMaxY(_headerView.frame);
  _tabBar.frame = CGRectMake(0.0, tabBarOffsetY, self.view.frame.size.width, _tabBarHeight);
  
  [self adjustScrollIndicatorInsetsOfScrollView:scrollView];
}

- (void)layoutViewControllersIfNeeded {
  for (NSInteger index = 0; index < _viewControllers.count; index++) {
    UIViewController *viewController = _viewControllers[index];
    CGRect frame = CGRectMake(_containerView.bounds.size.width * (CGFloat)index, 0.0, _containerView.bounds.size.width, _containerView.bounds.size.height);
    UIScrollView *scrollView = [self scrollViewInViewController:viewController];
    if (scrollView) {
      if (!CGRectEqualToRect(viewController.view.frame, frame)) {
        viewController.view.frame = frame;
      }
    }
  }
}

- (void)layoutSubViewControllerToSelectedViewController {
  UIScrollView *selectedScrollView = [self scrollViewInViewController:_viewControllers[_selectedIndex]];
  if (!selectedScrollView) return;
  
  for (NSInteger index = 0; index < _viewControllers.count; index++) {
    if (index == _selectedIndex) continue;
    UIScrollView *scrollView = [self scrollViewInViewController:_viewControllers[index]];
    if ([scrollView isKindOfClass:[UIScrollView class]]) {
      CGFloat relativePositionY = [self relativeOffsetYForOffsetY:selectedScrollView.contentOffset.y contentInsetTop:selectedScrollView.contentInset.top];
      if (relativePositionY > 0.0) {
        // The header view's height is higher than minimum height.
        scrollView.contentOffset = selectedScrollView.contentOffset;
      } else {
        // The header view height is lower than minimum height.
        // -> Adjust top of scrollview, If target header view's height is higher than minimum height.
        CGFloat targetRelativePositionY = [self relativeOffsetYForOffsetY:scrollView.contentOffset.y contentInsetTop:scrollView.contentInset.top];
        if (targetRelativePositionY > 0) {
          scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, -(CGRectGetMaxY(_tabBar.frame) - _containerView.contentInset.top));
        }
      }
    } else {
      // -> Adjust frame to area at the bottom of tab bar.
      CGFloat y = CGRectGetMaxY(_tabBar.frame) - _containerView.contentInset.top;
      scrollView.frame = CGRectMake(CGRectGetMinX(scrollView.frame), y, CGRectGetMinX(scrollView.frame), CGRectGetHeight(_containerView.frame) - y);
    }
  }
}

#pragma mark - Private Method

- (CGFloat)relativeOffsetYForOffsetY:(CGFloat)offsetY contentInsetTop:(CGFloat)contentInsetTop {
  return _headerView.defaultHeight - _headerView.minimumHeight - (offsetY + contentInsetTop);
}

- (UIScrollView *)scrollViewInViewController:(UIViewController *)viewController {
  if ([viewController.view isKindOfClass:[UIScrollView class]]) {
    return (id)viewController.view;
  } else if ([viewController isKindOfClass:[UITableViewController class]]) {
    return ((UITableViewController *)viewController).tableView;
  } else if ([viewController isKindOfClass:[UICollectionViewController class]]) {
    return ((UICollectionViewController *)viewController).collectionView;
  } else if ([viewController respondsToSelector:@selector(stretchableSubViewInSubViewController:)]) {
    return [(id<VOStretchableHeaderTabItem>)viewController stretchableSubViewInSubViewController:viewController];
  }
  return nil;
}

- (void)updateSelectedIndex:(NSInteger)selectedIndex {
  UIViewController *oldVC = _viewControllers[_selectedIndex];
  UIViewController *newVC = _viewControllers[selectedIndex];
  [oldVC beginAppearanceTransition:NO animated:NO];
  [newVC beginAppearanceTransition:YES animated:NO];
  [self willChangeValueForKey:@"selectedIndex"];
  _selectedIndex = selectedIndex;
  [self didChangeValueForKey:@"selectedIndex"];
  [oldVC endAppearanceTransition];
  [newVC endAppearanceTransition];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
  [self layoutSubViewControllerToSelectedViewController];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  [_tabBar setIndicatorProgress:scrollView.contentOffset.x / scrollView.frame.size.width scrollingLeft:scrollView.contentOffset.x > _lastOffsetX];
  _lastOffsetX = scrollView.contentOffset.x;
  NSInteger index = round(scrollView.contentOffset.x / scrollView.frame.size.width);
  index = MIN(MAX(0, index), _viewControllers.count - 1);
  if (_selectedIndex != index) {
    [self adjustScrollIndicatorInsetsOfScrollView:[self scrollViewInViewController:_viewControllers[index]]];
    [self updateSelectedIndex:index];
  }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
  [self adjustSelectedScrollViewOffsetIfNotFits];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
  [self adjustSelectedScrollViewOffsetIfNotFits];
}

@end
