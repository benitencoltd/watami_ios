//
//  QueryViewController.h
//  Khemara
//
//  Created by Beniten on 6/21/16.
//  Copyright © 2016 Beniten. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSONModel/JSONModel.h>
#import "SVStatusView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QueryViewController<ObjectType : JSONModel *> : UIViewController {
  @protected
  Class _objectClass;
  NSUInteger _numberOfPages;
}

@property (nonatomic, weak, nullable) UIScrollView *scrollView;
@property (nonatomic, strong, readonly) SVStatusView *statusView;
@property (nonatomic, assign) BOOL showsStatusView;

@property (nonatomic, strong, readonly, nullable) NSURLSessionDataTask *task;
@property (nonatomic, strong, readonly, nullable) NSError *error;
@property (nonatomic, strong, readonly, nullable) NSError *jsonError;

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong, readonly) NSMutableArray<ObjectType> *objects; // never be nil

@property (nonatomic, assign, readonly, getter=isLoading) BOOL loading;

@property (nonatomic, assign, getter=isPagingEnabled) BOOL pagingEnabled;
@property (nonatomic, assign, readonly) NSInteger page; // 0 If pagingEnabled is NO. Otherwise, 1-based.
@property (nonatomic, assign, readonly) NSUInteger numberOfPages;

@property (nonatomic, assign) BOOL loadsObjestsOnViewDidLoad;

- (instancetype)initWithObjectClass:(Class)ObjectClass; // If you're using storyboard, objectClass must be set in `initWithCoder:`.

- (void)reloadDataWithObjects:(NSArray<ObjectType> *)objects reset:(BOOL)reset;
- (void)resetData;

- (void)reloadObjects;
- (void)loadObjectsForNextPage;
- (void)loadObjectsForPage:(NSInteger)page reset:(BOOL)reset;

- (void)taskDidCompleteWithResponse:(NSDictionary<NSString *, id> *)response error:(NSError *)error;

- (NSURLRequest *)request; 
- (NSURLRequest *)requestForPage:(NSInteger)page;

@end

NS_ASSUME_NONNULL_END
