//
//  WTRouletteTicket.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/15/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

typedef NS_ENUM(NSUInteger, WTRouletteTicketType) {
  WTRouletteTicketTypeGold = 1,
  WTRouletteTicketTypeSilver = 2,
  WTRouletteTicketTypeBronze = 3,
  WTRouletteTicketTypeLose = 4,
};

extern NSString *WTRouletteTicketStringFromType(WTRouletteTicketType type);

@interface WTRouletteTicket : JSONModel

@property (nonatomic, assign) NSUInteger id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSURL *imageURL;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSDate *expiryDate;
@property (nonatomic, assign, getter=isUsed) BOOL used;

@end
