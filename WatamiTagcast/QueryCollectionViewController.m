//
//  QueryCollectionViewController.m
//  Khemara
//
//  Created by Beniten on 11/18/16.
//  Copyright © 2016 Beniten. All rights reserved.
//

#import "QueryCollectionViewController.h"

static NSString * const kQueryCellIdentifier = @"QueryCell";

@implementation QueryCollectionViewController
//{
//  CGFloat _itemWidth, _itemHeight;
//}

- (instancetype)initWithObjectClass:(Class)objectClass {
  return [self initWithObjectClass:objectClass cellClass:nil layout:[[UICollectionViewFlowLayout alloc] init]];
}

- (instancetype)initWithObjectClass:(Class)objectClass cellClass:(Class)cellClass {
  return [self initWithObjectClass:objectClass cellClass:cellClass layout:[[UICollectionViewFlowLayout alloc] init]];
}

- (instancetype)initWithObjectClass:(Class)objectClass cellClass:(Class)cellClass layout:(UICollectionViewLayout *)layout {
  self = [super initWithObjectClass:objectClass];
  if (self) {
    _layout = layout;
    _cellClass = cellClass;
    if ([layout isKindOfClass:[UICollectionViewFlowLayout class]]) {
      _flowLayout = (UICollectionViewFlowLayout *)layout;
    }
  }
  return self;
}

- (void)loadView {
  [super loadView];
  _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:_layout];
  _collectionView.alwaysBounceVertical = YES;
  _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  _collectionView.backgroundColor = [UIColor clearColor];
  if (_cellClass) {
    [_collectionView registerClass:_cellClass forCellWithReuseIdentifier:kQueryCellIdentifier];
//    if ([(Class)_cellClass respondsToSelector:@selector(height)]) {
//      _itemHeight = [_cellClass height];
//    }
  }
  _collectionView.dataSource = self;
  _collectionView.delegate = self;
  [self.view addSubview:_collectionView];
  
  self.scrollView = _collectionView;
}

- (void)reloadDataWithObjects:(NSArray *)objects reset:(BOOL)reset {
  [_collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return self.objects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  if (_cellClass) {
    return [collectionView dequeueReusableCellWithReuseIdentifier:kQueryCellIdentifier forIndexPath:indexPath];
  }
  return nil;
}

#pragma mark - UICollectionViewDelegate

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//  if (_cellClass) {
//    [(UICollectionViewCell<QueryViewCell> *)cell setObject:self.objects[indexPath.item]];
//  }
//  [self collectionView:collectionView willDisplayCell:cell withObject:self.objects[indexPath.item]];
//}
//
//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell withObject:(__kindof JSONModel *)object {
//  assert(@"Must override");
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//  [collectionView deselectItemAtIndexPath:indexPath animated:YES];
//  [self collectionView:collectionView didSelectItemWithObject:self.objects[indexPath.item]];
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemWithObject:(__kindof JSONModel *)object {
//  
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//  if (_itemWidth == 0.0) {
//    _itemWidth = (collectionView.frame.size.width - _itemSpacing * (_itemsPerRow + 1)) / _itemsPerRow;
//  }
//  return CGSizeMake(_itemWidth, _itemHeight > 0.0 ? _itemHeight : _itemWidth);
//}
//
//- (void)setItemSpacing:(CGFloat)itemSpacing {
//  _itemWidth = 0.0;
//  _itemSpacing = itemSpacing;
//  _flowLayout.sectionInset = UIEdgeInsetsMake(itemSpacing, itemSpacing, itemSpacing, itemSpacing);
//  _flowLayout.minimumLineSpacing = itemSpacing;
//  _flowLayout.minimumInteritemSpacing = itemSpacing;
//}
//
//- (void)setItemsPerRow:(NSInteger)itemsPerRow {
//  _itemWidth = 0.0;
//  _itemsPerRow = itemsPerRow;
//}

@end
