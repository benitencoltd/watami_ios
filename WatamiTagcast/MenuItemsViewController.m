//
//  MenuItemsViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <SDWebImage/SDImageCache.h>

#import "MenuItemsViewController.h"

#import "MenuItemCollectionViewCell.h"

@interface MenuItemsViewController ()

@end

@implementation MenuItemsViewController

- (instancetype)initWithCategoryId:(NSUInteger)categoryId {
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.sectionInset = UIEdgeInsetsMake(9.0, 9.0, 9.0, 9.0);
  flowLayout.minimumLineSpacing = 9.0;
  flowLayout.minimumInteritemSpacing = 9.0;
  self = [super initWithObjectClass:[WTMenuItem class] cellClass:[MenuItemCollectionViewCell class] layout:flowLayout];
  if (self) {
    self.key = @"data.menus";
    self.pagingEnabled = YES;
    _categoryId = categoryId;
  }
  return self;
}

- (void)dealloc {
  for (WTMenuItem *item in self.objects) { // Some images are too big (even take MB).
    [[SDImageCache sharedImageCache] removeImageForKey:[[SDWebImageManager sharedManager] cacheKeyForURL:item.imageURL] fromDisk:NO withCompletion:nil];
  }
  WTDeallocLog(self);
}

- (void)loadView {
  [super loadView];
  
  _collectionView.backgroundColor = [UIColor blackColor];
  _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
  
  self.statusView.style = SVStatusViewStyleWhite;
}

- (NSURLRequest *)requestForPage:(NSInteger)page {
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"menus",
                                             @"lang":[Common getLang],
                                             @"parameter":@{
                                                 @"category_id":@(_categoryId),
                                                 @"page":@(page)}};
  return [Common convertToRequest:postData];
}

- (void)taskDidCompleteWithResponse:(NSDictionary<NSString *, id> *)response error:(NSError *)error {
  if (response) {
    _numberOfPages = (NSUInteger)[response[@"data"][@"pages"] integerValue];
  }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MenuItemCollectionViewCell *cell = [super collectionView:collectionView cellForItemAtIndexPath:indexPath];
  WTMenuItem *item = self.objects[indexPath.item];
  [cell.imageView sd_setImageWithURL:item.imageURL];
  cell.nameLabel.text = item.name;
  cell.priceLabel.text = [NSString stringWithFormat:@"%.01f $", item.price];
  return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return CGSizeMake((collectionView.frame.size.width - 9.0 * 3) / 2.0, [MenuItemCollectionViewCell itemHeight]);
}

@end
