//
//  StampAddedAlertController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/21/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "StampAddedAlertController.h"

@interface StampAddedAlertController ()

@end

@implementation StampAddedAlertController

- (instancetype)init {
  self = [super initWithNibName:@"StampAddedAlertController" bundle:nil];
  if (self) {
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
  }
  return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didTapOk:(UIButton *)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

@end
