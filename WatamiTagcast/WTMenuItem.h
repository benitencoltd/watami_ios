//
//  WTMenuItem.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTMenuItem : JSONModel

@property (nonatomic, assign) NSUInteger id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSURL *imageURL;
@property (nonatomic, copy) NSURL *thumbnailURL;
@property (nonatomic, assign) float price;

@end
