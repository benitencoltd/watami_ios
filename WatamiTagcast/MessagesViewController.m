//
//  MessagesViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <MWPhotoBrowser/MWPhotoBrowser.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "MessagesViewController.h"
#import "LanguagePickerController.h"
#import "MessageCollectionViewCell.h"
#import "WTMessage.h"
#import "MWPhoto.h"

@interface MessagesViewController ()

@end

@implementation MessagesViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    self.key = @"data.news";
    self.pagingEnabled = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(showLanguagePicker:)];
    _objectClass = [WTMessage class];
    _cellClass = [MessageCollectionViewCell class];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.sectionInset = UIEdgeInsetsMake(9.0, 9.0, 9.0, 9.0);
    flowLayout.minimumLineSpacing = 10.0;
    flowLayout.minimumInteritemSpacing = 9.0;
    _layout = flowLayout;
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)loadView {
  [super loadView];
  self.statusView.style = SVStatusViewStyleWhite;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [Common trackScreen:@"message_screen"];
}

- (void)requestAppLinkWithURLString:(NSString *)URLString completion:(void (^)(NSString *appLinkId))completion {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getapplink",
                                             @"lang":[Common getLang],
                                             @"url":URLString};
  WTWeakify(self);
  [[[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(MessagesViewController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
      NSString *appLinkId = json[@"data"][@"app_link_id"];
      if (appLinkId.length > 0) {
        completion(appLinkId);
      }
    });
  }] resume];
}

- (NSURLRequest *)requestForPage:(NSInteger)page {
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"news",
                                             @"lang":[Common getLang],
                                             @"parameter":@{@"page":@(page)}};
  return [Common convertToRequest:postData];
}

- (void)taskDidCompleteWithResponse:(NSDictionary<NSString *, id> *)response error:(NSError *)error {
  if (response) {
    _numberOfPages = (NSUInteger)[response[@"data"][@"pages"] integerValue];
  }
}

- (void)showLanguagePicker:(UIBarButtonItem *)sender {
  LanguagePickerController *languagePickerController = [[LanguagePickerController alloc] init];
  [self.tabBarController presentViewController:[languagePickerController embedInNavigationController] animated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MessageCollectionViewCell *cell = [super collectionView:collectionView cellForItemAtIndexPath:indexPath];
  WTMessage *message = self.objects[indexPath.item];
  cell.titleLabel.text = message.title;
  [cell.imageView sd_setImageWithURL:message.imageURL];
  cell.textLabel.text = message.text;
  cell.dateLabel.text = WTStringFromDate(message.date);
  WTWeakify(self);
  cell.imageTapHandler = ^{
    WTStrongify(MessagesViewController);
    MWPhotoBrowser *photoBrowser = [[MWPhotoBrowser alloc] initWithPhotos:@[[[MWPhoto alloc] initWithURL:message.imageURL]]];
    photoBrowser.title = message.title;
    photoBrowser.doneBarButtonItemImage = [UIImage imageNamed:@"cross"];
    photoBrowser.keepsNavigationBarAppearance = YES;
    [strongSelf presentViewController:[photoBrowser embedInNavigationController] animated:YES completion:nil];
  };
  cell.shareHandler = ^{
    WTStrongify(MessagesViewController);
    NSString *appLinkURLString = [NSString stringWithFormat:@"com.beniten.tagcast.watami://detail?type=3"];
    [strongSelf requestAppLinkWithURLString:appLinkURLString completion:^(NSString *appLinkId) {
      FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
      content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://fb.me/%@", appLinkId]];
      content.contentTitle = message.title;
      content.contentDescription = message.text;
      content.imageURL = message.imageURL;
      
      FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
      dialog.fromViewController = self;
      dialog.mode = FBSDKShareDialogModeNative;
      dialog.shareContent = content;
      [dialog show];
    }];
  };
  return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewFlowLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  CGFloat itemWidth = collectionView.frame.size.width - collectionViewLayout.sectionInset.left - collectionViewLayout.sectionInset.right;
  return CGSizeMake(itemWidth, [MessageCollectionViewCell itemHeightForMessage:self.objects[indexPath.item] width:itemWidth]);
}

@end
