//
//  WTStampInfo.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/25/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTStampInfo : JSONModel

@property (nonatomic, assign) NSUInteger numberOfStamps;
@property (nonatomic, assign) NSUInteger numberOfTickets;
@property (nonatomic, assign) BOOL generatable;

@end
