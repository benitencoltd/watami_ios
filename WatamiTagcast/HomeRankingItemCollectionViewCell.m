//
//  HomeRankingItemCollectionViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/29/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "HomeRankingItemCollectionViewCell.h"

#define CIRCLE_BUTTON_WIDTH 30.0

@implementation HomeVIPItemCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    _circleImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _circleImageView.contentMode = UIViewContentModeScaleAspectFill;
    _circleImageView.clipsToBounds = YES;
    _circleImageView.tintColor = [UIColor whiteColor];
    [self.contentView addSubview:_circleImageView];
    
    _circleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _circleButton.backgroundColor = [UIColor colorWithRed:0.0 green:200.0/255.0 blue:83.0/255.0 alpha:1.0];
    _circleButton.tintColor = [UIColor whiteColor];
    [_circleButton addBorderColor:[UIColor whiteColor] borderWidth:3.0 cornerRadius:CIRCLE_BUTTON_WIDTH / 2];
    [_circleButton addTarget:self action:@selector(didTapCircle:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_circleButton];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.font = [UIFont appFontOfSize:13.0];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_titleLabel];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  _circleImageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.width);
  _circleImageView.layer.cornerRadius = _circleImageView.frame.size.width / 2.0;
  _circleButton.frame = CGRectMake(self.contentView.frame.size.width - CIRCLE_BUTTON_WIDTH, _circleImageView.frame.size.height - CIRCLE_BUTTON_WIDTH, CIRCLE_BUTTON_WIDTH, CIRCLE_BUTTON_WIDTH);
  _titleLabel.frame = CGRectMake(0.0, CGRectGetMaxY(_circleImageView.frame) + 4.0, self.contentView.frame.size.width, ceil(_titleLabel.font.lineHeight));
}

- (void)didTapCircle:(UIButton *)sender {
  _actionHandler();
}

@end

@implementation HomeRankingItemCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    _circleView = [[UIView alloc] init];
    _circleView.contentMode = UIViewContentModeScaleAspectFill;
    _circleView.clipsToBounds = YES;
    _circleView.tintColor = [UIColor whiteColor];
    [self.contentView addSubview:_circleView];
    
    _countLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _countLabel.font = [UIFont boldAppFontOfSize:32.0];
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_countLabel];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.font = [UIFont appFontOfSize:13.0];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_titleLabel];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  _circleView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.width);
  _circleView.layer.cornerRadius = _circleView.frame.size.width / 2.0;
  _countLabel.frame = _circleView.bounds;
  _titleLabel.frame = CGRectMake(0.0, CGRectGetMaxY(_circleView.frame) + 4.0, self.contentView.frame.size.width, ceil(_titleLabel.font.lineHeight));
}

@end
