//
//  MenuItemCategoryCollectionViewCell.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/10/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "MenuItemCategoryCollectionViewCell.h"

#define NAME_FONT_SIZE 13.0

@implementation MenuItemCategoryCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _imageView.clipsToBounds = YES;
    [self.contentView addSubview:_imageView];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _nameLabel.font = [UIFont appFontOfSize:NAME_FONT_SIZE];
    _nameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    _nameLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_nameLabel];
    
    self.selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    self.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:160.0/255.0 green:46.0/255.0 blue:48.0/255.5 alpha:1.0];
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  CGFloat imageWidth = self.contentView.frame.size.width - 8.0;
  _imageView.frame = CGRectMake(4.0, 4.0, imageWidth, imageWidth);
  _imageView.layer.cornerRadius = imageWidth / 2.0;
  
  _nameLabel.frame = CGRectMake(4.0, CGRectGetMaxY(_imageView.frame) + 6.0, imageWidth, ceil(_nameLabel.font.lineHeight));
}

+ (CGFloat)itemHeight {
  return (4.0 + floor([UIScreen mainScreen].bounds.size.width / 4) - 8.0) + 6.0 + ceil([UIFont appFontOfSize:NAME_FONT_SIZE].lineHeight) + 6.0;
}

@end
