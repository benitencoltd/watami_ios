//
//  NSDateFormatter+PrivateResponse.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "NSDateFormatter+PrivateResponse.h"

@implementation NSDateFormatter (PrivateResponse)

+ (NSDateFormatter *)responseSharedInstance {
  static NSDateFormatter *dateFormatter = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
  });
  return dateFormatter;
}

@end
