//
//  WTVIPUser.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/28/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTVIPUser.h"

@implementation WTVIPUser

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"accessId":@"access_id",
                                                                @"photoURL":@"photo",
                                                                @"numberOfStamps":@"nstamp"}];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
  NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dict];
  newDict[@"rank"] = @{@"rank":dict[@"rank"], @"type":dict[@"type"]};
  newDict[@"type"] = nil;
  return [super initWithDictionary:newDict error:err];
}

- (void)setPhotoURLWithNSString:(NSString *)string {
  _photoURL = [string containsString:@"no_image"] ? nil : [NSURL URLWithString:string];
}

@end
