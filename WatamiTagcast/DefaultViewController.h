//
//  DefaultViewController.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/2/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DefaultViewController : UIViewController

@property (nonatomic, assign) BOOL allowsUserInteraction;
@property (nonatomic, strong, readonly) UIActivityIndicatorView *loadingIndicatorView; // created on demand

@end
