//
//  MenuViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <CarbonKit/CarbonKit.h>
#import "MenuViewController.h"
#import "MenuItemsViewController.h"
#import "LanguagePickerController.h"
#import "MenuItemCategoryCollectionViewCell.h"

@interface MenuViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CarbonTabSwipeNavigationDelegate>

@property (nonatomic, strong) UICollectionView *categoryCollectionView;
@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) CarbonTabSwipeNavigation *carbonTabSwipeNavigation;

@end

@implementation MenuViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(showLanguagePicker:)];
    self.key = @"data.categories";
    _objectClass = [WTMenuItemCategory class];
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)loadView {
  [super loadView];
  
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
  flowLayout.minimumLineSpacing = 0.0;
  flowLayout.minimumInteritemSpacing = 0.0;
  
  _categoryCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, [MenuItemCategoryCollectionViewCell itemHeight]) collectionViewLayout:flowLayout];
  _categoryCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
  _categoryCollectionView.alwaysBounceHorizontal = YES;
  _categoryCollectionView.backgroundColor = [UIColor clearColor];
  _categoryCollectionView.showsHorizontalScrollIndicator = NO;
  _categoryCollectionView.showsVerticalScrollIndicator = NO;
  [_categoryCollectionView registerClass:[MenuItemCategoryCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([MenuItemCategoryCollectionViewCell class])];
  _categoryCollectionView.delegate = self;
  _categoryCollectionView.dataSource = self;
  [self.view addSubview:_categoryCollectionView];
  
  _containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, CGRectGetMaxY(_categoryCollectionView.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(_categoryCollectionView.frame))];
  _containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
  _containerView.backgroundColor = [UIColor clearColor];
  [self.view addSubview:_containerView];
  
  self.statusView.style = SVStatusViewStyleWhite;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [Common trackScreen:@"menu_screen"];
}

- (NSURLRequest *)request {
  NSDictionary<NSString *, id> *postData = @{@"action":@"categories",
                                             @"access_token":[Common getToken],
                                             @"lang":[Common getLang]};
  return [Common convertToRequest:postData];
}

- (void)taskDidCompleteWithResponse:(NSDictionary<NSString *, id> *)response error:(NSError *)error {
  if (!error) {
    [_categoryCollectionView reloadData];
    
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:[self.objects valueForKey:@"name"] toolBar:[[UIToolbar alloc] init] delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:_containerView];
    carbonTabSwipeNavigation.pagesScrollView.backgroundColor = [UIColor blackColor];
    _carbonTabSwipeNavigation = carbonTabSwipeNavigation;
  }
}

- (void)showLanguagePicker:(UIBarButtonItem *)sender {
  LanguagePickerController *languagePickerController = [[LanguagePickerController alloc] init];
  [self.tabBarController presentViewController:[languagePickerController embedInNavigationController] animated:YES completion:nil];
}

#pragma mark - CarbonTabSwipeNavigationDelegate

- (UIViewController *)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbontTabSwipeNavigation viewControllerAtIndex:(NSUInteger)index {
  return [[MenuItemsViewController alloc] initWithCategoryId:self.objects[index].id];
}

- (void)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbonTabSwipeNavigation didMoveAtIndex:(NSUInteger)index {
  [_categoryCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
  [_categoryCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return self.objects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MenuItemCategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MenuItemCategoryCollectionViewCell class]) forIndexPath:indexPath];
  WTMenuItemCategory *category = self.objects[indexPath.item];
  [cell.imageView sd_setImageWithURL:category.thumbnailURL];
  cell.nameLabel.text = category.name;
  return cell;
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return CGSizeMake(floor(collectionView.frame.size.width / 4.0), [MenuItemCategoryCollectionViewCell itemHeight]);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  [_carbonTabSwipeNavigation setCurrentTabIndex:indexPath.item];
}

@end
