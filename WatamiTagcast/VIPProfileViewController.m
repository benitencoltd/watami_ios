//
//  VIPProfileViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/28/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

#import "VIPProfileViewController.h"

#import "FBShareButton.h"

#import "WTMemberCard.h"

@interface VIPProfileViewController ()

@property (nonatomic, weak) IBOutlet UILabel *rankTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *photoImageView;
@property (nonatomic, weak) IBOutlet FBShareButton *shareButton;
@property (nonatomic, weak) IBOutlet UIView *verticalBarView;

- (IBAction)dismiss:(UIButton *)sender;
- (IBAction)share:(FBShareButton *)sender;

@end

@implementation VIPProfileViewController {
  CAGradientLayer *_verticalBarLayer;
}

- (instancetype)initWithProfile:(WTProfile *)profile vipInfo:(WTVIPInfo *)vipInfo {
  self = [super initWithNibName:NSStringFromClass([VIPProfileViewController class]) bundle:nil];
  if (self) {
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _profile = profile;
    _vipInfo = vipInfo;
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)viewDidLoad {
  [super viewDidLoad];
  _nameLabel.text = _profile.name;
  _rankTitleLabel.text = _vipInfo.currentRank.title;
  [_photoImageView sd_setImageWithURL:_profile.photoURL];
  _verticalBarLayer = [[CAGradientLayer alloc] init];
  
  UIColor *startColor = [UIColor colorWithRed:191.0/255.0 green:54.0/255.0 blue:12.0/255.0 alpha:1.0];
  UIColor *middleColor = [UIColor colorWithRed:1.0 green:171.0/255.0 blue:0.0 alpha:1.0];
  UIColor *endColor = [UIColor colorWithRed:1.0 green:214.0/255.0 blue:0.0 alpha:1.0];
  
  _verticalBarLayer.colors = @[(id)startColor.CGColor, (id)middleColor.CGColor, (id)endColor.CGColor];
  _verticalBarLayer.locations = @[@0.0, @0.5, @1.0];
  [_verticalBarView.layer addSublayer:_verticalBarLayer];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  _verticalBarLayer.frame = _verticalBarView.bounds;
}

- (void)loadMemberCardWithAppLinkURLString:(NSString *)appLinkURLString completion:(void (^)(WTMemberCard *memberCard))completion {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getmembercard",
                                             @"lang":[Common getLang],
                                             @"url":appLinkURLString};
  WTWeakify(self);
  [[[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(VIPProfileViewController);
    JSON(data)
    dispatch_async(dispatch_get_main_queue(), ^{
      [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
      if (json[@"data"]) {
        WTMemberCard *memberCard = [[WTMemberCard alloc] initWithDictionary:json[@"data"] error:NULL];
        if (memberCard && memberCard.appLinkId.length > 0) {
          completion(memberCard);
        }
      }
    });
  }] resume];
}

- (void)dismiss:(UIButton *)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)share:(FBShareButton *)sender {
  NSString *appLinkURLString = [NSString stringWithFormat:@"com.beniten.tagcast.watami://detail?type=2&level=%i", (int)_vipInfo.currentRank.type];
  WTWeakify(self);
  [self loadMemberCardWithAppLinkURLString:appLinkURLString completion:^(WTMemberCard *memberCard) {
    WTStrongify(VIPProfileViewController);
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://fb.me/%@", memberCard.appLinkId]];
    content.contentTitle = kWatamiTitle;
    content.contentDescription = [NSString stringWithFormat:@"%@ Level: %@", strongSelf.profile.name, strongSelf.vipInfo.currentRank.title];
    content.imageURL = memberCard.photoURL;
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = strongSelf;
    dialog.mode = FBSDKShareDialogModeNative;
    dialog.shareContent = content;
    [dialog show];
  }];
}

@end
