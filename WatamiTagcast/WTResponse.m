//
//  TCResponse.m
//  Khemara
//
//  Created by Beniten on 8/3/16.
//  Copyright © 2016 Beniten. All rights reserved.
//

#import "WTResponse.h"

@implementation WTResponse

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"succeeded":@"result"}];
}

@end
