//
//  StampsViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "StampsViewController.h"
#import "StampCollectionViewCell.h"

#define kSpacing 10.0
#define kMinimumNumberOfSlots 12

@interface StampsViewController ()

@end

@implementation StampsViewController {
  NSURLSessionDataTask *_task;
}

- (instancetype)init {
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.minimumInteritemSpacing = kSpacing;
  flowLayout.minimumLineSpacing = kSpacing;
  flowLayout.sectionInset = UIEdgeInsetsMake(kSpacing, kSpacing, kSpacing, kSpacing);
  CGFloat itemWidth = floor(([UIScreen mainScreen].bounds.size.width - kSpacing * 4) / 3);
  flowLayout.itemSize = CGSizeMake(itemWidth, itemWidth);
  
  self = [super initWithCollectionViewLayout:flowLayout];
  if (self) {
//    _numberOfStamps = 0;
  }
  return self;
}

- (void)dealloc {
  WTDeallocLog(self);
}

- (void)loadView {
  [super loadView];
  self.collectionView.alwaysBounceVertical = YES;
  self.collectionView.backgroundColor = [UIColor colorWithRed:128.0/255.0 green:26.0/255.0 blue:35.0/255.0 alpha:1.0];
  [self.collectionView registerClass:[StampCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([StampCollectionViewCell class])];
}

- (void)viewDidLoad {
  [super viewDidLoad];
//  [self reloadNumberOfStamps];
}

- (void)reloadNumberOfStamps {
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getstamp",
                                             @"lang":[Common getLang]};
  WTWeakify(self);
  _task = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToRequest:postData] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    WTStrongify(StampsViewController);
    JSON(data)
    if (json) {
      if ([json[@"result"] boolValue]) {
        _numberOfStamps = [json[@"data"][@"nstamps"] unsignedIntegerValue];
        dispatch_async(dispatch_get_main_queue(), ^{
          [self.collectionView reloadData];
//          [self.delegate getPrize:[json[@"data"][@"prize"] intValue]];
        });
      }
    }
  }];
  [_task resume];
}

- (void)setNumberOfStamps:(NSUInteger)numberOfStamps {
  _numberOfStamps = numberOfStamps;
  if ([self isViewLoaded]) {
    [self.collectionView reloadData];
  }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return _numberOfStamps > 0 ? MAX(_numberOfStamps, kMinimumNumberOfSlots) : kMinimumNumberOfSlots;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  StampCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([StampCollectionViewCell class]) forIndexPath:indexPath];
  cell.numberLabel.text = [NSString stringWithFormat:@"%i", (int)indexPath.item + 1];
  cell.bucketImageView.hidden = !(indexPath.row == 4 || indexPath.row == 9);
  cell.thankYouImageView.hidden = indexPath.item >= _numberOfStamps;
  return cell;
}

#pragma mark <UICollectionViewDelegate>

@end
