//
//  DefaultViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/2/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "DefaultViewController.h"
#import "LanguagePickerController.h"
#import "Constants.h"

@interface DefaultViewController ()

@end

@implementation DefaultViewController

@synthesize loadingIndicatorView = _loadingIndicatorView;

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(showLanguagePicker:)];
  if (self.navigationController && self.navigationController.viewControllers.count > 1) {
    self.navigationItem.leftItemsSupplementBackButton = NO;
    [self.navigationController.navigationBar removeBackBarButtonItemTitle];
  }
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  _loadingIndicatorView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
}

- (UIActivityIndicatorView *)loadingIndicatorView {
  if (!_loadingIndicatorView) {
    _loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _loadingIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_loadingIndicatorView];
  }
  return _loadingIndicatorView;
}

- (void)showLanguagePicker:(UIBarButtonItem *)sender {
  LanguagePickerController *languagePickerController = [[LanguagePickerController alloc] init];
  [self.tabBarController presentViewController:[languagePickerController embedInNavigationController] animated:YES completion:nil];
}

@end
