//
//  WTProfile.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 4/1/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTProfile : JSONModel

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *dob;
@property (nonatomic, copy) NSURL *photoURL;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *email;

@end
