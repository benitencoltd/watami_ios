//
//  WTStampTicket.h
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/22/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface WTStampTicket : JSONModel

@property (nonatomic, assign) NSUInteger id;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSURL *imageURL;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSDate *expiryDate;
@property (nonatomic, assign, getter=isUsed) BOOL used;

@end
