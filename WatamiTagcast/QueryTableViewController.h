//
//  QueryTableViewController.h
//  Khemara
//
//  Created by Beniten on 11/18/16.
//  Copyright © 2016 Beniten. All rights reserved.
//

#import "QueryViewController.h"
//#import "QueryViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QueryTableViewController<ObjectType : JSONModel *> : QueryViewController<ObjectType> <UITableViewDataSource, UITableViewDelegate> {
  @protected
  UITableView *_tableView;
}

@property (nonatomic, strong, readonly) UITableView *tableView;
@property (nonatomic, assign) UITableViewStyle style;
@property (nonatomic, assign, readonly, nullable) Class cellClass;

- (instancetype)initWithObjectClass:(Class)objectClass;
- (instancetype)initWithStyle:(UITableViewStyle)style objectClass:(Class)objectClass;
- (instancetype)initWithStyle:(UITableViewStyle)style objectClass:(Class)objectClass cellClass:(nullable Class)cellClass;

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell withObject:(ObjectType)object;
//- (void)tableView:(UITableView *)tableView didSelectRowWithObject:(ObjectType)object;

@end

NS_ASSUME_NONNULL_END
