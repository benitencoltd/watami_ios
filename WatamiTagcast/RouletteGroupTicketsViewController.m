//
//  RouletteGroupTicketsViewController.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/17/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "RouletteGroupTicketsViewController.h"
#import "RouletteTicketsViewController.h"
#import "LanguagePickerController.h"
#import "RouletteGroupTicketCollectionViewCell.h"

@interface RouletteGroupTicketsViewController ()

@end

@implementation RouletteGroupTicketsViewController {
  BOOL _loadsObjectsOnViewWillAppear;
}

- (instancetype)initWithTagcasts:(NSArray<NSDictionary<NSString *, id> *> *)tagcasts {
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.sectionInset = UIEdgeInsetsMake(10.0, 9.0, 10.0, 9.0);
  flowLayout.minimumLineSpacing = 10.0;
  self = [super initWithObjectClass:[WTRouletteGroupTicket class] cellClass:[RouletteGroupTicketCollectionViewCell class] layout:flowLayout];
  if (self) {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(showLanguagePicker:)];
    self.key = @"data.lotteries";
    _tagcasts = [tagcasts copy];
  }
  return self;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  WTDeallocLog(self);
}

- (void)loadView {
  [super loadView];
  self.view.backgroundColor = DARK_RED_COLOR;
  _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
  self.statusView.style = SVStatusViewStyleWhite;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = NSLocalizedString(@"tickets", nil);
  [self.navigationController.navigationBar removeBackBarButtonItemTitle];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidClaimRouletteTickets:) name:kUserDidClaimRouletteTicketsNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [Common trackScreen:@"lottery_list_screen"];
  if (_loadsObjectsOnViewWillAppear) {
    _loadsObjectsOnViewWillAppear = NO;
    [self reloadObjects];
  }
}

- (void)userDidClaimRouletteTickets:(NSNotification *)notification {
  if (self.isVisible) {
    [self reloadObjects];
  } else {
    _loadsObjectsOnViewWillAppear = YES;
    [self.objects removeAllObjects];
  }
}

- (void)showLanguagePicker:(UIBarButtonItem *)sender {
  LanguagePickerController *languagePickerController = [[LanguagePickerController alloc] init];
  [self.tabBarController presentViewController:[languagePickerController embedInNavigationController] animated:YES completion:nil];
}

- (NSURLRequest *)request {
  NSDictionary<NSString *, id> *postData = @{@"access_token":[Common getToken],
                                             @"action":@"getalllotterylist",
                                             @"lang":[Common getLang]};
  return [Common convertToRequest:postData];
}


#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  RouletteGroupTicketCollectionViewCell *cell = [super collectionView:collectionView cellForItemAtIndexPath:indexPath];
  WTRouletteGroupTicket *group = self.objects[indexPath.item];
  [cell.imageView sd_setImageWithURL:group.tickets.firstObject.imageURL];
  return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewFlowLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  CGFloat itemWidth = collectionView.frame.size.width - collectionViewLayout.sectionInset.left - collectionViewLayout.sectionInset.right;
  return CGSizeMake(itemWidth, [RouletteGroupTicketCollectionViewCell rowHeightForWidth:itemWidth]);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  WTRouletteGroupTicket *group = self.objects[indexPath.item];
  RouletteTicketsViewController *ticketsViewController = [[RouletteTicketsViewController alloc] initWithType:group.type tickets:group.tickets tagcasts:_tagcasts];
  [self.navigationController pushViewController:ticketsViewController animated:YES];
}

@end
