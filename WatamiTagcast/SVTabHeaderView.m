//
//  SVTabHeaderView.m
//  SVKit
//
//  Created by Sereivoan Yong on 7/31/16.
//  Copyright © 2016 BENITEN CO., LTD. All rights reserved.
//

#import "SVTabHeaderView.h"

@implementation SVTabHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    _bounces = YES;
    _minimumHeight = 40.0;
    _defaultHeight = 128.0;
    self.clipsToBounds = NO;
    self.userInteractionEnabled = NO;
  }
  return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
  UIView *targetView = [super hitTest:point withEvent:event];
  if (!targetView) {
    return nil;
  }
  
  NSArray<UIView *> *interactiveSubviews;
  if ([_delegate respondsToSelector:@selector(interactiveSubviewsInHeaderView:)]) {
    interactiveSubviews = [_delegate interactiveSubviewsInHeaderView:self];
  } else {
    return targetView;
  }
  
  if ([interactiveSubviews containsObject:self]) {
    return targetView;
  }
  
  __block BOOL isFound = NO;
  UIView *checkView = targetView;
  while (checkView != self) {
    [interactiveSubviews enumerateObjectsUsingBlock:^(UIView *interactiveSubview, NSUInteger idx, BOOL *stop) {
      if (checkView == interactiveSubview) {
        isFound = YES;
        *stop = YES;
      }
    }];
    if (isFound) {
      return targetView;
    }
    checkView = checkView.superview;
  }
  return nil;
}

@end
