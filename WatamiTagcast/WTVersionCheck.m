//
//  WTVersionCheck.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/23/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTVersionCheck.h"

@implementation WTVersionCheck

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"latestVersionString":@"latest_version",
                                                                @"forced":@"force_update",
                                                                @"incentiveClaimed":@"incentive"}];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

@end
