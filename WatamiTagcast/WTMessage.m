//
//  WTMessage.m
//  WatamiTagcast
//
//  Created by Sereivoan Yong on 3/13/17.
//  Copyright © 2017 BENITEN CO., LTD. All rights reserved.
//

#import "WTMessage.h"
#import "NSDateFormatter+PrivateResponse.h"

@implementation WTMessage

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"id":@"news_id",
                                                                @"title":@"news_title",
                                                                @"text":@"news_text",
                                                                @"date":@"news_date",
                                                                @"imageURL":@"news_image"}];
}

- (void)setDateWithNSString:(NSString *)string {
  _date = [[NSDateFormatter responseSharedInstance] dateFromString:[string componentsSeparatedByString:@" "].firstObject];
}

- (void)setImageURLWithNSArray:(NSArray<NSString *> *)array {
  _imageURL = [NSURL URLWithString:array.firstObject];
}

@end
